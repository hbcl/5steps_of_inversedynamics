clear all; close all; clc;


gitfolder = 'C:\Users\emily.mundinger\Documents\Git';
addpath(genpath([gitfolder,'\v3dpipelinescripts']))
addpath(genpath([gitfolder,'5steps_of_inversedynamics']))

% name = 'emily.mundinger';
% name = 'tim.vanderzee';
 date = '0326';
% 
% if strcmp(name(1),'e')==1
%     date = [date,'e'];
% elseif strcmp(name(1),'d')==1
%     date = [date,'d'];
% end

%% folders
% where the pipeline script building blocks live locally
folders.local.input.pipeline = 'C:\Users\emily.mundinger\Documents\Git\v3dpipelinescripts\';

% where the pipeline script is outputted locally
folders.local.output.pipeline = 'D:\ID_grid_data\Tim\'; % local pipeline script path

% where the CMOs live locally (for name reference)
folders.local.input.data = 'D:\ID_grid_data\Tim\'; % local data path

% where the CMOs live remotely (for actual processing)
folders.remote.input.data = 'D:\ID_grid_data\Tim\'; % remote CMO data path
% folders.remote.output.data = ['C:\Users\',name,'\OneDrive - University of Calgary\shared 5steps data\rawdata\']; % remote matlab data path

% where the V3D data is exported to remotely
folders.remote.output.data = 'D:\ID_grid_data\Tim\'; % remote matlab data path

%% names
names.CMO = {'070to110_and_160to200.cmo', '125_and_140.cmo'};
names.subj = {'eu' ,'kel', 'kez', 'leb', 'mmm', 'pbc', 'pga', 'smo', 'wj', 'yg'};
names.tags = {'TG1+TG2+TG3+TG4+TG5+TG6+TG7+TG8+TG9+TG10+TG11+TG12+TG13+TG14+TG15+TG16', 'TG17+TG18+TG19+TG20+TG21+TG22+TG23+TG24+TG25+TG26+TG27+TG28+TG29+TG30+TG31+TG32+TG33'};

make_pipelinescripts(date,folders,names)