function[] = runpipeline(datafolder,date,subjects,CMOfiles)
subjnames = {'eu' ,'kel', 'kez', 'leb', 'mmm', 'pbc', 'pga', 'smo', 'wj', 'yg'};
% datafolder = 'D:\ID_grid_data\Tim\'
% date = '0609';

for subj = subjects

    % list v3s files
    files=dir([datafolder,char(subjnames(subj)),'\*',date,'.v3s']);

    % loop over files
    
    for i = CMOfiles
        pipelinefile = [datafolder,char(subjnames(subj)),'\', files(i).name];
        disp(pipelinefile)
        dos(['"C:\Program Files\Visual3D v6 x64\Visual3D.exe" /s "' pipelinefile '"']) % '&'])
    end
end
end
