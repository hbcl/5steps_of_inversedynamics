function[offset, sfidx] = findgrfoffset(force)

%% Parameters
% These parameters influence the identification of samples that belong to
% the swing phase. The parameters values are partially determined using
% common sense, and partially using trial-and-error. They have been found
% to robustly result in satisfying results. 
fs = 1200; % 10x larger than mocap fs
fc1 = 20; % filter forces
fc2 = 10; % filter force-rates
nsamples = 200; % samples in moving average
threshold = [.05 .5]; % treshold for considering small values
[b1,a1] = butter(2, fc1/(fs/2), 'low');
[b2,a2] = butter(2, fc2/(fs/2), 'low');

%% Extract forces and filter
% Filters using a 20 Hz Butterworth low-pass filter
Fsf = filtfilt(b1,a1,force); 

%% Calculate derivative and filter differentiated signal
% The derivative is used to help identify the samples that belong to the
% swing phase (i.e. during the swing phase, the derivative has near-zero
% value, which is in contrast to the force-signal itself which might have
% non-zero value due to the offset)
dFsf = filtfilt(b2,a2,diff(Fsf)); 

%% Take small value parts, do moving average and take samples with neighbours
% Stance phase indices are set to nan. Stance phase indices are identified
% using: 
% 1) Derivative of the signal -> should be very small
% 2) Signal itself: should be reasonably small. 1) is not enough by itself,
% because one could be somehow able to produce near constant ground reaction
% force during stance
% 3) A moving average is used to identify indices that have a substantial
% amount of non-nan neighbours
dFsfs = [nan; dFsf];
copy = force;


% take small value parts
copy(abs(dFsf)>(std(dFsf,'omitnan')*threshold(1))) = nan;
copy(abs(Fsf)>(std(Fsf,'omitnan')*threshold(2))) = nan;

% moving average
madFsfs = movmean2(copy, nsamples);

idx = [1:length(force)]';
sfidx = idx(isfinite(madFsfs));

% retry with less samples
if isempty(sfidx);
   madFsfs = movmean2(copy, nsamples/2); 
    idx = [1:length(force)]';
    sfidx = idx(isfinite(madFsfs));
end


offset = mean(force(sfidx));



end


