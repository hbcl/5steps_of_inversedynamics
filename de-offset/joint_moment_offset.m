function joint_moment_offset(datafolder,codefolder, subj, joint)
% The joint moment offset function finds the trials with the largest offset
% for a given subject and joint based on peak moments for each step. Offset
% is the difference in peak joint moments between the left and right leg
%
%Inputs: datafolder- a string that has the full path name to the folder 
% where the 5 step data is located
% codefolder- a string specifying where this code is. The path can stop at
% 5stepinversedynamics repository no need to include the subfolders
%subj- lets you specify one of the 10 subjects, input should be a single
%number ex. 5
%joint can be defined as one of the following 3 strings:
%'ankle', 'knee', or 'hip'
%
% For each trial, peak joint moment is defined for steps on both the left 
% and right leg and in the x, y, and z direction. The difference between and
% left and right leg for each step is calculated and the average of the 
% absolute differences for the 5 steps is found. After this is done for x, y, 
% and z directions, the 3 values are squared, summed, and the square root 
%of this is used to determine the trial with the largest offset.
%
%This function will display in the command window the trial with the
%greatest offset and the offset value. It will also produce 2 graphs for
%the trial- The first figure displays 6 subplots with joint moments on the
%left and right leg in the x, y, and z direction, the second plot is
%produced using visualize_hipkin, kneekin, or anklekin functions
%
%function is used for one subject at a time

addpath(genpath(datafolder))
addpath(genpath(codefolder))

if strcmp(joint, 'ankle')
    num=[1,2,3,10,11,12];
elseif strcmp(joint, 'knee')
    num=[4,5,6,13,14,15];
elseif strcmp(joint, 'hip')
    num=[7,8,9,16,17,18];
end

load(['p',num2str(subj),'_5steps_data'])
deltapeak=nan(1,length(data));
mass = [81.8 57.3 97.5 60.3 56.7 72.6 86.2 88.6 77 57]; % from excel sheets
for trial = 1:length(data)
    jointmoment=data(trial).jointmoment2; %.jointmoment or .jointmoment2
    jointmoment=jointmoment/mass(subj); %normalize if using jointmoment2, if using jointmoment comment out this line
    if ~isnan(jointmoment)
%% Extract data

        % Extract grfs
        grfl = data(trial).grf(:,1:3);
        grfr = data(trial).grf(:,4:6);

        % Threshold at 20 N
        grfl(grfl(:,3)<20,:) = 0;
        grfr(grfr(:,3)<20,:) = 0;

        % Get heelstrikes
        [hsl, tol, hsr, tor] = invDynGrid_getHS_TO(grfl,grfr, 10);
        hsl = round(unique(hsl)/10) + 1;
        hsr = round(unique(hsr)/10) + 1;
        hsr(6)=length(jointmoment); %add index on end of hsr that is equal to the length of the joint moment data

        
%% Find difference in left and right joint moment peaks  
        peak_momentx=nan(5,2);  
        peak_momenty=nan(5,2);  
        peak_momentz=nan(5,2);
        
        for step=1:(length(hsl)-1)
            peak_momentx(step,:)= [max(abs(jointmoment(hsl(step):hsl(step+1),num(1)))),max(abs(jointmoment(hsr(step):hsr(step+1),num(4))))];
            peak_momenty(step,:)= [max(abs(jointmoment(hsl(step):hsl(step+1),num(2)))),max(abs(jointmoment(hsr(step):hsr(step+1),num(5))))];
            peak_momentz(step,:)= [max(abs(jointmoment(hsl(step):hsl(step+1),num(3)))),max(abs(jointmoment(hsr(step):hsr(step+1),num(6))))];
        end

        %Takes mean diff across the 5 steps for each individual trial
        deltapeakx=mean((abs(peak_momentx(:,1)-peak_momentx(:,2))),'omitnan');
        deltapeaky=mean((abs(peak_momenty(:,1)-peak_momenty(:,2))),'omitnan');
        deltapeakz=mean((abs(peak_momentz(:,1)-peak_momentz(:,2))),'omitnan');
        deltapeak(trial)=sqrt(deltapeakx.^2+deltapeaky.^2+deltapeakz.^2);
    end
end

%% Calculate hsl/hsr values for worst trial
%first empty grfl/grfr and hsl/hsr
clear('grfl')
clear('grfr')
clear ('hsl')
clear('hsr')

[peak, index]= sort(deltapeak,'descend','MissingPlacement','last');
disp('offset and its corresponding trial')
disp([peak',index'])
%index has the trial number (1-33) for given delta peak value
disp('largest difference is Trial:')
disp(index(1))
disp('difference=')
disp(peak(1))

%heelstrike for worst trial
grfl = data(index(1)).grf(:,1:3);
grfr = data(index(1)).grf(:,4:6);
grfl(grfl(:,3)<20,:) = 0;
grfr(grfr(:,3)<20,:) = 0;

[hsl, tol, hsr, tor] = invDynGrid_getHS_TO(grfl,grfr, 10);
hsl = round(unique(hsl)/10) + 1;
hsr = round(unique(hsr)/10) + 1;
hsr(6)=length(data(index(1)).jointmoment2);

jm=data(index(1)).jointmoment2; %jm is worst joint moment
jm=jm/mass(subj);


%% Make plot for worst trial
figure('name',(['joint moment, subj:', num2str(subj), '  trial:', num2str(index(1))]))

    for i=1:(length(hsl)-1) %plots each step on top of one another
        subplot(3,2,1)
        plot(jm(hsl(i):hsl(i+1),num(1)),'b')
        ylabel('joint moment x')
        hold on
        subplot(3,2,2)
        plot(jm(hsr(i):hsr(i+1),num(4)),'r')
        hold on
        ylabel('joint moment x')

        subplot(3,2,3)
        plot(jm(hsl(i):hsl(i+1),num(2))); hold on
        ylabel('joint moment y')
        subplot(3,2,4)
        plot(jm(hsr(i):hsr(i+1),num(5))); hold on
        ylabel('joint moment y')
        
        subplot(3,2,5)
        plot(jm(hsl(i):hsl(i+1),num(3))); hold on
        ylabel('joint moment z')
        subplot(3,2,6)
        plot(jm(hsr(i):hsr(i+1),num(6))); hold on
        ylabel('joint moment z')
    end
    
    if strcmp(joint, 'ankle')
        visualize_anklekin(datafolder,subj,index(1))
    elseif strcmp(joint, 'knee')
        visualize_kneekin(datafolder,subj,index(1))
    elseif strcmp(joint, 'hip')
        visualize_hipkin(datafolder,subj,index(1))
    end

end

