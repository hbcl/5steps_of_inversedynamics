function[vdoffset] = determine_grf_offset(datafolder,subj)

addpath(genpath(datafolder))
cd(datafolder)

offset_grf = nan(33,6);
offset_grf2 = nan(33,6);

load(['p',num2str(subj),'_5steps_data.mat']);

%% determine offsets
for trial = 1:length(data)
     if isempty(data(trial).grf2) || sum(isnan(data(trial).grf2(:))>0)     % give up?
        continue
     end
     
     % flip some axes
    data(trial).grf2 = [-data(trial).grf2(:,1) data(trial).grf2(:,2) -data(trial).grf2(:,3) -data(trial).grf2(:,4) data(trial).grf2(:,5) -data(trial).grf2(:,6)];

    % difference between the two during stance is offset used for grf
    offset_grf(trial,:) = median(data(trial).grf2 - data(trial).grf);

    % find the vertical offset you should use for grf2 (and therefore also for grf)
    [offset_grf2(trial,3), lidx] = findgrfoffset(data(trial).grf2(:,3)); % left leg    
    [offset_grf2(trial,6), ridx] = findgrfoffset(data(trial).grf2(:,6)); % right leg
    
    % also find corresponding horizontal offsets
    offset_grf2(trial,1) = mean(data(trial).grf2(lidx,1));  % left leg    
    offset_grf2(trial,2) = mean(data(trial).grf2(lidx,2));  % left leg    
    offset_grf2(trial,4) = mean(data(trial).grf2(ridx,4));  % right leg    
    offset_grf2(trial,5) = mean(data(trial).grf2(ridx,5));  % right leg    
end

%% difference in offsets
doffset = offset_grf2 - offset_grf;
vdoffset = offset_grf2(:,[3 6]) - offset_grf(:,[3 6]);
disp(sum(abs(vdoffset),2))
[doffsets,worstid] = sort(sum(abs(vdoffset),2),'descend','MissingPlacement','last'); % find the worst offsets

%% load the full trial
trial = worstid(1);
subjnames = {'eu' ,'kel', 'kez', 'leb', 'mmm', 'pbc', 'pga', 'smo', 'wj', 'yg'};
date = '0710';
midparts = {'25Force6Marker_walking_070to110_and_160to200_data', '25Force6Marker_walking_125_and_140_data'};
subjname = char(subjnames(subj));
    
if trial<17, CMOfile = 1;
else, CMOfile = 2;
end

if exist([subjname, midparts{CMOfile}, date,'_TG', num2str(trial),'.mat'],'file')
    load([subjname, midparts{CMOfile}, date,'_TG', num2str(trial),'.mat'],'f1zproc','f2zproc','zerorange')
end
zeror = double(zerorange{1}) * 10;


%% plotting
close all
figure('name',['subject:',num2str(subj),' - trial:',num2str(trial)])    

data(trial).grf = data(trial).grf(1:10:end,:);
data(trial).grf2 = data(trial).grf2(1:10:end,:);

for panel = 1:3

    % left leg
    subplot(2,3,panel);
    plot(data(trial).grf(:,panel)+1,'-','linewidth',2);hold on
    plot(data(trial).grf2(:,panel),'-','linewidth',1)

    plot(data(trial).grf2(:,panel)-offset_grf(trial,panel),'-','linewidth',2)
    plot(data(trial).grf2(:,panel)-offset_grf2(trial,panel),'--','linewidth',2)

    title(['offset difference = ', num2str(doffset(trial,panel)), 'N']);

    % right leg
    subplot(2,3,panel+3);
    plot(data(trial).grf(:,panel+3)+1,'-','linewidth',2);hold on
    plot(data(trial).grf2(:,panel+3),'-','linewidth',1)
    plot(data(trial).grf2(:,panel+3)-offset_grf(trial,panel+3),'-','linewidth',2)
    plot(data(trial).grf2(:,panel+3)-offset_grf2(trial,panel+3),'--','linewidth',2)

    title(['offset difference = ', num2str(doffset(trial,panel+3)), 'N']);

    set(gcf,'units','normalized')
    set(gcf,'position',[0 0 1 1])

end

for j = 1:6
    subplot(2,3,j)
    plot([0 length(data(trial).grf(:,1))], [0 0], 'k-')
    xlim([0 length(data(trial).grf(:,1))])
end

subplot(2,3,1)
legend('grf+1 (thresholded)','grf2 (raw)','grf2-[used offset]','grf2-[true offset]','location','best')

%% Second figure: full trial
fs = 1200;
figure('name',['full trial, subject:',num2str(subj),' - trial:',num2str(trial)])   
subplot(211);
plot(f1zproc{1}); hold on
plot(f2zproc{1}); hold on
plot([zeror(1,1) zeror(1,1)], [min(f1zproc{1}) max(f1zproc{1})],'k')
plot([zeror(2,1) zeror(2,1)], [min(f1zproc{1}) max(f1zproc{1})],'k')
axis tight; xlim([zeror(1,1)-fs zeror(2,1)+fs])

subplot(212);
plot(f1zproc{1}); hold on
plot(f2zproc{1}); hold on
plot([zeror(1,2) zeror(1,2)], [min(f2zproc{1}) max(f2zproc{1})],'k')
plot([zeror(2,2) zeror(2,2)], [min(f2zproc{1}) max(f2zproc{1})],'k')
axis tight; xlim([zeror(1,2)-fs zeror(2,2)+fs])

%% Plot interpretation
% You want grf (blue), grf2-[used offset] (yellow) and grf2-[true offset] (purple)
% to be the same at all times, because this would indicate that
% [true offset] = [used offset] and that the offset is correctly applied to
% grf. grf2 (red) is okay to be different, since this is the raw grf, which
% is not supposed to have been de-offsetted

end



