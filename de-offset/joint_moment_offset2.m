function joint_moment_offset2(datafolder,codefolder, subj, joint)
% calculates the trial with the largest joint moment offset using 100% of 
% gait cycle. The offset is the diffence in joint moments on the left and
% right leg over 100% gait cycle
%
%Inputs: datafolder- a string that has the full path name to the folder 
% where the 5 step data is located
% code folder- a string specifying where this code is. The path can stop at
% 5stepinversedynamics repository no need to include the subfolders
%subj- lets you specify one of the 10 subjetcs, input should be a single
%number ex. 5
%joint can be defined as one of the following 3 strings:
%'ankle', 'knee', or 'hip'
%
%The difference between the left and right leg over 100 % of 
% the gait cycle (101 points for each of the 5 steps)is calculated. This 
% difference over the 5 steps is found for the x, y, and z directions and 
%is then averaged over steps. The 3 values are squared, summed, and the 
%square root is found in order to determine the largest offset.
%
%The trial with the largest offset and the offset value will be displayed
%in the command window and 2 graphs for joint moment will appear for this
%trial.
%
% function is used for one subject at a time

addpath(genpath('datafolder'))
cd(datafolder);

if strcmp(joint, 'ankle')
    num=[1,2,3,10,11,12];
elseif strcmp(joint, 'knee')
    num=[4,5,6,13,14,15];
elseif strcmp(joint, 'hip')
    num=[7,8,9,16,17,18];
end

load(['p',num2str(subj),'_5steps_data'])
deltapeak=nan(1,length(data));
mass = [81.8 57.3 97.5 60.3 56.7 72.6 86.2 88.6 77 57]; % from excel sheets
for trial = 1:length(data)
    disp(trial)
    jointmoment=data(trial).jointmoment2;
    jointmoment=jointmoment/mass(subj);
    if ~isnan(jointmoment)
%% Extract data

        % Extract grfs
        grfl = data(trial).grf(:,1:3);
        grfr = data(trial).grf(:,4:6);

        % Threshold at 20 N
        grfl(grfl(:,3)<20,:) = 0;
        grfr(grfr(:,3)<20,:) = 0;

        % Get heelstrikes
        [hsl, tol, hsr, tor] = invDynGrid_getHS_TO(grfl,grfr, 10);
        hsl = round(unique(hsl)/10) + 1;
        hsr = round(unique(hsr)/10) + 1;
        hsr(6)=length(jointmoment); %add index on end of hsr that is equal to the length of the joint moment data

        
%% Find difference in left and right joint moment peaks  
        deltax=nan(1,5);  
        deltay=nan(1,5);  
        deltaz=nan(1,5);

        jointmomentxl=interpolate_to_percgaitcycle(jointmoment(:,num(1)),hsl,101);
        jointmomentxr=interpolate_to_percgaitcycle(jointmoment(:,num(4)),hsr,101);
        jointmomentyl=interpolate_to_percgaitcycle(jointmoment(:,num(2)),hsl,101);
        jointmomentyr=interpolate_to_percgaitcycle(jointmoment(:,num(5)),hsr,101);
        jointmomentzl=interpolate_to_percgaitcycle(jointmoment(:,num(3)),hsl,101);
        jointmomentzr=interpolate_to_percgaitcycle(jointmoment(:,num(6)),hsr,101);
        
        %Find the difference between left and right for all 5 steps, and 
        %take the mean for all 101 data points
        for step=1:5
            deltax(step)= mean(abs(jointmomentxl(:,step)-jointmomentxr(:,step)));
            deltay(step)= mean(abs(jointmomentyl(:,step)-jointmomentyr(:,step)));
            deltaz(step)= mean(abs(jointmomentzl(:,step)-jointmomentzr(:,step)));
        end

        %Takes mean diff across the 5 steps for each individual trial
        deltaxmean=mean(deltax,'omitnan');
        deltaymean=mean(deltay,'omitnan');
        deltazmean=mean(deltaz,'omitnan');
        deltapeak(trial)=sqrt(deltaxmean.^2+deltaymean.^2+deltazmean.^2);
    end
end

%% Calculate hsl/hsr values for worst trial
%first empty grfl/grfr and hsl/hsr
clear('grfl')
clear('grfr')
clear ('hsl')
clear('hsr')

[peak, index]= sort(deltapeak,'descend','MissingPlacement','last');
disp('offset and its corresponding trial')
disp([peak',index'])
%index has the trial number (1-33) for given delta peak value
disp('largest difference is Trial:')
disp(index(1))
disp('difference=')
disp(peak(1))


%heelstrike for worst trial
grfl = data(index(1)).grf(:,1:3);
grfr = data(index(1)).grf(:,4:6);
grfl(grfl(:,3)<20,:) = 0;
grfr(grfr(:,3)<20,:) = 0;

[hsl, tol, hsr, tor] = invDynGrid_getHS_TO(grfl,grfr, 10);
hsl = round(unique(hsl)/10) + 1;
hsr = round(unique(hsr)/10) + 1;
hsr(6)=length(data(index(1)).jointmoment2);

jm=data(index(1)).jointmoment2; %jm is worst joint moment
jm=jm/mass(subj);


%% Make plot for worst trial
figure('name',(['joint moment, subj:', num2str(subj), '  trial:', num2str(index(1))]))

    for i=1:(length(hsl)-1) %plots each step on top of one another
        subplot(3,2,1)
        plot(jm(hsl(i):hsl(i+1),num(1)),'b')
        ylabel('joint moment x')
        hold on
        subplot(3,2,2)
        plot(jm(hsr(i):hsr(i+1),num(4)),'r')
        hold on
        ylabel('joint moment x')

        subplot(3,2,3)
        plot(jm(hsl(i):hsl(i+1),num(2))); hold on
        ylabel('joint moment y')
        subplot(3,2,4)
        plot(jm(hsr(i):hsr(i+1),num(5))); hold on
        ylabel('joint moment y')
        
        subplot(3,2,5)
        plot(jm(hsl(i):hsl(i+1),num(3))); hold on
        ylabel('joint moment z')
        subplot(3,2,6)
        plot(jm(hsr(i):hsr(i+1),num(6))); hold on
        ylabel('joint moment z')
    end
    
    if strcmp(joint, 'ankle')
        visualize_anklekin(datafolder,subj,index(1))
    elseif strcmp(joint, 'knee')
        visualize_kneekin(datafolder,subj,index(1))
    elseif strcmp(joint, 'hip')
        visualize_hipkin(datafolder,subj,index(1))
    end

end

