clear all; close all; clc;

%% 3D
grid = 0:.025:1;

SL = repmat(grid,length(grid),1);
SP = repmat(grid(:),1,length(grid));

W = @(SP,SL) SL.^2.*SP.^2;

figure(1);
mesh(SP,SL,W(SP,SL))
axis equal; hold on;
plot3(grid, grid.^.42, W(grid,grid.^.42),'k','linewidth',2)
plot3(grid, grid, W(grid,grid),'k--','linewidth',2)
xlabel('Speed')
ylabel('Step length')
zlabel('Collision work')
% plot2svg('SLSPW.svg');

%% 2D
grid = 0:.025:2;
figure;
subplot(121)
plot(grid, W(grid,1)); hold on
plot(grid, W(grid,grid))
plot(grid, W(grid,grid.^.42))

subplot(122)
plot(grid, W(1,grid))
title('v = 1.25')
ylim([0 16])



