function[Wmat, RMSE_R2, LM] = make_3D_plot(Wterm, parms, optional_fit, color, type)
 
if nargin == 2
    optional_fit = nan;
end

if nargin < 5
    type = 'impact';
end

if isfield(parms,'colors')
    colors = parms.colors;
else
    figure;
    colors = get(gca,'colororder');
    close
end

%% make 3D plot
idx = parms.idx;
subjs = [1:3, 5:10];
xgrid = 0:.05:1.2; xgrid = xgrid(:);
xfine = 0:.01:1.2; xfine = xfine(:);

[SP_grid,SL_grid] = meshgrid(xgrid); % Generate x and y data
SW = @(C,SP,SL) C*SL.^2.*SP.^2;
[SLs, SPs, Wmat,SPs_av, dVs] = get_Wmat(Wterm, parms,type);

if strcmp(type(1:7), 'average') == 1
    SPs = SPs_av;
    c = 1/8;
   
end

if strcmp(type, 'average&exclude') == 1
    Wmat(idx(3):idx(4)-1,:) = nan;
end

if strcmp(type, 'velocity-based') == 1
    SLs = dVs /180*pi;
end

if strcmp(type, 'velocity-based&heelstrike') == 1
    SLs = tan(dVs /180*pi/2);
    c = .5;
%     SLs = dVs / 180*pi;
        
end

% Method 1: fminsearch
% SW_cost = @(C,SP,SL,W) sum((W-(C*SL.^2.*SP.^2)).^2,'all','omitnan');
% C_star = fminsearch(@(C) SW_cost(C,SPs, SLs, Wmat), 10); 

% Method 2: regression
% stats = anyfit(SPs(:,subjs).^2 .* SLs(:,subjs).^2,  Wmat(:,subjs),'offset','none','plot','none');

X = c .* SPs(:,subjs).^2 .* SLs(:,subjs).^2;
Y = Wmat(:,subjs);
S = repmat((1:size(X,2))',1,size(X,1))';
%        stats(i) = anyfit(X, Y,'plot','none');

M = [(Y(1:numel(Y)))' (X(1:numel(X)))' (S(1:numel(S)))'];
varnames = {'Work' 'GaitPar' 'Subject'};
modelString = parms.modelString;

T = array2table(M);
T.Properties.VariableNames = varnames;
%        LME = fitlme(T,modelString,'DummyVarCoding','reference');
LM = fitlm(T,modelString);

stats.coef = LM.Coefficients.Estimate;
coef = stats.coef(end) * c;
if length(stats.coef) == 1
    offset = 0;
else
    offset = stats.coef(1);
end
[~, f1] = relation_between_speed_and_steplength(1);

%% Slope for preferred SP-SL curve
% figure
prefSLs = SLs(idx(1):idx(2)-1,:);
prefSPs = SPs(idx(1):idx(2)-1,:);
% plot(prefSLs, prefSPs,'o')

% [varargout] = anyfit(prefSPs'.^.42,prefSLs','offset','none');
% close

varargout.coef =  1.1007;

%% Plotting
symb = {'o','s','d','v'};
fillcolor = parms.colors(:,:,1);

if strcmp(optional_fit,'Dofit') == 1
   hold on; grid on
   
   for i = 1:4
       SLsvec = SLs(idx(i):idx(i+1)-1,:); SPsvec = SPs(idx(i):idx(i+1)-1,:); Wvec = Wmat(idx(i):idx(i+1)-1,:);
       h = plot3(SLsvec(:), SPsvec(:), Wvec(:), symb{i});
       set(h,'marker',symb{i},'color',color(i,:),'markersize',7,'markerfacecolor',fillcolor(i,:)); hold on
       
       % plot fits
%        [func] = relation_between_speed_and_steplength(i,parms.show);
%        figure(f)
%        if i == 3
%            plot3(xfine, func(xfine), SW(coef, func(xfine), xfine) + offset, '-','color',color(i,:),'linewidth',2)
%        else
%            plot3(func(xfine), xfine, SW(coef, xfine, func(xfine)) + offset, '-','color',color(i,:),'linewidth',2)
%        end

   end
    
    % zero plane
    patch([1 1 -1 -1],[1 -1 -1 1], [0 0 0 0], 'LineStyle','None', 'FaceColor', 'k','FaceAlpha', .05); hold on
    
    % plot surface
    surf(SL_grid,SP_grid,SW(coef, SP_grid,SL_grid) + offset,color(1,:),'FaceColor', color(1,:),'FaceAlpha', .2, 'EdgeColor', color(1,:))

    % make nice
    ylabel('Speed'); xlabel('Step length'); zlabel('Soft tissue dissipation')
%     title(['W = c \cdot SL^2 \cdot SP^2 with (c = ',num2str(round(stats.coef,3)),'\pm',num2str(round(stats.coefint,3)),')'])

end

if ~strcmp(type(1),'v')
    %% plot relationships ideal 
    fitcolors = parms.colors(:,:,3);
    foffset = offset+.001;

    plot3(varargout.coef*xfine.^.42, xfine, SW(coef, xfine, varargout.coef*xfine.^.42)+ foffset , 'color',fitcolors(1,:),'linewidth',2)
    plot3(xfine/mean(parms.prefFQ_dl),xfine, SW(coef, xfine, xfine/mean(parms.prefFQ_dl))+ foffset , 'color',fitcolors(2,:),'linewidth',2)
    plot3(xfine,repmat(mean(parms.prefSP_dl), size(xfine)), SW(coef, repmat(mean(parms.prefSP_dl), size(xfine)), xfine)+foffset , 'color',fitcolors(3,:),'linewidth',2)
    plot3(repmat(mean(parms.prefSL_dl), size(xfine)), xfine,SW(coef, xfine, repmat(mean(parms.prefSL_dl), size(xfine)))+foffset , 'color',fitcolors(4,:),'linewidth',2)

    %% plot actual relationship for variable step length conditions
    plot3(xfine/mean(parms.prefFQ_dl), (xfine+f1(xfine/mean(parms.prefFQ_dl))), ...
        SW(coef, (xfine+f1(xfine/mean(parms.prefFQ_dl))), xfine/mean(parms.prefFQ_dl))+ foffset,':', 'color',fitcolors(2,:),'linewidth',2)

    plot3(xfine, (repmat(mean(parms.prefSP_dl), size(xfine))+f1(xfine)), ...
        SW(coef, (repmat(mean(parms.prefSP_dl), size(xfine))+f1(xfine)), xfine)+ foffset,':','color',fitcolors(3,:),'linewidth',2)

end
%% RMSE & R2
prediction = SW(coef, SPs,SLs) + offset;
actual = Wmat(:,subjs);
pred = prediction(:,subjs);

RMSE_R2(1) = rms(actual(:)-pred(:),'omitnan');
RES = sum((actual(:)-pred(:)).^2,'omitnan');    
TOT = sum((actual(:)-mean(actual(:),'omitnan')).^2,'omitnan');
RMSE_R2(2) = 1 - RES/TOT; 
end

