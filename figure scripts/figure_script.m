clear all; close all; clc

olddatapath = 'C:\Users\Tim\OneDrive - University of Calgary\shared 5steps data\processed_data';
datapath = 'C:\Users\Tim\Documents\5 Step Data';
codepath = 'C:\Users\Tim\Documents\GitHub\5steps_of_inversedynamics';

addpath(genpath(datapath));
addpath(genpath(codepath));
addpath(genpath(olddatapath))

subjs = [1:3, 5:10];

%% Figure 1
grid = 0:.1:1;

SL = repmat(grid,length(grid),1);
SP = repmat(grid(:),1,length(grid));

W = @(SP,SL) SL.^2.*SP.^2;

figure(1);
color = get(gca,'colororder');
surf(SL,SP,W(SL,SP),'FaceColor', color(1,:),'FaceAlpha', .2, 'EdgeColor', color(1,:))

%% Load some things
% processed data
cd(datapath);
cd(olddatapath)
load('Wsoft.mat')
% Wsoftcoll(4,9,:) = nan;

cd(olddatapath)
A = load('Ankle_PG.mat');
K = load('Knee_PG.mat');
H = load('Hip_PG.mat');

% parameters
load('parms.mat')


% % import colors
% cd('C:\Users\Tim\OneDrive\Soft tissue\SVG files')
% cimage = imread('color_gradient.png');
% colors = squeeze(cimage(100,2:end-1,:));
% cd('C:\Users\Tim\Documents\GitHub\5steps_of_inversedynamics\figure scripts')
% save('newcolors.mat','colors');

load('newcolors.mat');
ncolors = colors;
% load('colors.mat');

% created in fig8 and used in fig9
% load('Work3Dstats2.mat')

%% Parameters
parms.leglength = [0.8941 0.9398 1.04  0.8636 0.8636 0.9398 0.9906 0.99 0.9398 0.876]; % m
parms.mass = [81.8000 57.3000 97.5000 60.3000 56.7000 72.6000 86.2000 88.6000 77 57]; % kg
parms.prefFQ = [106 124 106 116 116 102 102 102 110 110]/60; % BPM -> Hz
parms.prefSP = 1.25; % m/s

parms.g = 9.81; % m/s2
Mg = parms.mass .* parms.g;
MgL = Mg .* parms.leglength;
gL = mean(parms.g .* parms.leglength);
gsgsL = mean(parms.g .* sqrt(parms.g) .* sqrt(parms.leglength));
srgL = sqrt(parms.g ./ parms.leglength);

parms.idx = [1 9 16 23 30];
altcolors = [76 131 177; 179 157 97; 255 198 46; 120 182 255]/255;
p = [.5 1 2 3 1e6];
for i = 1:5
    fcolors(:,:,i) = altcolors.^p(i);
end

parms.colors = fcolors;
parms.dimensionless = 1;
parms.show = 1;

% dimensionless preferences
parms.prefSL_dl = (parms.prefSP ./ parms.prefFQ) ./ parms.leglength; % dimensionless
parms.prefSP_dl = parms.prefSP ./ sqrt(parms.g * parms.leglength);
parms.prefFQ_dl = parms.prefSP_dl ./ parms.prefSL_dl;

%% Figure 3: 3x3 joint plot
disp('Preparing Figure 3')
figure(3); fig3

%% Figure 4: 1x3 joint vs. COM plot
disp('Preparing Figure 4')
parms.colors = fcolors(:,:,2);
figure(4); fig4

%% Figure 5: 2x2 joint work
disp('Preparing Figure 5')
figure(5); fig5

%% Figure 6&7: 3x4 joint vs. COM plots
disp('Preparing Figure 6 & 7')
load('colors.mat');
figure(6); fig6and7

return
%% Figure 8: work 3D plots
close all
disp('Preparing Figure 8')
parms.dimensionless = 1;
parms.colors = fcolors;
% type = 'average&exclude';
% type = 'average';
% type = 'velocity-based';
type = 'velocity-based&heelstrike';
% type = 'regular';

figure(8); fig8

% coefficients
disp(['Coefficient for 3D whole-body collision: ', num2str(Wcoll_3Dstats.Coefficients.Estimate(1)), ' +- ',  num2str(Wcoll_3Dstats.Coefficients.SE(1))])
disp(['Coefficient for 3D soft tissue collision: ', num2str(Wsoftcoll_3Dstats.Coefficients.Estimate(1)), ' +- ',  num2str(Wsoftcoll_3Dstats.Coefficients.SE(1))])
disp(['Coefficient for 3D whole-body stride: ', num2str(Wbody_neg_3Dstats.Coefficients.Estimate(2)), ' +- ',  num2str(Wbody_neg_3Dstats.Coefficients.SE(2))])
disp(['Offset for 3D whole-body stride: ', num2str(Wbody_neg_3Dstats.Coefficients.Estimate(1)*mean(MgL(subjs))), ' +- ',  num2str(Wbody_neg_3Dstats.Coefficients.SE(1)*mean(MgL(subjs)))])

% standard deviations
SD = Wcoll_3Dstats.Coefficients.SE * sqrt(Wcoll_3Dstats.NumObservations)
SD = Wsoftcoll_3Dstats.Coefficients.SE * sqrt(Wsoftcoll_3Dstats.NumObservations)
SD = Wbody_neg_3Dstats.Coefficients.SE(2) * sqrt(Wbody_neg_3Dstats.NumObservations)


% p values
disp(['p-value for 3D whole-body collision: ', num2str(Wcoll_3Dstats.Coefficients.pValue(1))])
disp(['p-value for 3D soft tissue collision: ', num2str(Wsoftcoll_3Dstats.Coefficients.pValue(1))])
disp(['p-value for 3D whole-body stride: ', num2str(Wbody_neg_3Dstats.Coefficients.pValue(2))])
disp(' ')

disp(['R2 for 3D whole-body collision: ', num2str(RMSE_coll(2))]) 
disp(['R2 for 3D soft tissue collision: ', num2str(RMSE_softcoll(2))]) 
disp(['R2 for 3D whole-body stride: ', num2str(RMSE_body(2))]) 

disp(' ')


return
%%
for i = 1:3
    subplot(1,3,i);
    xlim([0 .45])
        xlim([0 .41])
    ylim([0 .8]);     ylim([0 .75])
    zlim([0 .3])
        zlim([0 .15])
    set(gca,'xtick',[0:.1:.8])
end

%% Figure 8D: average speed vs. impact speed
fig8D

%% Figure 9: work 2D plots
disp('Preparing Figure 9')
figure(9); fig9
stats9

for i = 1:4
    disp(titles{i})
    disp(['Rigid-body relative to whole-body: ', num2str(rigid_rels(i))])
    disp(['R2 for 2D whole-body collision: ', num2str(R2(i).coll)])
    disp(['R2 for 2D soft tissue collision: ', num2str(R2(i).soft)])
    disp(['R2 for 2D whole-body stride: ', num2str(R2(i).stride)])
    disp(' ')
end

disp([titles{3}, ' corrected'])
disp(['R2 for 2D whole-body collision: ', num2str(R2_alt.coll)])
disp(['R2 for 2D soft tissue collision: ', num2str(R2_alt.soft)])
disp(['R2 for 2D whole-body stride: ', num2str(R2_alt.stride)])

