function [f,f1, R2] = relation_between_speed_and_steplength(i,show)

if nargin<2
    show = 0;
end

% addpath(genpath('C:\Users\Tim\OneDrive - University of Calgary\shared 5steps data'))
% cd('C:\Users\Tim\OneDrive - University of Calgary\soft tissue paper')
load('Wsoft.mat','Tstride','vcom_hs','vwalks')
titles = {'Preferred walking', 'Fixed frequency', 'Fixed speed', 'Fixed step length'};

leglength = [0.8941 0.9398 1.04  0.8636 0.8636 0.9398 0.9906 0.99 0.9398 0.876];

% make dimensionless
SL_m = (Tstride/2) .* repmat(vwalks', 1, 10, 2); % meters
SL = SL_m ./ repmat(leglength, size(SL_m,1), 1, size(SL_m,3));
SP = vcom_hs ./ repmat(sqrt(9.81 * leglength), size(vcom_hs,1),1,size(vcom_hs,3));    
vwalks_dimless = repmat(vwalks(:),1,10) ./ repmat(sqrt(9.81 * leglength), size(vcom_hs,1),1);

% average between legs
SP = mean(SP,3);
SL = mean(SL,3);

eqs = {'b*x^m', 'b*x', 'a+b*x', 'a+b*x'};
Start(1).Point = [1 .5];
Start(2).Point = [1];
Start(3).Point = [0.5 .1];
Start(4).Point = [0.5 .1];

    if i == 1, idx = [2 5 8 11 14 16 20 32];
    elseif i == 2, idx = [3 6 9 10 13 20 31];
    elseif i == 3, idx = [17:20, 23:25];
    elseif i == 4, idx = [1 4 7 12 15 20 33];
    end

    SPs = SP(idx,:); 
    SLs = SL(idx,:);
    
    
    if i == 3, x = SLs; y = SPs;
    else,      y = SLs; x = SPs;
    end
 
    xfin = x(isfinite(x)); 
    yfin = y(isfinite(y));  


   f = fit(xfin(1:numel(xfin)),yfin(1:numel(yfin)), eqs{i}, 'StartPoint', Start(i).Point);

    
    if show
        figure(98);
        subplot(2,2,i)
        plot(vwalks_dimless(idx,:), SP(idx,:),'.','markersize',10); hold on
        plot([0 1], [0 1], 'k-')
        xlabel('Intended speed (dim. less)'); ylabel('Collision speed (dim. less)')
        title(titles{i})
        
        figure(99);
        subplot(2,2,i)
        plot(SLs, SPs,'.','markersize',10); hold on
        set(gca,'ColorOrderIndex',1)
        plot(SLs, vwalks_dimless(idx,:),'x','linewidth',2); hold on
        xlabel('Step length (dim. less)'); ylabel('Speed (dim. less)')
        title(titles{i})
        
        figure(100)
        subplot(2,2,i)
        plot(0:.01:1.2, f(0:.01:1.2),'k','linewidth',2); hold on
        plot(x,y,'.','markersize',10); hold on
        ylabel('Step length (dim. less)'); xlabel('Speed (dim. less)');
        axis([0 1.2 0 1.2])
        title(titles{i})
        
        subplot(2,2,3)
        xlabel('Step length (dim. less)'); ylabel('Speed (dim. less)');
       
    end
    
% figure(101)
dv = SP - vwalks_dimless;
SLfin = SL(isfinite(SL(:)));
dvfin = dv(isfinite(SL(:)));

f1 = fit(SLfin, dvfin, 'b*x^2', 'StartPoint', 1);
% plot(f1, SLfin, dvfin); 

%% R-squared
% RES = sum(sqrt((yfin(:) - f(xfin(:))).^2));
% TOT = sum(sqrt((yfin(:) - mean(yfin)).^2));

RES = sum(sqrt((dvfin(:) - f1(SLfin(:))).^2));
TOT = sum(sqrt((dvfin(:) - mean(dvfin)).^2));

R2 = 1 - RES/TOT;
end
