%% constant step frequency
mTstride = mean(Tstride,3,'omitnan'); % average across legs
trials = [3 6 9 10 13 26 31];
speed_or_steplength = [.7 .9 1.1 1.6 1.8 1.25 1.4];
[~,i] = sort(speed_or_steplength);
k = 0;


normfac = [parms.g .* parms.leglength;  parms.g .* sqrt(parms.g) .* sqrt(parms.leglength)];

n = length(trials);
scolors = ncolors(round(linspace(1,length(ncolors),n)),:);

for trial = trials(i)
    k = k+1;
    
   
    [p,h] = figure3_func(trial, mean(mTstride(trial,:),'omitnan'),normfac,scolors(k,:));
    set(h,'linewidth',1.5);
end

%% make nice (try to reproduce Zelik & Kuo 2010 figure 2)
for i = 1:9
    subplot(3,3,i)
    plot([0 100], [0 0], 'k-')
    xlim([0 2]);
    set(gca,'xtick', [0 1 2])
    
    if i < 4
        yyaxis('right'); ylim([-80 30]) ;  set(gca,'YColor','k');
        yyaxis('left'); ylim([-80 30])  ; set(gca,'YColor','k');
    elseif i > 6
        yyaxis('right'); ylim([-4 6]/gsgsL);  set(gca,'YColor','k');
        yyaxis('left'); ylim([-4 6])  ; set(gca,'YColor','k');
        
    else
        yyaxis('right'); ylim([-1 2]/gL);  set(gca,'YColor','k');
        yyaxis('left'); ylim([-1 2]) % moment
        
    end
    
    if i == 1, title('Ankle'); ylabel('Angle (deg)')
    elseif i == 2, title('Knee')
    elseif i == 3, title('Hip')
    elseif i == 4, ylabel('Torque (N-m kg^{-1})')
    elseif i == 7, ylabel('Power (W kg^{-1})')
    elseif i == 8, xlabel('Time (s)')
    end          
end


%% xrange
for i = 1:9
    subplot(3,3,i)
    set(gca,'xtick', [0:.5:2])
    xlim([0 1.11])
end

% %% additional axes
% ylim([-4.5 2])
% yyaxis('right')
% ylim([-4.5 2]/gsgsL)

function [p,h] = figure3_func(trial, stridetime, normfac, color)
% subjects = [1:3, 5:10]; trials = 1:33;
% extract_anklekin(datafolder, subjects, trials)
% extract_kneekin(datafolder, subjects, trials)
% extract_hipkin(datafolder, subjects, trials)

A = load('Ankle_PG.mat');
K = load('Knee_PG.mat');
H = load('Hip_PG.mat');

% make dimensionless
A.Mjoint_pc = A.Mjoint_pc ./ repmat(normfac(1,:), 201,1,33,2);
K.Mjoint_pc = K.Mjoint_pc ./ repmat(normfac(1,:), 201,1,33,2);
H.Mjoint_pc = H.Mjoint_pc ./ repmat(normfac(1,:), 201,1,33,2);

A.Pjoint_pc = A.Pjoint_pc ./ repmat(normfac(2,:), 201,1,33,2);
K.Pjoint_pc = K.Pjoint_pc ./ repmat(normfac(2,:), 201,1,33,2);
H.Pjoint_pc = H.Pjoint_pc ./ repmat(normfac(2,:), 201,1,33,2);

%% average between legs
Aank = mean(A.Ajoint_pc,4,'omitnan');
Mank = mean(A.Mjoint_pc,4,'omitnan');
Pank = mean(A.Pjoint_pc,4,'omitnan');

Akne = mean(K.Ajoint_pc,4,'omitnan');
Mkne = mean(K.Mjoint_pc,4,'omitnan');
Pkne = mean(K.Pjoint_pc,4,'omitnan');

Ahip = mean(H.Ajoint_pc,4,'omitnan');
Mhip = mean(H.Mjoint_pc,4,'omitnan');
Phip = mean(H.Pjoint_pc,4,'omitnan');
% load('parms.mat');

time = linspace(0, stridetime, length(Aank));

%% correcting signs
% for some reason we have to correct ankle and hip angle and moments as to
% be consistent with extension/flexion definition while this is not
% necessary for knee. don't understand TZ 2020-06-08
Aank = -Aank;
Mank = -Mank;
Ahip = -Ahip;
Mhip = -Mhip;

% %% average between legs
% COMpower = mean(Pcom,4,'omitnan');

%% angles
p(1) = subplot(331);   yyaxis('right'); 
h(1) = plot(time, mean(Aank(:,:,trial),2,'omitnan'),'-','color',color); hold on

p(2) = subplot(332);   yyaxis('right'); 
h(2) = plot(time, mean(Akne(:,:,trial),2,'omitnan'),'-','color',color); hold on

p(3) = subplot(333);   yyaxis('right'); 
h(3) = plot(time, mean(Ahip(:,:,trial),2,'omitnan'),'-','color',color); hold on

%% moments
p(4) = subplot(334);   yyaxis('right'); 
h(4) = plot(time, mean(Mank(:,:,trial),2,'omitnan'),'-','color',color); hold on

p(5) = subplot(335);   yyaxis('right'); 
h(5) = plot(time, mean(Mkne(:,:,trial),2,'omitnan'),'-','color',color); hold on

p(6) = subplot(336);   yyaxis('right'); 
h(6) = plot(time, mean(Mhip(:,:,trial),2,'omitnan'),'-','color',color); hold on

%% powers
p(7) = subplot(337);   yyaxis('right'); 
h(7) = plot(time, mean(Pank(:,:,trial),2,'omitnan'),'-','color',color); hold on

p(8) = subplot(338);   yyaxis('right'); 
h(8) = plot(time, mean(Pkne(:,:,trial),2,'omitnan'),'-','color',color); hold on

p(9) = subplot(339);   yyaxis('right'); 
h(9) = plot(time, mean(Phip(:,:,trial),2,'omitnan'),'-','color',color); hold on

end


