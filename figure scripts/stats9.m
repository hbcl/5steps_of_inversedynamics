%% rigid body relative to whole-body
% rigid_rel = mean(Wjointmat(SPs<.5) ./ Wcollmat(SPs<.5), 'omitnan');
idx = parms.idx;
clear R2

for i = 1:4
    ycoll = Wcollmat(idx(i):idx(i+1)-1,:);
    yjoint = Wjointmat(idx(i):idx(i+1)-1,:);
    
    rigid_rels(i) = mean(yjoint(:) ./ ycoll(:),'omitnan');
    
end

%% Some stats
for i = 1:4
    % R-squared
    ycoll_act = Wcollmat(idx(i):idx(i+1)-1,:);
    ysoft_act = Wsoftmat(idx(i):idx(i+1)-1,:);
    ystride_act = Wstridemat(idx(i):idx(i+1)-1,:);
    
    if i == 1 || i == 4
        x = SPs(idx(i):idx(i+1)-1,:);
    else
        x = SLs(idx(i):idx(i+1)-1,:);
    end
    
    x = x(:);
    
    % theoretical
    ycoll_pred = Wcoll_3Dstats.Coefficients.Estimate * 1/8 * corfac(i) .* x.^p(i);
    ysoft_pred = Wsoftcoll_3Dstats.Coefficients.Estimate *  1/8 * corfac(i) .* x.^p(i);
    ystride_pred = Wbody_neg_3Dstats.Coefficients.Estimate(2) *  1/8 * corfac(i) .* x.^p(i) + Wbody_neg_3Dstats.Coefficients.Estimate(1);
    
    % collision
    RES = sum(((ycoll_act(:)-ycoll_pred(:)).^2),'omitnan');
    TOT = sum(((ycoll_act(:)-mean(ycoll_act(:),'omitnan')).^2),'omitnan');
    R2(i).coll = 1 - RES/TOT;
    
    % soft collision
    RES = sum(((ysoft_act(:)-ysoft_pred(:)).^2),'omitnan');
    TOT = sum(((ysoft_act(:)-mean(ysoft_act(:),'omitnan')).^2),'omitnan');
    R2(i).soft = 1 - RES/TOT;
    
    % stride
    RES = sum(((ystride_act(:)-ystride_pred(:)).^2),'omitnan');
    TOT = sum(((ystride_act(:)-mean(ystride_act(:),'omitnan')).^2),'omitnan');
    R2(i).stride = 1 - RES/TOT;
end

%% alternative
i = 3;
x = SLs(idx(i):idx(i+1)-1,:);
x = x(:);

ycoll_act = Wcollmat(idx(i):idx(i+1)-1,:);
ysoft_act = Wsoftmat(idx(i):idx(i+1)-1,:);
ystride_act = Wstridemat(idx(i):idx(i+1)-1,:);
    
ycoll_pred_alt =  Wcoll_3Dstats.Coefficients.Estimate * (sqrt(corfac(3)) + f1(x)) .^2 .* x.^2;
ysoft_pred_alt = Wsoftcoll_3Dstats.Coefficients.Estimate * (sqrt(corfac(3)) + f1(x)) .^2 .* x.^2;
ystride_pred_alt = Wbody_neg_3Dstats.Coefficients.Estimate(2) * (sqrt(corfac(3)) + f1(x)) .^2 .* x.^2 + +Wbody_neg_3Dstats.Coefficients.Estimate(1);        

RES = sum(((ycoll_act(:)-ycoll_pred_alt(:)).^2),'omitnan');
TOT = sum(((ycoll_act(:)-mean(ycoll_act(:),'omitnan')).^2),'omitnan');
R2_alt.coll = 1 - RES/TOT;

RES = sum(((ysoft_act(:)-ysoft_pred_alt(:)).^2),'omitnan');
TOT = sum(((ysoft_act(:)-mean(ysoft_act(:),'omitnan')).^2),'omitnan');
R2_alt.soft = 1 - RES/TOT;

RES = sum(((ystride_act(:)-ystride_pred_alt(:)).^2),'omitnan');
TOT = sum(((ystride_act(:)-mean(ystride_act(:),'omitnan')).^2),'omitnan');
R2_alt.stride = 1 - RES/TOT;
