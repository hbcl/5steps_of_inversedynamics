%% Run the function for different terms
symb = {'o','s','d','v'};
parms.modelString = 'Work ~ -1 + GaitPar';

% Joint: light color
[~,~,Wjointmat] = make_2D_plot(Wjointcoll,parms,'Nofit',1,'average','o');

% Soft: main color
[~,~,Wsoftmat] = make_2D_plot(Wsoftcoll,parms,'Dofit',2,'average','s');

% Collision: dark color
[~,~,Wcollmat] = make_2D_plot(Wbodycoll,parms,'Dofit',3,'average','d');

% Total negative: darkest color 
[SLs,SPs,Wstridemat] = make_2D_plot(Wbody_neg,parms,'Nofit',4,'average','v');

% Figure 3: Soft collision
% figure(3)
% Wsoft_stats = make_2D_plot(Wsoft,parms,'Dofit');

%% Slope for preferred SP-SL curve
% figure
idx = parms.idx;
prefSLs = SLs(idx(1):idx(2)-1,:);
prefSPs = SPs(idx(1):idx(2)-1,:);
% plot(prefSLs, prefSPs,'o')

[varargout] = anyfit(prefSPs'.^.42,prefSLs','offset','none');
close

%% correct the coefficients
corfac = [varargout.coef .56^2 .41^2 .74^2];
[f, f1] = relation_between_speed_and_steplength(1);
  
%% Regular slices
idx = parms.idx;
xgrid = 0:.01:2.0;
xgrid = xgrid(:);
p = [2.84 4 2 2];

for i = 1:4
    subplot(2,2,i); yyaxis('right') 

    plot(xgrid, (Wcoll_3Dstats.Coefficients.Estimate) * 1/8 * corfac(i) .* xgrid.^p(i) ,'k-', 'linewidth',2)
    plot(xgrid, (Wsoftcoll_3Dstats.Coefficients.Estimate) * 1/8 *corfac(i) .* xgrid.^p(i) ,'k-', 'linewidth',2)
    plot(xgrid, (Wbody_neg_3Dstats.Coefficients.Estimate(2)) * 1/8 *corfac(i) .* xgrid.^p(i) +Wbody_neg_3Dstats.Coefficients.Estimate(1),'k-', 'linewidth',2)
    
end

%% Alternative slices
SLpref = 1.15 * xgrid.^.52;

% subplot(221);
% plot(xgrid, (Wcoll_3Dstats.Coefficients.Estimate) .* (xgrid + f1(SLpref)) .^2 .* SLpref.^2 ,'b--', 'linewidth',2)
% plot(xgrid, (Wsoftcoll_3Dstats.Coefficients.Estimate) .* (xgrid + f1(SLpref)) .^2 .* SLpref.^2 ,'b--', 'linewidth',2)
% plot(xgrid, (Wbody_neg_3Dstats.Coefficients.Estimate(2)) .* (xgrid + f1(SLpref)) .^2 .* SLpref.^2 +Wbody_neg_3Dstats.Coefficients.Estimate(1),'b--', 'linewidth',2)

% subplot(222);
% plot(xgrid, (Wcoll_3Dstats.Coefficients.Estimate) .* (sqrt(corfac(2))*xgrid + f1(xgrid)) .^2 .* xgrid.^2 ,'r-', 'linewidth',2)
% plot(xgrid, (Wsoftcoll_3Dstats.Coefficients.Estimate) .* (sqrt(corfac(2))*xgrid + f1(xgrid)) .^2 .* xgrid.^2 ,'r-', 'linewidth',2)
% plot(xgrid, (Wbody_neg_3Dstats.Coefficients.Estimate(2)) .* (sqrt(corfac(2))*xgrid + f1(xgrid)) .^2 .* xgrid.^2 +Wbody_neg_3Dstats.Coefficients.Estimate(1),'r-', 'linewidth',2)

subplot(223);
plot(xgrid, (Wcoll_3Dstats.Coefficients.Estimate) .* (sqrt(corfac(3)) + f1(xgrid)) .^2 .* xgrid.^2 ,'r-', 'linewidth',2)
plot(xgrid, (Wsoftcoll_3Dstats.Coefficients.Estimate) .* (sqrt(corfac(3)) + f1(xgrid)) .^2 .* xgrid.^2 ,'r-', 'linewidth',2)
plot(xgrid, (Wbody_neg_3Dstats.Coefficients.Estimate(2)) .* (sqrt(corfac(3)) + f1(xgrid)) .^2 .* xgrid.^2 +Wbody_neg_3Dstats.Coefficients.Estimate(1),'r-', 'linewidth',2)

%% make nice
% names = {'COM collision work: 2D plot', 'Soft tissue collision work: 2D plot','Soft tissue net work: 2D plot'};
titles = {'Preferred walking', 'Fixed frequency', 'Fixed speed', 'Fixed step length'};
if parms.dimensionless, units = {'dimensionless','dimensionless'};
else, units = {'m/s','m'};
end


% set(gcf,'name',names{i})

for j = 1:4
    subplot(2,2,j);
    title(titles{j})

    xlim([0 .8])

    yyaxis right
    set(gca,'YCOlor', 'k')
    ylim([-.02 .15])
    ylabel('Dissipation per stride (dim. less)');

    yyaxis left
    set(gca,'YCOlor', 'k')
    ylim([-.02 .15]*mean(MgL))

    plot([0 10], [0 0], 'k-')    
    ylabel('Dissipation per stride (J)'); xlabel(['Speed (',units{1},')'])

    if j == 2 || j == 3
    xlabel(['Step length (',units{2},')'])
    xlim([0 1.4])
    end

end


set(gcf,'units','normalized','Position',[.2 .2 .6 .6])


