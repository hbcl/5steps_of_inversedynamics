
[SLs, SPs, ~, SPsav, dVs] = get_Wmat(Wbodycoll, parms);
idx = parms.idx;
color = parms.colors(:,:,2);
fillcolor = parms.colors(:,:,1);
fitcolor = parms.colors(:,:,3);
xfine = 0:.01:1.2; xfine = xfine(:);

%% delta velocity versus step length
figure
plot(SLs(:), dVs(:)/180*pi,'.','markersize',10); hold on
xlim([0 1.2]); xlabel('Step length (dim. less)');
ylim([0 .8]); ylabel('\delta_{vel} (rad)')

% outlier
plot(SLs(dVs == max(dVs(:))), max(dVs(:)/180*pi),'r.','markersize',15)

x = 0:.1:1.2;
plot(x, 0.307*x + 0.088,'k','linewidth',2)

%% relation speed and step length
% figure(101)
id = idx(3):(idx(4)-1);

dv = SPs(id,:) - SPsav(id,:);
SL = SLs(id,:);

SLfin = SL(isfinite(dv));
dvfin = dv(isfinite(dv));

f1 = fit(SLfin(:), dvfin(:), 'b*x^2', 'StartPoint', 1);

% R-squared
RES = sum(sqrt((dvfin(:) - f1(SLfin(:))).^2));
TOT = sum(sqrt((dvfin(:) - mean(dvfin)).^2));

R2 = 1 - RES/TOT;

%% figure
figure;
plot(SL(:), dv(:),'ko'); hold on
plot(0:.1:1.2, f1(0:.1:1.2),'r')
xlabel('Step length (dim. less)');
ylabel('Difference in speed (dim. less)');
legend('Using COM event "vdown"', ['Best quadratic fit, R2 = ', num2str(R2)])

%% Slope for preferred SP-SL curve
% figure
prefSLs = SLs(idx(1):idx(2)-1,:);
prefSPs = SPs(idx(1):idx(2)-1,:);
% plot(prefSLs, prefSPs,'o')

[varargout] = anyfit(prefSPs'.^.42,prefSLs','offset','none');
close

%% plot relationships ideal 
figure;
plot(varargout.coef*xfine.^.42, xfine, 'color',fitcolor(1,:),'linewidth',2); hold on
plot(xfine/mean(parms.prefFQ_dl),xfine , 'color',fitcolor(2,:),'linewidth',2)
plot(xfine,repmat(mean(parms.prefSP_dl), size(xfine)) , 'color',fitcolor(3,:),'linewidth',2)
plot(repmat(mean(parms.prefSL_dl), size(xfine)), xfine, 'color',fitcolor(4,:),'linewidth',2)

%% plot actual relationship for variable step length conditions
plot(xfine/mean(parms.prefFQ_dl), (xfine+f1(xfine/mean(parms.prefFQ_dl))), 'color',fitcolor(2,:),'linewidth',2)
plot(xfine, (repmat(mean(parms.prefSP_dl), size(xfine))+f1(xfine)), 'color',fitcolor(3,:),'linewidth',2)

%% Plot dots
symb = {'o','s','v','d'};
for i = 1:4
    SLsvec = SLs(idx(i):idx(i+1)-1,:); SPsvec = SPs(idx(i):idx(i+1)-1,:);
   h = plot(SLsvec(:),SPsvec(:));
          set(h,'linestyle','none','marker',symb{i},'color',color(i,:),'markersize',10,'markerfacecolor',fillcolor(i,:)); hold on
end
   
axis([0 1.2 0 .8])
set(gca,'Xtick', 0:.3:1.2,'Ytick',0:.2:.8)
xlabel('');ylabel(''); 

set(gcf,'units','normalized','position',[.3 .3 .3 .5])

%% Subpanel
figure;
symb = {'o','s','v','d'};

xvec = 0:.01:1.2;

plot(xvec, f1(xvec),'k'); hold on

for i = 1:4
    SPsvec1 = SPsav(idx(i):idx(i+1)-1,:); SPsvec2 = SPs(idx(i):idx(i+1)-1,:); SLsvec = SLs(idx(i):idx(i+1)-1,:); 
   h = plot(SLsvec(:), SPsvec2(:)-SPsvec1(:));
          set(h,'linestyle','none','marker',symb{i},'color',color(i,:),'markersize',7,'markerfacecolor',fillcolor(i,:)); hold on
end

set(gcf,'units','normalized','position',[.3 .3 .3 .5])
