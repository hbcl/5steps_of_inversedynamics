%% average between legs and participants
COMpower = mean(Pcom_pc,4,'omitnan');
PERpower = mean(Pper_pc,4,'omitnan'); 
mTstride = mean(Tstride,3,'omitnan'); % average across legs
Jointpower = mean(Pjoint_pc,4,'omitnan');
Pjoint = squeeze(mean(Jointpower,2,'omitnan'));
Pcom = squeeze(mean(COMpower,2,'omitnan'));
Ptot = Pcom + squeeze(mean(PERpower,2,'omitnan'));

mTstride = mean(Tstride,3,'omitnan'); % average across legs

%% Soft tissue figure
panels = [1 5 9; 2 6 10; 3 7 11; 4 8 12];

titles = {'Preferred walking', 'Fixed frequency', 'Fixed speed', 'Fixed step length'};
for condition = 1:4

if condition == 2
    icFR = [3 6 9 10 13 26 31];
    scFR = [.7 .9 1.1 1.6 1.8 1.25 1.4];
    [v,i] = sort(scFR);
    trials = icFR(i);
elseif condition == 4
    icSL = [1 4 7 12 15 20 33];
    scSL = [.7 .9 1.1 1.6 1.8 1.25 1.4]; % speeds
    [v,i] = sort(scSL);
    trials = icSL(i);
elseif condition == 1
    iPREF = [2 5 8 11 14 16 20 32];
    vPRFs = [.7 .9 1.1 1.6 1.8 2.0 1.25 1.4];
    [v,i] = sort(vPRFs);
    trials = iPREF(i);        
elseif condition == 3
    icSP = [17:20, 23:25];
    fcSL = [.7 .8 .9 1 1.1 1.2 1.3];
    [~,i] = sort(fcSL,'descend');
     trials = icSP(i);        
end

k = 0;
n = length(trials);
scolors = ncolors(round(linspace(1,length(ncolors),n)),:);
yrange = [-6.2 7.2];

for trial = trials
    k = k+1;
    time = linspace(0, mean(mTstride(trial,:),'omitnan'), 101);
    
    subplot(3,4,panels(condition,1)); yyaxis left;
    plot(time, Ptot(:,trial), '-','color', scolors(k,:)); hold on
        axis tight; ylim(yrange)
        set(gca,'ytick',[-6:2:6])
        title({titles{condition}, 'Whole-body'})
        yyaxis right; ylim(yrange/gsgsL)
        
    if condition == 1, ylabel('Work rate (W/kg)')
    end
    
    subplot(3,4,panels(condition,2)); yyaxis left;
    plot(time, Pjoint(:,trial), '-','color', scolors(k,:)); hold on
        axis tight; ylim(yrange)
        set(gca,'ytick',[-6:2:6])
        
        title('Rigid body')
        yyaxis right; ylim(yrange/gsgsL)
        
    if condition == 1, ylabel('Work rate (W/kg)')
    end
    
    subplot(3,4,panels(condition,3)); yyaxis left;
    plot(time, Ptot(:,trial) - Pjoint(:,trial), '-','color', scolors(k,:)); hold on
        axis tight; ylim(yrange)
        set(gca,'ytick',[-6:2:6])
    xlabel('Time (s)')
    
    title('Soft tissue')
    yyaxis right; ylim(yrange/gsgsL)
    if condition == 1, ylabel('Work rate (W/kg)')
    end

end
%
set(gcf,'units','normalized','position', [.1 0 .9 .9])
end