%% Run the function for different terms
parms.modelString = 'Work ~ -1 + GaitPar';

%% 3D plots
subplot(131);
[Wcolln_mat, RMSE_coll, Wcoll_3Dstats] = make_3D_plot(Wbodycoll,parms,'Dofit', parms.colors(:,:,2),type);
% exportgraphics(gcf, 'Collision.pdf','ContentType','vector')

% Soft tissue collision
parms.show = 0;
subplot(132);
[Wsoftcolln_mat, RMSE_softcoll, Wsoftcoll_3Dstats] = make_3D_plot(Wsoftcoll,parms,'Dofit', parms.colors(:,:,2),type);
% exportgraphics(gcf, 'Soft_Collision.pdf','ContentType','vector')

% Whole body negative
parms.modelString = 'Work ~ GaitPar';
subplot(133);
[Wbody_neg_mat, RMSE_body, Wbody_neg_3Dstats] = make_3D_plot(Wbody_neg,parms,'Dofit', parms.colors(:,:,2),type);
% [Wbody_neg_mat, RMSE_body, Wbody_neg_3Dstats] = make_3D_plot(-Wbody_pos,parms,'Dofit', parms.colors(:,:,2),type);

% exportgraphics(gcf, 'Negative.pdf','ContentType','vector')

% % Soft tissue net
% parms.modelString = 'Work ~ GaitPar';
% Wsoftn_mat = make_3D_plot(Wsoft,parms,'Dofit', colors(:,:,1));
% set(gcf,'name','Soft tissue net work')
% axis([0 1.2 0 .7 -.005 .15])
% exportgraphics(gcf, 'Soft_Net.pdf','ContentType','vector')

titles = {'Whole-body collision', 'Soft tissue collision', 'Whole-body stride'};

for i = 1:3
    subplot(1,3,i);
    set(gca,'Xtick', 0:.3:1.2,'Ytick',0:.2:.8);
    xlabel('Step length');ylabel('Speed'); zlabel('Dissipation');
    axis([0 1.2 0 .8 -.005 .15])
    axis([0 .45 0 .8 0 .3])
    axis([0 .45 0 .8 0 .3])
    view(-45,20)
    title(titles{i});
end

set(gcf,'units','normalized','position', [.1 .3 .9 .4])

    