%% Average between legs
PRank = mean(A.Pjoint_pc,4,'omitnan');
PRkne = mean(K.Pjoint_pc,4,'omitnan');
PRhip = mean(H.Pjoint_pc,4,'omitnan');
PTank = mean(A.Tjoint_pc,4,'omitnan');
PTkne = mean(K.Tjoint_pc,4,'omitnan');
PThip = mean(H.Tjoint_pc,4,'omitnan');

% colors = parms.colors;

pg_mocap = 0:.5:100;
pg_COM = 0:1:100;

%% Sum transpower and rotational power
Pank = PRank + PTank;
Pkne = PRkne + PTkne;
Phip = PRhip + PThip;

% Determine rigid body power and whole-body power
Prigid = Pank + Pkne + Phip;
Pbody = mean(Pcom_pc,4) + mean(Pper_pc,4);

%% Main figure
trial = 20;

subplot(131);
plot(pg_mocap, mean(Pank(:,:,trial),2,'omitnan'),'color',parms.colors(1,:),'linewidth',2); hold on
plot(pg_mocap, mean(Pkne(:,:,trial),2,'omitnan'),'color',parms.colors(2,:),'linewidth',2); hold on
plot(pg_mocap, mean(Phip(:,:,trial),2,'omitnan'),'color',parms.colors(3,:),'linewidth',2); hold on
plot(pg_mocap, mean(Prigid(:,:,trial),2,'omitnan'),'k:','linewidth',2); hold on
legend('ankle','knee','hip','rigid')

subplot(132);
plot(pg_COM, mean(mean(Pcom_pc(:,:,trial,:),4), 2, 'omitnan'),'k--','linewidth',2); hold on
plot(pg_COM, mean(mean(Pper_pc(:,:,trial,:),4), 2, 'omitnan'),'k.','linewidth',2);
plot(pg_COM, mean(mean(Pbody(:,:,trial,:),4), 2, 'omitnan'),'k','linewidth',2);

subplot(133);
plot(pg_COM, mean(Pbody(:,:,trial),2,'omitnan'),'k-','linewidth',2); hold on
plot(pg_mocap, mean(Prigid(:,:,trial),2,'omitnan'),'k:','linewidth',2); hold on

titles = {'Rigid body', 'Whole-body', 'Soft tissue'};

for s = 1:3
    subplot(1,3,s);
    ylim([-2 5])
    title(titles{s})
    xlabel('Percentage gait cycle');
    ylabel('Work rate (W/kg)')
    
end

set(gcf,'units','normalized','position', [.1 .3 .6 .3])

% %% Additional figure: compare rigid with alternative and look at work
% figure;
% subplot(211);
% Psoft = mean(Pbody(:,:,20),2,'omitnan') - mean(sum(Prigid(:,:,20,:),4),2,'omitnan');
% 
% plot(Psoft); hold on
% plot(mean(Pbody(:,:,20),2,'omitnan')); hold on
% plot(mean(sum(Prigid(:,:,20,:),4),2,'omitnan'))
% 
% plot(mean(mean(Pjoint_pc(:,:,20,:),4),2,'omitnan'))
% legend('Soft','Body','Rigid','Rigid alt')
% 
% subplot(212);
% plot(cumtrapz(Psoft)); hold on
% plot(cumtrapz(mean(Pbody(:,:,20),2,'omitnan')));
% plot(cumtrapz(mean(sum(Prigid(:,:,20,:),4),2,'omitnan')));
% plot(cumtrapz(mean(mean(Pjoint_pc(:,:,20,:),4),2,'omitnan')));
% legend('Soft','Body','Rigid','Rigid alt')

% comparison ballpark numbers: 
% - Wsoft is about 6.5 W/kg * %gc during collision, that is
% roughly 0.065 J/kg (since 1% gait cycle = 0.01 s), which is roughly 4.9 J
% (assuming 75 kg), 
% - Wbody is about 11 W/kg -> 0.011 J/kg -> 8 J
% - Wrigid is about 4.5 W/kg

%and this is consistent with soft tissue colision phase work for 1.25 m/s

