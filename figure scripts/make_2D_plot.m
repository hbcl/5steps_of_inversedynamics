function[SLs,SPs,Wmat,stats] = make_2D_plot(Wterm, parms, optional_fit, nplot, speedtype, symb)

[SLs, SPs, Wmat, SPs_av] = get_Wmat(Wterm, parms);

if nargin > 4
if strcmp(speedtype, 'average') == 1
    SPs = SPs_av;
end
end

if nargin == 2
    optional_fit = nan;
end

if nargin < 4
    nplot = 1;
end

if isfield(parms,'colors')
    colors = parms.colors;
else
    figure;
    colors = get(gca,'colororder');
    close
end

Mg = parms.mass .* parms.g;
MgL = Mg .* parms.leglength;

%% Plot soft tissue work
idx = parms.idx;
if parms.dimensionless,  multfac = mean(MgL);
else, multfac = 1;
end
    % errorbars
    for i = 1:4
        if size(colors,3)>1
            color = colors(i,:,nplot);
        else
            color = colors(nplot,:);
        end
        
        subplot(2,2,i)
%         yyaxis(axside); 
        
        if i == 1 || i == 4, x = SPs;
        else, x = SLs;
        end
        
        if strcmp(optional_fit,'Line') == 1
            [xsort,isort] = sort(mean(x(idx(i):idx(i+1)-1,:),2,'omitnan'));
            y = mean(Wmat(idx(i):idx(i+1)-1,:),2,'omitnan')*multfac;
            ysort = y(isort);
            plot(xsort, ysort,'-','color',color);
        else
            
         errorbar(mean(x(idx(i):idx(i+1)-1,:),2,'omitnan'), mean(Wmat(idx(i):idx(i+1)-1,:),2,'omitnan')*multfac, ...
            std(Wmat(idx(i):idx(i+1)-1,:),1,2,'omitnan')*multfac, std(Wmat(idx(i):idx(i+1)-1,:),1,2,'omitnan')*multfac,...
            std(x(idx(i):idx(i+1)-1,:),1,2,'omitnan'),std(x(idx(i):idx(i+1)-1,:),1,2,'omitnan'),symb,'MarkerSize',8, 'color', color,'markerfacecolor',color); hold on
        end
    end
end




