%% parameters
parms.colors(5,:) = 0;

%% Run the function for different terms
% Minus sign are added because we defined dissipation as positive
parms.dimensionless = 0;
titles = {'Preferred walking', 'Fixed frequency', 'Fixed speed', 'Fixed step length'};
make_2D_plot(-Wjoint,parms,'Nofit',5,'average','o');
make_2D_plot(-Wankle,parms,'Nofit',1,'average','d');
make_2D_plot(-Wknee,parms,'Nofit',2,'average','s');
make_2D_plot(-Whip,parms,'Nofit',3,'average','v');

make_2D_plot(-Wjoint_pos,parms,'Line',5,'average');
make_2D_plot(-Wjoint_neg,parms,'Line',5,'average');
% make_2D_plot(Wjoint_neg+Wjoint_pos,parms,'Nofit',5,'average');

return

%% Make nice
for i = 1:4
    subplot(2,2,i)
    plot([0 10], [0 0], 'k-')
    title(titles{i})
            
    xlim([0 2.1])
    ylim([-42 82])
    
    if i == 2 || i == 3
        xlim([0 1.1]);    xlabel('Step length (m)')
    else
    xlabel('Speed (m\cdots^{-1})')
    end
    
    yyaxis right
    ylim([-42 82]/mean(MgL))
        
    ylabel('Net work per stride (dim. less)')
    yyaxis left
    ylabel('Net work per stride (J)')
    
end


