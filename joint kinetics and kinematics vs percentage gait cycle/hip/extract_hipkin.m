function[] = extract_hipkin(datafolder,subjects,trials)

% Set current directory (cd) to data folder
cd(datafolder)

% Define variables
Mhipl = nan(101,10,33,3);
Mhipr = nan(101,10,33,3);
Ahipl = nan(101,10,33,3);
Ahipr = nan(101,10,33,3);
Phipl = nan(101,10,33,3);
Phipr = nan(101,10,33,3);
Thipl = nan(101,10,33,3);
Thipr = nan(101,10,33,3);
JFhipr_pc= nan(101,10,33,3);
JFhipl_pc= nan(101,10,33,3);
PThipl_pc= nan(101,10,33,3);  
PThipr_pc= nan(101,10,33,3);

for subj = subjects
    load(['p',num2str(subj),'_5steps_data.mat'],'data')
    disp(['subj:', num2str(subj)]);
   
    for trial = trials
        fprintf_r('%i', trial);
        
        if sum(isnan(data(trial).grf(:)))<1
            % Extract hip moment and angle
            Mhipx = [data(trial).jointmoment(:,7) data(trial).jointmoment(:,16)];
            Mhipy = [data(trial).jointmoment(:,8) data(trial).jointmoment(:,17)];
            Mhipz = [data(trial).jointmoment(:,9) data(trial).jointmoment(:,18)];
            
            Ahipx = [data(trial).jointangle(:,7) data(trial).jointangle(:,16)];
            Ahipy = [data(trial).jointangle(:,8) data(trial).jointangle(:,17)];
            Ahipz = [data(trial).jointangle(:,9) data(trial).jointangle(:,18)];
            
            Phipx = [data(trial).jointpower(:,7) data(trial).jointpower(:,16)];
            Phipy = [data(trial).jointpower(:,8) data(trial).jointpower(:,17)];
            Phipz = [data(trial).jointpower(:,9) data(trial).jointpower(:,18)];
            
            %Extract Joint Force
            JFhipz= [data(trial).jointforce.loc(:,9) data(trial).jointforce.loc(:,18)];
            JFhipy=[data(trial).jointforce.loc(:,8) data(trial).jointforce.loc(:,17)];
            JFhipx=[data(trial).jointforce.loc(:,7) data(trial).jointforce.loc(:,16)];
            
            % extract joint translation
            Thipz_start=repmat([data(trial).jointpos.loc(1,9), data(trial).jointpos.loc(1,18)],length(JFhipz),1);
            Thipz = [data(trial).jointpos.loc(:,9) data(trial).jointpos.loc(:,18)]-Thipz_start;
            Thipy_start=repmat([data(trial).jointpos.loc(1,8), data(trial).jointpos.loc(1,17)],length(JFhipy),1);
            Thipy = [data(trial).jointpos.loc(:,8) data(trial).jointpos.loc(:,17)]-Thipy_start;
            Thipx_start=repmat([data(trial).jointpos.loc(1,7), data(trial).jointpos.loc(1,16)],length(JFhipx),1);
            Thipx = [data(trial).jointpos.loc(:,7) data(trial).jointpos.loc(:,16)]-Thipx_start;
            
            %extract translational power 
            PThipx=[data(trial).transpower.loc(:,7),data(trial).transpower.loc(:,16)];
            PThipy=[data(trial).transpower.loc(:,8),data(trial).transpower.loc(:,17)];
            PThipz=[data(trial).transpower.loc(:,9),data(trial).transpower.loc(:,18)];
            
            % Extract grfs
            grfl = data(trial).grf(:,1:3);
            grfr = data(trial).grf(:,4:6);
            
            % Threshold at 20 N
            grfl(grfl(:,3)<20,:) = 0;
            grfr(grfr(:,3)<20,:) = 0;         
           
            % Get heelstrikes
            hsl = invDynGrid_getHS_TO(grfl,grfr, 10);
            hsl = round(unique(hsl)/10) + 1;
   

            % Interpolate to percentage gait cycle
            if sum(isnan(Mhipx(:)))<1 && sum(isnan(Phipx(:)))<1
                Mhipl(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Mhipx(:,1),hsl,101),2,'omitnan');
                Mhipl(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Mhipy(:,1),hsl,101),2,'omitnan');
                Mhipl(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(Mhipz(:,1),hsl,101),2,'omitnan');
                Mhipr(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Mhipx(:,2),hsl,101),2,'omitnan');
                Mhipr(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Mhipy(:,2),hsl,101),2,'omitnan');
                Mhipr(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(Mhipz(:,2),hsl,101),2,'omitnan');
                
                Ahipl(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Ahipx(:,1),hsl,101),2,'omitnan');
                Ahipl(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Ahipy(:,1),hsl,101),2,'omitnan');
                Ahipl(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(Ahipz(:,1),hsl,101),2,'omitnan');
                Ahipr(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Ahipx(:,2),hsl,101),2,'omitnan');
                Ahipr(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Ahipy(:,2),hsl,101),2,'omitnan');
                Ahipr(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(Ahipz(:,2),hsl,101),2,'omitnan');
                
                Phipl(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Phipx(:,1),hsl,101),2,'omitnan');
                Phipl(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Phipy(:,1),hsl,101),2,'omitnan');
                Phipl(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(Phipz(:,1),hsl,101),2,'omitnan');
                Phipr(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Phipx(:,2),hsl,101),2,'omitnan');
                Phipr(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Phipy(:,2),hsl,101),2,'omitnan');
                Phipr(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(Phipz(:,2),hsl,101),2,'omitnan');
                
                JFhipl_pc(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(JFhipx(:,1),hsl,101),2,'omitnan');
                JFhipl_pc(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(JFhipy(:,1),hsl,101),2,'omitnan');
                JFhipl_pc(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(JFhipz(:,1),hsl,101),2,'omitnan');
                
                JFhipr_pc(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(JFhipx(:,2),hsl,101),2,'omitnan');
                JFhipr_pc(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(JFhipy(:,2),hsl,101),2,'omitnan');
                JFhipr_pc(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(JFhipz(:,2),hsl,101),2,'omitnan');
                
                Thipl(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Thipx(:,1),hsl,101),2,'omitnan');
                Thipl(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Thipy(:,1),hsl,101),2,'omitnan');
                Thipl(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(Thipz(:,1),hsl,101),2,'omitnan');
                
                Thipr(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Thipx(:,2),hsl,101),2,'omitnan');
                Thipr(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Thipy(:,2),hsl,101),2,'omitnan');
                Thipr(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(Thipz(:,2),hsl,101),2,'omitnan');
                
                PThipl_pc(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(PThipx(:,1),hsl,101),2,'omitnan');
                PThipl_pc(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(PThipy(:,1),hsl,101),2,'omitnan');
                PThipl_pc(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(PThipz(:,1),hsl,101),2,'omitnan');
        
                PThipr_pc(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(PThipx(:,2),hsl,101),2,'omitnan');
                PThipr_pc(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(PThipy(:,2),hsl,101),2,'omitnan');
                PThipr_pc(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(PThipz(:,2),hsl,101),2,'omitnan');
                
            end
        end
    end
        fprintf_r('reset');
        fprintf('\n')
end

save('Hipkin','Mhipl', 'Mhipr','Ahipl','Ahipr','Phipl','Phipr','Thipl','Thipr', 'JFhipl_pc','JFhipr_pc','PThipl_pc','PThipr_pc')
