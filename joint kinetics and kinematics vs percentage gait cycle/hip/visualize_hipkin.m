function[] = visualize_hipkin(datafolder,subj, trials)

cd(datafolder)
load('Hipkin.mat','Mhipl', 'Mhipr','Ahipl','Ahipr','Phipl','Phipr','Thipr','Thipl')

%% Angle, Moment, Power
for trial = trials
    figure('name',['Rotational Hipkin trial: ', num2str(trial)])

    % left hip moment
    subplot(321)
    plot(Mhipl(:,:,trial)); hold on
    plot(Mhipl(:,subj,trial),'k','linewidth',2); hold on
    plot([0 100], [0 0], 'k-')
    title('Left hip moment (N-m/kg)')
    axis tight; yl = get(gca,'ylim');

    % right hip moment
    subplot(322)
    plot(Mhipr(:,:,trial)); hold on
    plot(Mhipr(:,subj,trial),'k','linewidth',2); hold on
    plot([0 100], [0 0], 'k-')
    title('Right hip moment (N-m/kg)')
    ylim(yl)

    % left hip angle
    subplot(323)
    plot(Ahipl(:,:,trial)); hold on
    plot(Ahipl(:,subj,trial),'k','linewidth',2); hold on
    plot([0 100], [0 0], 'k-')
    title('Left hip angle (deg)')
    axis tight; yl = get(gca,'ylim');

    % right hip angle
    subplot(324)
    plot(Ahipr(:,:,trial)); hold on
    plot(Ahipr(:,subj,trial),'k','linewidth',2); hold on
    plot([0 100], [0 0], 'k-')
    title('Right hip angle (deg)')
    ylim(yl)

    % left hip power
    subplot(325)
    plot(Phipl(:,:,trial)); hold on
    plot(Phipl(:,subj,trial),'k','linewidth',2); hold on
    plot([0 100], [0 0], 'k-')
    title('Left hip power (W/kg)')
    axis tight; yl = get(gca,'ylim');

    % right hip power
    subplot(326)
    plot(Phipr(:,:,trial)); hold on
    plot(Phipr(:,subj,trial),'k','linewidth',2); hold on
    plot([0 100], [0 0], 'k-')
    title('Right hip power (W/kg)')
    ylim(yl)

        % make nice
    for i = 1:6
        subplot(3,2,i)
        xlim([0 100])
        xlabel('Percentage gait cycle');

        if i<5, ylabel('<---- Extension ------------ Flexion ---->')
        end
    end

    legend(['#',num2str(subj)],'#1','#2','etc','location','best')

%     %% Angle, Moment, Power
%     figure('name',['Translational Hipkin trial: ', num2str(trial)])
%     subplot(323)
%     plot(Thipl(:,subj,trial),'k','linewidth',2); hold on
%     plot(Thipl(:,:,trial)); hold on
%     plot([0 100], [0 0], 'k-')
%     title('Left hip translation (m)')
% 
%     subplot(324)
%     plot(Thipr(:,subj,trial),'k','linewidth',2); hold on
%     plot(Thipr(:,:,trial)); hold on
%     plot([0 100], [0 0], 'k-')
%     title('Right hip translation (m)')

end
% %% Positions
% HHl_rel = HHl - Pel;
% HHr_rel = HHr - Pel;
% ThPl_rel = ThPl - Pel;
% ThPr_rel = ThPr - Pel;
% 
% 
% for trial = trials
%     figure('name',['Hipkin trial: ', num2str(trial)])
%     
%     % left hip moment
%     subplot(321)
%     plot(HHl_rel(:,subj,trial),'k','linewidth',2); hold on
%     plot(HHl_rel(:,:,trial)); hold on
%     plot([0 100], [0 0], 'k-')
%     title('Helen-Haley relative to pelvis')
%     axis tight; yl = get(gca,'ylim');
%     
%     % right hip moment
%     subplot(322)
%     plot(HHr_rel(:,subj,trial),'k','linewidth',2); hold on
%     plot(HHr_rel(:,:,trial)); hold on
%     plot([0 100], [0 0], 'k-')
%     title('Helen-Haley relative to pelvis')
%     ylim(yl)
%         
%     % left hip angle
%     subplot(323)
%     plot(ThPl_rel(:,subj,trial),'k','linewidth',2); hold on
%     plot(ThPl_rel(:,:,trial)); hold on
%     plot([0 100], [0 0], 'k-')
%     title('Proximal Thigh relative to pelvis')
%     axis tight; yl = get(gca,'ylim');
%     
%     % right hip angle
%     subplot(324)
%     plot(ThPr_rel(:,subj,trial),'k','linewidth',2); hold on
%     plot(ThPr_rel(:,:,trial)); hold on
%     plot([0 100], [0 0], 'k-')
%     title('Proximal Thigh relative to pelvis')
%     ylim(yl)
%     
%     
%         % left hip angle
%     subplot(325)
%     plot(HHl(:,subj,trial)-ThPl(:,subj,trial),'k','linewidth',2); hold on
%     plot(HHl(:,:,trial)-ThPl(:,:,trial)); hold on
%     plot([0 100], [0 0], 'k-')
%     title('Helen Hayes relative to Proximal Thigh')
%     axis tight; yl = get(gca,'ylim');
%     
%     % right hip angle
%     subplot(326)
%     plot(HHr(:,subj,trial)-ThPr(:,subj,trial),'k','linewidth',2); hold on
%     plot(HHr(:,:,trial)-ThPr(:,:,trial)); hold on
%     plot([0 100], [0 0], 'k-')
%     title('Helen Hayes relative to Proximal Thigh')
%     ylim(yl)
%     
% 
%     
%     % make nice
%     for i = 1:6
%         subplot(3,2,i)
%         xlim([0 100])
%         xlabel('Percentage gait cycle');
%         ylabel('Position (m)')
%         
%     end
%     
%     legend(['#',num2str(subj)],'#1','#2','etc','location','best')
end

