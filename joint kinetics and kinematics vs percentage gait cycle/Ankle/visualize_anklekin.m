function[] = visualize_anklekin(datafolder,subj,trials)

cd(datafolder)
load('Anklekin.mat','Manklel', 'Mankler','Aanklel','Aankler','Panklel','Pankler')

% keyboard
for trial = trials
    figure('name',['Anklekin trial: ', num2str(trial)])
    
    % left ankle moment
    subplot(321)
    plot(Manklel(:,:,trial)); hold on
    plot(Manklel(:,subj,trial),'k','linewidth',2); hold on
    plot([0 100], [0 0], 'k-')
    title('Left ankle moment (N-m/kg)')
    axis tight; yl = get(gca,'ylim');
    
    % right ankle moment
    subplot(322)
    plot(Mankler(:,:,trial)); hold on
    plot(Mankler(:,subj,trial),'k','linewidth',2); hold on   
    plot([0 100], [0 0], 'k-')
    title('Right ankle moment (N-m/kg)')
    ylim(yl)
        
    % left ankle angle
    subplot(323)
    plot(Aanklel(:,:,trial)); hold on
    plot(Aanklel(:,subj,trial),'k','linewidth',2); hold on
    plot([0 100], [0 0], 'k-')
    title('Left ankle angle (deg)')
    axis tight; yl = get(gca,'ylim');
    
    % right ankle angle
    subplot(324)
    plot(Aankler(:,:,trial)); hold on
    plot(Aankler(:,subj,trial),'k','linewidth',2); hold on
    plot([0 100], [0 0], 'k-')
    title('Right ankle angle (deg)')
    ylim(yl)
    
    % left ankle power
    subplot(325)
    plot(Panklel(:,:,trial)); hold on
    plot(Panklel(:,subj,trial),'k','linewidth',2); hold on
    plot([0 100], [0 0], 'k-')
    title('Left ankle power (W/kg)')
    axis tight; yl = get(gca,'ylim');
    
    % right ankle power
    subplot(326)
    plot(Pankler(:,:,trial)); hold on
    plot(Pankler(:,subj,trial),'k','linewidth',2); hold on
    plot([0 100], [0 0], 'k-')
    title('Right ankle power (W/kg)')
    ylim(yl)
    
    % make nice
    for i = 1:6
        subplot(3,2,i)
        xlim([0 100])
        xlabel('Percentage gait cycle');
        if i<5, ylabel('<---- Extension ------------ Flexion ---->')
        end
    end
    
    legend(['#',num2str(subj)],'#1','#2','etc','location','best')
end


