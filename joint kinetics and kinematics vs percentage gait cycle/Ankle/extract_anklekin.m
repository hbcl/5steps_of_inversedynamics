function[] = extract_anklekin(datafolder,subjects,trials)

% Set current directory (cd) to data folder

cd(datafolder)     

% Define variables
Manklel = nan(101,10,33,3);
Mankler = nan(101,10,33,3);
Aanklel = nan(101,10,33,3);
Aankler = nan(101,10,33,3);
Panklel = nan(101,10,33,3);
Pankler = nan(101,10,33,3);
Tanklel = nan(101,10,33,3);
Tankler = nan(101,10,33,3);
JFanklel_pc= nan(101,10,33,3);
JFankler_pc= nan(101,10,33,3);
PTanklel_pc= nan(101,10,33,3);
PTankler_pc= nan(101,10,33,3);

for subj = subjects
    load(['p',num2str(subj),'_5steps_data.mat'],'data')
    disp(['subj:', num2str(subj)]);
   
    for trial = trials
        fprintf_r('%i', trial);
        
        if sum(isnan(data(trial).grf(:)))<1
            % Extract ankle moment and angle
            Manklex = [data(trial).jointmoment(:,1) data(trial).jointmoment(:,10)];
            Mankley = [data(trial).jointmoment(:,2) data(trial).jointmoment(:,11)];
            Manklez = [data(trial).jointmoment(:,3) data(trial).jointmoment(:,12)];
            
            Aanklex = [data(trial).jointangle(:,1) data(trial).jointangle(:,10)];
            Aankley = [data(trial).jointangle(:,2) data(trial).jointangle(:,11)];
            Aanklez = [data(trial).jointangle(:,3) data(trial).jointangle(:,12)];
            
            Panklex = [data(trial).jointpower(:,1) data(trial).jointpower(:,10)];
            Pankley = [data(trial).jointpower(:,2) data(trial).jointpower(:,11)];
            Panklez = [data(trial).jointpower(:,3) data(trial).jointpower(:,12)];
           
             %Extract joint force
            JFanklex=[data(trial).jointforce.loc(:,1) data(trial).jointforce.loc(:,10)];
            JFankley=[data(trial).jointforce.loc(:,2) data(trial).jointforce.loc(:,11)];
            JFanklez=[data(trial).jointforce.loc(:,3) data(trial).jointforce.loc(:,12)];

            % extract joint translation
            Tanklex_start=repmat([data(trial).jointpos.loc(1,1), data(trial).jointpos.loc(1,10)],length(JFanklex),1);
            Tanklex = [data(trial).jointpos.loc(:,1) data(trial).jointpos.loc(:,10)]-Tanklex_start;
            Tankley_start=repmat([data(trial).jointpos.loc(1,2), data(trial).jointpos.loc(1,11)],length(JFankley),1);
            Tankley = [data(trial).jointpos.loc(:,2) data(trial).jointpos.loc(:,11)]-Tankley_start;
            Tanklez_start=repmat([data(trial).jointpos.loc(1,3), data(trial).jointpos.loc(1,12)],length(JFanklez),1);
            Tanklez = [data(trial).jointpos.loc(:,3) data(trial).jointpos.loc(:,12)]-Tanklez_start;
            
            %extract translational power 
            PTanklex = [data(trial).transpower.loc(:,1),data(trial).transpower.loc(:,10)];
            PTankley= [data(trial).transpower.loc(:,2),data(trial).transpower.loc(:,11)];
            PTanklez= [data(trial).transpower.loc(:,3),data(trial).transpower.loc(:,12)];

            % Extract grfs
            grfl = data(trial).grf(:,1:3);
            grfr = data(trial).grf(:,4:6);
            
            % Threshold at 20 N
            grfl(grfl(:,3)<20,:) = 0;
            grfr(grfr(:,3)<20,:) = 0;         
           
            % Get heelstrikes
            hsl = invDynGrid_getHS_TO(grfl,grfr, 10);
            hsl = round(unique(hsl)/10) + 1;

            % Interpolate to percentage gait cycle
            if sum(isnan(Manklex(:)))<1 && sum(isnan(Panklex(:)))<1
                Manklel(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Manklex(:,1),hsl,101),2,'omitnan');
                Manklel(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Mankley(:,1),hsl,101),2,'omitnan');
                Manklel(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(Manklez(:,1),hsl,101),2,'omitnan');
                Mankler(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Manklex(:,2),hsl,101),2,'omitnan');
                Mankler(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Mankley(:,2),hsl,101),2,'omitnan');
                Mankler(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(Manklez(:,2),hsl,101),2,'omitnan');
                
                Aanklel(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Aanklex(:,1),hsl,101),2,'omitnan');
                Aanklel(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Aankley(:,1),hsl,101),2,'omitnan');
                Aanklel(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(Aanklez(:,1),hsl,101),2,'omitnan');
                Aankler(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Aanklex(:,2),hsl,101),2,'omitnan');
                Aankler(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Aankley(:,2),hsl,101),2,'omitnan');
                Aankler(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(Aanklez(:,2),hsl,101),2,'omitnan');
                
                Panklel(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Panklex(:,1),hsl,101),2,'omitnan');
                Panklel(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Pankley(:,1),hsl,101),2,'omitnan');
                Panklel(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(Panklez(:,1),hsl,101),2,'omitnan');
                Pankler(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Panklex(:,2),hsl,101),2,'omitnan');
                Pankler(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Pankley(:,2),hsl,101),2,'omitnan');
                Pankler(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(Panklez(:,2),hsl,101),2,'omitnan');
                
                JFanklel_pc(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(JFanklex(:,1),hsl,101),2,'omitnan');
                JFanklel_pc(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(JFankley(:,1),hsl,101),2,'omitnan');
                JFanklel_pc(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(JFanklez(:,1),hsl,101),2,'omitnan');
                JFankler_pc(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(JFanklex(:,2),hsl,101),2,'omitnan');
                JFankler_pc(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(JFankley(:,2),hsl,101),2,'omitnan');
                JFankler_pc(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(JFanklez(:,2),hsl,101),2,'omitnan');
                
                Tanklel(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Tanklex(:,1),hsl,101),2,'omitnan');
                Tanklel(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Tankley(:,1),hsl,101),2,'omitnan');
                Tanklel(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(Tanklez(:,1),hsl,101),2,'omitnan');
                Tankler(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Tanklex(:,2),hsl,101),2,'omitnan');
                Tankler(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Tankley(:,2),hsl,101),2,'omitnan');
                Tankler(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(Tanklez(:,2),hsl,101),2,'omitnan');
                
                PTanklel_pc(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(PTanklex(:,1),hsl,101),2,'omitnan');
                PTanklel_pc(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(PTankley(:,1),hsl,101),2,'omitnan');
                PTanklel_pc(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(PTanklez(:,1),hsl,101),2,'omitnan');
                PTankler_pc(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(PTanklex(:,2),hsl,101),2,'omitnan');
                PTankler_pc(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(PTankley(:,2),hsl,101),2,'omitnan');
                PTankler_pc(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(PTanklez(:,2),hsl,101),2,'omitnan');
            end
        end
    end
        fprintf_r('reset');
        fprintf('\n')
end

save('Anklekin','Manklel', 'Mankler','Aanklel','Aankler','Panklel','Pankler', 'JFanklel_pc', 'JFankler_pc','Tanklel', 'Tankler','PTanklel_pc','PTankler_pc')
