function[] = extract_jointkin(datafolder,joint)

% Set current directory (cd) to data folder
cd(datafolder)
subjects = [1:3, 5:10];
trials = 1:33;
mass = [81.8 57.3 97.5 60.3 56.7 72.6 86.2 88.6 77 57]; % from excel sheets

npoints = 201;

%% Define variables
Ajoint_pc = nan(npoints, subjects(end), trials(end), 2);
Mjoint_pc = nan(npoints, subjects(end), trials(end), 2);
Pjoint_pc = nan(npoints, subjects(end), trials(end), 2);
Tjoint_pc = nan(npoints, subjects(end), trials(end), 2);

%% Look-up left and right indexes
if strcmp(joint,'Ankle')
    idl = 1:3;
    idr = 10:12;
    idp = [1 4];
elseif strcmp(joint,'Knee')
    idl = 4:6;
    idr = 13:15;
    idp = [2 5];
else
    idl = 7:9;
    idr = 16:18;
    idp = [3 6];
end

%% Loop
for subj = subjects
    load(['p',num2str(subj),'_5steps_data.mat'],'data')
    disp(['subj:', num2str(subj)]);
   
    for trial = trials
        fprintf_r('%i', trial);
        
        if sum(isnan(data(trial).grf(:)))<1
           
            
            Mjoint = [data(trial).jointmoment(:,idl(1)) data(trial).jointmoment(:,idr(1))];
            Ajoint = [data(trial).jointangle(:,idl(1)) data(trial).jointangle(:,idr(1))];
            Pjoint = [data(trial).jointpower2(:,idp(1)) data(trial).jointpower2(:,idp(2))] / mass(subj);
            Tjoint = [sum(data(trial).transpower.loc(:,idl),2) sum(data(trial).transpower.loc(:,idr),2)];
            
            %% extract heel strikes (include in process_5steps at some point)
            % Extract grfs
            grfl = data(trial).grf(:,1:3);
            grfr = data(trial).grf(:,4:6);
            
            % Threshold at 20 N
            grfl(grfl(:,3)<20,:) = 0;
            grfr(grfr(:,3)<20,:) = 0;         
           
            % Get heelstrikes
            [hsl, tol, hsr, tor] = invDynGrid_getHS_TO(grfl,grfr, 10);
                hsr(6) = length(grfl); % often not found with the function
    
            hsl = unique(hsl); hsl(find(diff(hsl)<5)) = [];
            hsr = unique(hsr); hsr(find(diff(hsr)<5)) = [];
            
           hs = [hsl hsr];
           hs_mo = ceil(hs/10);
            %% interpolate to percentage gait cycle
            if sum(isnan(Mjoint(:)))<1 && sum(isnan(Pjoint(:)))<1
                for L = 1:2
                    Ajoint_pc(:,subj,trial,L) = mean(interpolate_to_percgaitcycle(Ajoint(:,L),hs_mo(:,L),npoints),2,'omitnan');
                    Mjoint_pc(:,subj,trial,L) = mean(interpolate_to_percgaitcycle(Mjoint(:,L),hs_mo(:,L),npoints),2,'omitnan');
                    Pjoint_pc(:,subj,trial,L) = mean(interpolate_to_percgaitcycle(Pjoint(:,L),hs_mo(:,L),npoints),2,'omitnan');
                    Tjoint_pc(:,subj,trial,L) = mean(interpolate_to_percgaitcycle(Tjoint(:,L),hs_mo(:,L),npoints),2,'omitnan');
                end
            end
        end
    end
end
cd(datafolder)
save([joint,'_PG.mat'],'Ajoint_pc', 'Mjoint_pc', 'Pjoint_pc','Tjoint_pc')