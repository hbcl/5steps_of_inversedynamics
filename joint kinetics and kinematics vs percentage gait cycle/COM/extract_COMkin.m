clear all; close all; clc
datafolder = 'C:\Users\Tim\OneDrive - University of Calgary\shared 5steps data';
addpath(genpath(datafolder));
addpath(genpath('C:\Users\Tim\Documents\GitHub\5steps_of_inversedynamics'))

vwalks = [.7 .7 .7 .9 .9 .9 1.1 1.1 1.1 1.6 1.6 1.6 1.8 1.8 1.8 2.0...
    1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.4 1.4 1.4]; % m/s
mass = [81.8 57.3 97.5 60.3 56.7 72.6 86.2 88.6 77 57]; % from excel sheets
relsegmass = [0.0145 0.0465 .1 0.0145 0.0465 .1 .142]; % according to visual 3D

dt = 1/1200;
dtmo = 1/120;

subjects = 1:10;
trials = 1:33;

%% define
Pcom = nan(101,10,33,2);
Pper = nan(101,10,33,2);

%% loop
for subj = subjects
    load(['p',num2str(subj),'_5steps_data.mat'])
    
    disp(['subj:', num2str(subj)])
    disp('trial:')
    
    for trial = trials

        if ~isempty(data(trial).grf) && sum(isnan(data(trial).grf),'all')<1
          fprintf_r('%i', trial);

        grfl = data(trial).grf(:,1:3);
        grfr = data(trial).grf(:,4:6);
        grf = grfl + grfr;

        % make time series
        tgrf = 0:dt:(dt*(length(grf)-1));
        tmo = 0:dtmo:(dtmo*(length(data(trial).jointpower)-1));

        vcom = [];
        vcom(:,1) = detrend(cumtrapz(tgrf,(grf(:,1))/mass(subj))); % Donelan et al. 2002, med-lat
        vcom(:,2) = detrend(cumtrapz(tgrf,(grf(:,2))/mass(subj))) - vwalks(trial); % Donelan et al. 2002, for-aft
        vcom(:,3) = detrend(cumtrapz(tgrf,(grf(:,3)-mass(subj)*9.81)/mass(subj))); % Donelan et al. 2002, vertical

        % center of mass power
%         Pcom = sum(grf .* vcom,2);
        Pcomr = sum(grfr .* vcom,2) / mass(subj);
        Pcoml = sum(grfl .* vcom,2) / mass(subj);

        %% Peripheral work term using motion capture
        Pcom_mo = [];
        Pcom_mo(:,1) = interp1(tgrf,Pcoml,tmo);
        Pcom_mo(:,2) = interp1(tgrf,Pcomr,tmo);
%         Pcom_mo(:,3) = interp1(tgrf,Pcom,tmo);
        vcom_mo = interp1(tgrf, vcom, tmo);
        vcom_mo(:,2) = vcom_mo(:,2) + vwalks(trial); % relative to the lab frame

        % segment mass
        ms = mass(subj) * relsegmass;

        % translational energy    
        Etrans = [];
        k = 0;
        for s = 1:3:21
            k = k+1;
            vrel = data(trial).segmenvel(:,s:s+2) - vcom_mo;
            Etrans(:,k) = .5 * ms(k) * dot(vrel', vrel')';
        end

        % sum over ankle, knee and hip per leg
        Eperl = sum(Etrans(:,1:3),2) + sum(data(trial).rotenergy(:,1:3),2);
        Eperr = sum(Etrans(:,4:6),2) + sum(data(trial).rotenergy(:,4:6),2);

        % take derivative
        Pperl = nan(size(Eperl)); Pperr = nan(size(Eperr));
        Pperl(isfinite(Eperl)) = grad5(Eperl(isfinite(Eperl)), dtmo)  / mass(subj);
        Pperr(isfinite(Eperr)) = grad5(Eperr(isfinite(Eperr)), dtmo)  / mass(subj);
        
        %% make percentage gait cycle
        % Get heelstrikes
%         if subj == 5 && trial == 13,  keyboard
%         end
%         
        [hsl,~,hsr,~] = invDynGrid_getHS_TO(grfl, grfr, 20);
        hsl_mo = ceil(hsl/10);
        hsr_mo = ceil(hsr/10);
       
        Pcom(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Pcoml,unique(hsl),101),2,'omitnan');
        Pcom(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Pcomr,unique(hsr),101),2,'omitnan');
       
        
        Pper(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Pperl,unique(hsl_mo),101),2,'omitnan');
        Pper(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Pperr,unique(hsr_mo),101),2,'omitnan');
        end
    end
end

cd(datafolder)
save('COMkin.mat','Pcom','Pper')