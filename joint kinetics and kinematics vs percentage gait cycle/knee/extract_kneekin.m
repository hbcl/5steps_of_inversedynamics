function[] = extract_kneekin(datafolder,subjects,trials)

% Set current directory (cd) to data folder
cd(datafolder) 

% Define variables
Mkneel = nan(101,10,33,3);
Mkneer = nan(101,10,33,3);
Akneel = nan(101,10,33,3);
Akneer = nan(101,10,33,3);
Pkneel = nan(101,10,33,3);
Pkneer = nan(101,10,33,3);
Tkneel = nan(101,10,33,3);
Tkneer = nan(101,10,33,3);
JFkneel_pc= nan(101,10,33,3);
JFkneer_pc= nan(101,10,33,3);
PTkneel_pc= nan(101,10,33,3);
PTkneer_pc= nan(101,10,33,3);

for subj = subjects
    load(['p',num2str(subj),'_5steps_data.mat'],'data')
    disp(['subj:', num2str(subj)]);
   
    for trial = trials
        fprintf_r('%i', trial);
        
        if sum(isnan(data(trial).grf(:)))<1
            % Extract knee moment and angle
            Mkneex = [data(trial).jointmoment(:,4) data(trial).jointmoment(:,13)];
            Mkneey = [data(trial).jointmoment(:,5) data(trial).jointmoment(:,14)];
            Mkneez = [data(trial).jointmoment(:,6) data(trial).jointmoment(:,15)];
            
            Akneex = [data(trial).jointangle(:,4) data(trial).jointangle(:,13)];
            Akneey = [data(trial).jointangle(:,5) data(trial).jointangle(:,14)];
            Akneez = [data(trial).jointangle(:,6) data(trial).jointangle(:,15)];
            
            Pkneex = [data(trial).jointpower(:,4) data(trial).jointpower(:,13)];
            Pkneey = [data(trial).jointpower(:,5) data(trial).jointpower(:,14)];
            Pkneez = [data(trial).jointpower(:,6) data(trial).jointpower(:,15)];
             
            %Extract joint force
            JFkneex=[data(trial).jointforce.loc(:,4) data(trial).jointforce.loc(:,13)];
            JFkneey=[data(trial).jointforce.loc(:,5) data(trial).jointforce.loc(:,14)];
            JFkneez=[data(trial).jointforce.loc(:,6) data(trial).jointforce.loc(:,15)];
            
            %Tkneez = [data(trial).jointpos.loc(:,9) data(trial).jointpos.loc(:,18)] - [data(trial).jointpos.loc(1,9) data(trial).jointpos.loc(1,18)];
            Tkneex_start=repmat([data(trial).jointpos.loc(1,4), data(trial).jointpos.loc(1,13)],length(JFkneex),1);
            Tkneex = [data(trial).jointpos.loc(:,4) data(trial).jointpos.loc(:,13)]-Tkneex_start;
            Tkneey_start=repmat([data(trial).jointpos.loc(1,5), data(trial).jointpos.loc(1,14)],length(JFkneey),1);
            Tkneey = [data(trial).jointpos.loc(:,5) data(trial).jointpos.loc(:,14)]-Tkneey_start;
            Tkneez_start=repmat([data(trial).jointpos.loc(1,6), data(trial).jointpos.loc(1,15)],length(JFkneez),1);
            Tkneez = [data(trial).jointpos.loc(:,6) data(trial).jointpos.loc(:,15)]-Tkneez_start;
            
            % extract translational power
            PTkneex = [data(trial).transpower.loc(:,4),data(trial).transpower.loc(:,13)];
            PTkneey= [data(trial).transpower.loc(:,5),data(trial).transpower.loc(:,14)];
            PTkneez= [data(trial).transpower.loc(:,6),data(trial).transpower.loc(:,15)];
            
            % Extract grfs
            grfl = data(trial).grf(:,1:3);
            grfr = data(trial).grf(:,4:6);
            
            % Threshold at 20 N
            grfl(grfl(:,3)<20,:) = 0;
            grfr(grfr(:,3)<20,:) = 0;         
           
            % Get heelstrikes
            hsl = invDynGrid_getHS_TO(grfl,grfr, 10);
            hsl = round(unique(hsl)/10) + 1;

            % Interpolate to percentage gait cycle
            if sum(isnan(Mkneex(:)))<1 && sum(isnan(Pkneex(:)))<1
                Mkneel(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Mkneex(:,1),hsl,101),2,'omitnan');
                Mkneel(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Mkneey(:,1),hsl,101),2,'omitnan');
                Mkneel(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(Mkneez(:,1),hsl,101),2,'omitnan');
                Mkneer(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Mkneex(:,2),hsl,101),2,'omitnan');
                Mkneer(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Mkneey(:,2),hsl,101),2,'omitnan');
                Mkneer(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(Mkneez(:,2),hsl,101),2,'omitnan');
                
                Akneel(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Akneex(:,1),hsl,101),2,'omitnan');
                Akneel(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Akneey(:,1),hsl,101),2,'omitnan');
                Akneel(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(Akneez(:,1),hsl,101),2,'omitnan');
                Akneer(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Akneex(:,2),hsl,101),2,'omitnan');
                Akneer(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Akneey(:,2),hsl,101),2,'omitnan');
                Akneer(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(Akneez(:,2),hsl,101),2,'omitnan');
                
                Pkneel(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Pkneex(:,1),hsl,101),2,'omitnan');
                Pkneel(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Pkneey(:,1),hsl,101),2,'omitnan');
                Pkneel(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(Pkneez(:,1),hsl,101),2,'omitnan');
                Pkneer(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Pkneex(:,2),hsl,101),2,'omitnan');
                Pkneer(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Pkneey(:,2),hsl,101),2,'omitnan');
                Pkneer(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(Pkneez(:,2),hsl,101),2,'omitnan');
                
                %fourth dimension is the direction (axis) 1-x, 2-y, 3-z
                JFkneel_pc(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(JFkneex(:,1),hsl,101),2,'omitnan');
                JFkneel_pc(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(JFkneey(:,1),hsl,101),2,'omitnan');
                JFkneel_pc(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(JFkneez(:,1),hsl,101),2,'omitnan');
                
                JFkneer_pc(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(JFkneex(:,2),hsl,101),2,'omitnan');
                JFkneer_pc(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(JFkneey(:,2),hsl,101),2,'omitnan');
                JFkneer_pc(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(JFkneez(:,2),hsl,101),2,'omitnan');
                
                Tkneel(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Tkneex(:,1),hsl,101),2,'omitnan');
                Tkneel(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Tkneey(:,1),hsl,101),2,'omitnan');
                Tkneel(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(Tkneez(:,1),hsl,101),2,'omitnan');
                
                Tkneer(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(Tkneex(:,2),hsl,101),2,'omitnan');
                Tkneer(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(Tkneey(:,2),hsl,101),2,'omitnan');
                Tkneer(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(Tkneez(:,2),hsl,101),2,'omitnan');
                
                PTkneel_pc(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(PTkneex(:,1),hsl,101),2,'omitnan');
                PTkneel_pc(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(PTkneey(:,1),hsl,101),2,'omitnan');
                PTkneel_pc(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(PTkneez(:,1),hsl,101),2,'omitnan');
        
                PTkneer_pc(:,subj,trial,1) = mean(interpolate_to_percgaitcycle(PTkneex(:,2),hsl,101),2,'omitnan');
                PTkneer_pc(:,subj,trial,2) = mean(interpolate_to_percgaitcycle(PTkneey(:,2),hsl,101),2,'omitnan');
                PTkneer_pc(:,subj,trial,3) = mean(interpolate_to_percgaitcycle(PTkneez(:,2),hsl,101),2,'omitnan');
            end
        end
    end
        fprintf_r('reset');
        fprintf('\n')
end

save('Kneekin','Mkneel', 'Mkneer','Akneel','Akneer','Pkneel','Pkneer', 'JFkneel_pc', 'JFkneer_pc','Tkneel','Tkneer','PTkneel_pc','PTkneer_pc')
