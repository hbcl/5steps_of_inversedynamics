function[] = visualize_kneekin(datafolder,subj,trials)

cd(datafolder)
load('Kneekin.mat','Mkneel', 'Mkneer','Akneel','Akneer','Pkneel','Pkneer')

% keyboard
for trial = trials
    figure('name',['Kneekin trial: ', num2str(trial)])
    
    % left knee moment
    subplot(321)
    plot(Mkneel(:,:,trial)); hold on
    plot(Mkneel(:,subj,trial),'k','linewidth',2); hold on
    plot([0 100], [0 0], 'k-')
    title('Left knee moment (N-m/kg)')
    axis tight; yl = get(gca,'ylim');
    
    % right knee moment
    subplot(322)
    plot(Mkneer(:,:,trial)); hold on
    plot(Mkneer(:,subj,trial),'k','linewidth',2); hold on
    plot([0 100], [0 0], 'k-')
    title('Right knee moment (N-m/kg)')
    ylim(yl)
        
    % left knee angle
    subplot(323)
    plot(Akneel(:,:,trial)); hold on
    plot(Akneel(:,subj,trial),'k','linewidth',2); hold on
    plot([0 100], [0 0], 'k-')
    title('Left knee angle (deg)')
    axis tight; yl = get(gca,'ylim');
    
    % right knee angle
    subplot(324)
    plot(Akneer(:,:,trial)); hold on
    plot(Akneer(:,subj,trial),'k','linewidth',2); hold on
    plot([0 100], [0 0], 'k-')
    title('Right knee angle (deg)')
    ylim(yl)
    
    % left knee power
    subplot(325)
    plot(Pkneel(:,:,trial)); hold on
    plot(Pkneel(:,subj,trial),'k','linewidth',2); hold on
    plot([0 100], [0 0], 'k-')
    title('Left knee power (W/kg)')
    axis tight; yl = get(gca,'ylim');
    
    % right knee power
    subplot(326)
    plot(Pkneer(:,:,trial)); hold on
    plot(Pkneer(:,subj,trial),'k','linewidth',2); hold on
    plot([0 100], [0 0], 'k-')
    title('Right knee power (W/kg)')
    ylim(yl)
    
    % make nice
    for i = 1:6
        subplot(3,2,i)
        xlim([0 100])
        xlabel('Percentage gait cycle');
        
        if i<5, ylabel('<---- Extension ------------ Flexion ---->')
        end
    end
    
    legend(['#',num2str(subj)],'#1','#2','etc','location','best')
end


