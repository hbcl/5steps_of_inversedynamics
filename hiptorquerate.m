clear all; close all; clc

name = mfilename; % name of this script
loca = mfilename('fullpath'); % full name, including path
fold = loca(1:end-length(name)); % folder, excluding name
cd(fold); % set cd to that folder
addpath(genpath(cd)); % add all subfolders of cd to path


datafolder = 'C:\Users\Tim\OneDrive - University of Calgary\shared 5steps data';
cd(datafolder)

mass = [81.8 57.3 97.5 60.3 56.7 72.6 86.2 88.6 77 57]; % from excel sheets
%% loop
trials = 22:25;
Mhip_dot_peak = nan(10,4);

for subj = [1:3, 5:10]
    load(['p',num2str(subj),'_5steps_data.mat'])
    
    disp(['subj:', num2str(subj)])
    disp('trial:')
    
    
    for t = 1:4
            trial = trials(t);
            load(['p',num2str(subj),'_5steps_data.mat'])
            
            % fs = 120 Hz, 5.5 strides per 5-step data file
            Tstride_quick = length(data(trial).jointmoment) / 120 / 5.5;
                % hip power
                
                if sum(isnan(data(trial).jointmoment))<1
                    Mhipl = data(trial).jointmoment(:,7);
                    Mhipr = data(trial).jointmoment(:,16);
                    Mhip_dot_peak(subj, t) = mean([max(Mhipl), max(Mhipr)]) / (Tstride_quick/2);
                end
     
    end
end