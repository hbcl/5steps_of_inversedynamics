function[] = analyse_5steps(datafolder)
% analyse_5steps(datafolder) analyses the 5steps data stored in datafolder
% and saves outputs in Wsoft.mat and COMkin.mat. Outputs mainly consist of
% work estimates, centered around the soft tissue work estimate. 5stepsdata
% have been created by process_5steps.m. This function only cuts the data
% and doesn't do processing. Data loaded into analyse_5steps can thus be
% viewed as being directly outputted from visual 3D. 

name = mfilename; % name of this script
loca = mfilename('fullpath'); % full name, including path
fold = loca(1:end-length(name)); % folder, excluding name
cd(fold); % set cd to that folder
addpath(genpath(cd)); % add all subfolders of cd to path
cd(datafolder)

vwalks = [.7 .7 .7 .9 .9 .9 1.1 1.1 1.1 1.6 1.6 1.6 1.8 1.8 1.8 2.0...
    1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.4 1.4 1.4]; % m/s

mass = [81.8 57.3 97.5 60.3 56.7 72.6 86.2 88.6 77 57]; % from excel sheets
% mass = [82.0 57.3 97.5 nan  56.7 72.6 86.2 88.6 77 57]; % from V3D

%% some parameters
fs = 1200; dt = 1/fs;
fsmo = 120; dtmo = 1/fsmo;

relsegmass = [0.0145 0.0465 .1 0.0145 0.0465 .1 .142]; % according to visual 3D
% https://www.c-motion.com/v3dwiki/index.php/Segment_Mass

nt = 33; ns = 10; nl = 2;

%% define variables of interest
Pcom_pc = nan(101,ns,nt,nl);
Pjoint_pc = nan(101,ns,nt,nl);
Pcom_alt_pc = nan(101,ns,nt,nl);
Pper_pc = nan(101,ns,nt,nl);
vcom_pc = nan(101,ns,nt,nl);
vcom_alt_pc = nan(101,ns,nt,nl);

Wsoft = nan(nt,ns,nl);
Wankle = nan(nt,ns,nl);
Wknee = nan(nt,ns,nl);
Whip = nan(nt,ns,nl);
Wperi = nan(nt,ns,nl);
Wcom = nan(nt,ns,nl);
Wbody = nan(nt,ns,nl);
Wbody_neg = nan(nt,ns,nl);
Wbody_pos = nan(nt,ns,nl);
Wjoint = nan(nt,ns,nl);
Wjoint_pos = nan(nt,ns,nl);
Wjoint_neg = nan(nt,ns,nl);
Wbodycoll = nan(nt,ns,nl);
WCOMcoll = nan(nt,ns,nl);
Wsoftcoll = nan(nt,ns,nl);
Wjointcoll = nan(nt,ns,nl);
Fpeak = nan(nt,ns,nl);
Tstride = nan(nt,ns,nl);
vcom_hs = nan(nt,ns,nl);
vcom_hs_alt = nan(nt,ns,nl);
deltav = nan(nt,ns,nl);
nsteps = nan(nt,ns,nl);
Wjointtranscoll = nan(nt,ns,nl);
Wjoint_trans = nan(nt,ns,nl);
Wankle_trans = nan(nt,ns,nl);
Wperi_neg = nan(nt,ns,nl);

   
FTI = nan(nt,ns,nl);
%% loop
for subj = [1:3, 5:10]
    load(['p',num2str(subj),'_5steps_data.mat'])
    
    disp(['subj:', num2str(subj)])
    disp('trial:')
    
    
for trial = 1:length(data)
    
    if ~isempty(data(trial).grf) && sum(isnan(data(trial).grf),'all')<1
      fprintf_r('%i', trial);
      
      % exclude bad trials, exclude subject 4
      if subj == 4 || (subj == 3 && trial == 4) || (subj == 9 && trial == 14) || (subj == 10 && trial == 1)
          continue
      end
          
    %% Force platform: GRF, vCOM and PCOM
    grfl = data(trial).grf(:,1:3);
    grfr = data(trial).grf(:,4:6);
    grf = grfl + grfr;

    % peak grf
    Fpeak(trial,subj) = max(data(trial).grf,[],'all');

    % make time series
    tgrf = 0:dt:(dt*(length(grf)-1));
    tmo = 0:dtmo:(dtmo*(length(data(trial).jointpower)-1));
    
    %% Gait events
    if ~(subj == 7 && trial == 27), threshold = 30;
    else, threshold = 50;
    end
        
    [hsl, tol, hsr, tor] = invDynGrid_getHS_TO_v2(grfl, grfr, threshold);
    
    hsr(6) = length(grfl); % often not found with the function
    
    hsl = unique(hsl); hsl(find(diff(hsl)<5)) = [];
    hsr = unique(hsr); hsr(find(diff(hsr)<5)) = [];
    tol = unique(tol); tol(find(diff(tol)<5)) = [];
    tor = unique(tor); tor(find(diff(tor)<5)) = [];

    nsteps(trial,subj,1) = length(hsl);
    nsteps(trial,subj,2) = length(hsr);
    
    actual_mass = mean(grf(hsl(1):hsl(end),3))/9.81;

    FTI(trial,subj,1) = mean(grfl(hsl(1):hsl(end),3));
    FTI(trial,subj,2) = mean(grfr(hsr(1):hsr(end),3));
    
%% Center of mass velocity
    vcom_raw = []; vcom = []; Pcom = [];
    vcom_raw(:,1) = cumtrapz(tgrf,(grf(:,1))/actual_mass); % Donelan et al. 2002, med-lat
    vcom_raw(:,2) = cumtrapz(tgrf,(grf(:,2))/actual_mass) - vwalks(trial); % Donelan et al. 2002, for-aft
    vcom_raw(:,3) = cumtrapz(tgrf,(grf(:,3)-actual_mass*9.81)/actual_mass); % Donelan et al. 2002, vertical
    
    p = nan(3,2);
    for i = 1:3
        p(i,:) = polyfit(hsl(1):hsl(end), vcom_raw(hsl(1):hsl(end),i), 1);
        vcom(:,i) = vcom_raw(:,i) - polyval(p(i,:), 1:length(tgrf))';
    end
    
    vcom(:,2) = vcom(:,2) - vwalks(trial);
    
    % center of mass power
    Pcom(:,1) = sum(grfl .* vcom,2);
    Pcom(:,2) = sum(grfr .* vcom,2);
    
    % flip for-aft to make it positive
    Vcom = [vcom(:,1) -vcom(:,2) vcom(:,3)];
    
    %% Velocity events   
    % COM veloicty events
    hstohstohs = [hsl(1:5) tor(1:5) hsr(1:5) tol(1:5)]';
    out = comvelocityevents(Vcom,[hstohstohs(1:numel(hstohstohs)) hsl(end)]);

    % v-up events for left and right leg
    vupl = out.vcomevents(4:12:end);
    vupr = out.vcomevents(10:12:end); 
    
    % v-down events for left and right leg
    vdownl = out.vcomevents(6:12:end);
    vdownr = out.vcomevents(12:12:end);
    
    % convert to motion capture frames
    hs = [hsl hsr];
    hs_mo = ceil(hs/10);
    if hs_mo(end,2) > length(tmo), hs_mo(end,2) = length(tmo);
    end
    vup_mo = ceil([vupl vupr]/10);
    
    % velocity at heelstrike
    vcom_hs(trial,subj,1) = norm(mean(Vcom(vdownl,:)));
    vcom_hs(trial,subj,2) = norm(mean(Vcom(vdownr,:)));
    
    vcom_hs_alt(trial,subj,1) = norm(mean(Vcom(hsl-1,:)));
    vcom_hs_alt(trial,subj,2) = norm(mean(Vcom(hsr-1,:)));
    
%     if trial == 20
%         figure(1);
%         plot(Vcom(:,2),Vcom(:,3),'k'); hold on
%         
%         plot(Vcom(vdownl,2), Vcom(vdownl,3),'ro');
%         plot(Vcom(vdownr,2), Vcom(vdownr,3),'bo');
%         
%         plot(Vcom(vupl,2), Vcom(vupl,3),'rx');
%         plot(Vcom(vupr,2), Vcom(vupr,3),'bx');
%         
%         
%         figure
%         plot(Vcom(:,3));
%         hold on
%         plot(vupl,Vcom(vupl,3),'rx');
%        plot(vdownr,Vcom(vdownr,3),'bo');
%        
%        plot(vupr,Vcom(vupr,3),'bx');
%        plot(vdownl,Vcom(vdownl,3),'ro');
%         
% %         keyboard
%     end
%         if subj == 5
%             saveV = Vcom;
%         end
%     end

    
    % delta velocity 
    deltav(trial,subj,1) = mean(atan2d(Vcom(vupr,3), Vcom(vupr,2)) - atan2d(Vcom(vdownl,3), Vcom(vdownl,2)));
    deltav(trial,subj,2) = mean(atan2d(Vcom(vupl(2:end),3), Vcom(vupl(2:end),2)) - atan2d(Vcom(vdownr(1:4),3), Vcom(vdownr(1:4),2)));
    
    %% Stride time
    % stride time (take length(grf) because things end with right
    % heelstrike, but this last heelstrike is not included in hsr)
    Tstride(trial,subj,1) = ((hsl(end)-hsl(1))*dt)/5;
    Tstride(trial,subj,2) = ((hsr(end)-hsr(1))*dt)/5;
    
    %% COM power and velocity in motion capture time
    Pcom_mo = [];
    Pcom_mo(:,1) = interp1(tgrf,Pcom(:,1),tmo);
    Pcom_mo(:,2) = interp1(tgrf,Pcom(:,2),tmo);

    vcom_mo = interp1(tgrf, vcom, tmo);
    vcom_mo(:,2) = vcom_mo(:,2) + vwalks(trial); % relative to the lab frame

    % alternative COM power: from pelvis velocity
    Pcom_mo_alt = []; vcom_mo_alt = []; grf_mo = [];
    grf_mo(:,1:3) = interp1(tgrf,grfl,tmo); grf_mo(:,4:6) = interp1(tgrf,grfr,tmo);
    vcom_mo_alt = data(trial).segmenvel(:,end-2:end) - repmat([0 vwalks(trial) 0], length(grf_mo),1);
    Pcom_mo_alt(:,1) = sum(grf_mo(:,1:3) .* vcom_mo_alt,2);
    Pcom_mo_alt(:,2) = sum(grf_mo(:,4:6) .* vcom_mo_alt,2);
    
    %% Peripheral work 
    segmentmass = mass(subj) * relsegmass;
    [Pper] = CalcPeripheralPower(data(trial).segmenvel, data(trial).rotenergy, segmentmass, vcom_mo, fsmo);
    
    %% Translational joint power
    [transjointpower] = CalcTransJointPower_Locally(-data(trial).jointpos.loc, data(trial).jointforce.loc, fsmo);
    
    %% Joint powers
    % ankle power
    Pank = nan(length(data(trial).jointpower2), 2,2);
    Pank(:,:,1) = [data(trial).jointpower2(:,1) sum(transjointpower(:,1:3),2)*mass(subj)];
    Pank(:,:,2) = [data(trial).jointpower2(:,4) sum(transjointpower(:,10:12),2)*mass(subj)];
    
    % knee power
    Pkne = nan(length(data(trial).jointpower2), 2,2);
    Pkne(:,:,1) = [data(trial).jointpower2(:,2) sum(transjointpower(:,4:6),2)*mass(subj)];
    Pkne(:,:,2) = [data(trial).jointpower2(:,5) sum(transjointpower(:,13:15),2)*mass(subj)];
    
    % hip power
    Phip = nan(length(data(trial).jointpower2), 2,2);
    Phip(:,:,1) = [data(trial).jointpower2(:,3) sum(transjointpower(:,7:9),2)*mass(subj)];
    Phip(:,:,2) = [data(trial).jointpower2(:,6) sum(transjointpower(:,16:18),2)*mass(subj)];
    
    %% Summed joint and soft tissue
    Pjoint_rot = Pank(:,1,:)  + Pkne(:,1,:) + Phip(:,1,:); % summed rotational power
    Pjoint_trans = Pank(:,2,:)  + Pkne(:,2,:) + Phip(:,2,:); % summed translational power
    Pjoint = squeeze(Pjoint_rot + Pjoint_trans);
    Pbody = Pcom_mo + Pper; % whole-body power
    Psoft = Pbody - Pjoint; % soft tissue power
    
    %% Work terms: full stride
    for i = 1:2
        % individual joint work
        Wankle(trial,subj,i) = fintrapz(tmo, sum(Pank(:,:,i),2),[],[hs_mo(1,i) hs_mo(end,i)]) / 5;
        Wknee(trial,subj,i) = fintrapz(tmo, sum(Pkne(:,:,i),2),[],[hs_mo(1,i) hs_mo(end,i)]) / 5;
        Whip(trial,subj,i) = fintrapz(tmo, sum(Phip(:,:,i),2),[],[hs_mo(1,i) hs_mo(end,i)]) / 5;
        
        % summed joint
        Wjoint(trial,subj,i) = fintrapz(tmo, Pjoint(:,i),[],[hs_mo(1,i) hs_mo(end,i)]) / 5;
        Wjoint_pos(trial,subj,i) = fintrapz(tmo, Pjoint(:,i),'+',[hs_mo(1,i) hs_mo(end,i)]) / 5;
        Wjoint_neg(trial,subj,i) = fintrapz(tmo, Pjoint(:,i),'-',[hs_mo(1,i) hs_mo(end,i)]) / 5;
        Wjoint_trans(trial,subj,i) = fintrapz(tmo, Pjoint_trans(:,i),[],[hs_mo(1,i) hs_mo(end,i)]) / 5;
        Wankle_trans(trial,subj,i) = fintrapz(tmo, Pank(:,2,i),[],[hs_mo(1,i) hs_mo(end,i)]) / 5;

        % whole-body work
        Wperi(trial,subj,i) = fintrapz(tmo, Pper(:,i),[],[hs_mo(1,i) hs_mo(end,i)]) / 5;
        Wcom(trial,subj,i) = fintrapz(tgrf, Pcom(:,i),[], [hs(1,i) hs(end,i)]) / 5;
        Wbody(trial,subj,i) = fintrapz(tmo, Pbody(:,i),[],[hs_mo(1,i) hs_mo(end,i)]) / 5;
        Wbody_neg(trial,subj,i) = fintrapz(tmo, Pbody(:,i),'-',[hs_mo(1,i) hs_mo(end,i)]) / 5;
        Wbody_pos(trial,subj,i) = fintrapz(tmo, Pbody(:,i),'+',[hs_mo(1,i) hs_mo(end,i)]) / 5;
        Wperi_neg(trial,subj,i) = fintrapz(tmo, Pper(:,i),'-',[hs_mo(1,i) hs_mo(end,i)], Pbody(:,i)) / 5;
        
        % soft tissue work
        Wsoft(trial,subj,i) = fintrapz(tmo, Psoft(:,i),[],[hs_mo(1,i) hs_mo(end,i)]) / 5;
    end
    
    %% Work terms: collision phase
    % collision work
    Wbodycol_step = nan(5,2); WCOMcol_step = nan(5,2);  Wsoftcol_step = nan(5,2);   Wjointcol_step = nan(5,2); Wjointtrans_col_step = nan(5,2);
    
    for i = 1:2
        for istep = 1:5       
                Wbodycol_step(istep,i) = fintrapz(tmo, Pbody(:,i), '-', [hs_mo(istep,i) vup_mo(istep,i)]);
                WCOMcol_step(istep,i) = fintrapz(tmo, Pcom_mo(:,i), '-', [hs_mo(istep,i) vup_mo(istep,i)]);
                Wsoftcol_step(istep,i) = fintrapz(tmo, Psoft(:,i), '-', [hs_mo(istep,i) vup_mo(istep,i)], Pbody(:,i));  
                Wjointcol_step(istep,i) = fintrapz(tmo, Pjoint(:,i), '-', [hs_mo(istep,i) vup_mo(istep,i)], Pbody(:,i));  
                Wjointtrans_col_step(istep,i) = fintrapz(tmo, Pjoint_trans(:,i), '-', [hs_mo(istep,i) vup_mo(istep,i)], Pbody(:,i));  
        end
        
        % average over steps
        WCOMcoll(trial,subj,i) = mean(WCOMcol_step(:,i));
        Wbodycoll(trial,subj,i) = mean(Wbodycol_step(:,i));
        Wsoftcoll(trial,subj,i) = mean(Wsoftcol_step(:,i));
        Wjointcoll(trial,subj,i) = mean(Wjointcol_step(:,i)); 
        Wjointtranscoll(trial,subj,i) = mean(Wjointtrans_col_step(:,i)); 
    end   
    

    
    %% percentage gait cycle
    for i = 1:3
        vcom_pc(:,subj,trial,i) = mean(interpolate_to_percgaitcycle(vcom(:,i),unique(hsl),101),2,'omitnan');
        vcom_alt_pc(:,subj,trial,i) = mean(interpolate_to_percgaitcycle(vcom_mo_alt(:,i),unique(hs_mo(:,1)),101),2,'omitnan');
    end
    
    for i = 1:2
        Pjoint_pc(:,subj,trial,i) = mean(interpolate_to_percgaitcycle(Pjoint(:,i)/mass(subj),unique(hs_mo(:,i)),101),2,'omitnan');
        
        Pcom_pc(:,subj,trial,i) = mean(interpolate_to_percgaitcycle(Pcom_mo(:,i)/mass(subj),unique(hs_mo(:,i)),101),2,'omitnan');
        Pper_pc(:,subj,trial,i) = mean(interpolate_to_percgaitcycle(Pper(:,i)/mass(subj),unique(hs_mo(:,i)),101),2,'omitnan');
        
        Pcom_alt_pc(:,subj,trial,i) = mean(interpolate_to_percgaitcycle(Pcom_mo_alt(:,i)/mass(subj),unique(hs_mo(:,i)),101),2,'omitnan');        
    end
    end
end
    fprintf_r('reset');
    fprintf('\n')
end

% missing trials
[r,c] = find(isnan(nsteps(:,:,1)));
missing = [c r];

%% Saving
save('COMkin.mat','Pcom_pc','Pcom_alt_pc','Pper_pc','vcom_pc','vcom_alt_pc','Pjoint_pc')
save('Wsoft')
