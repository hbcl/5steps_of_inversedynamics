function [] = plotperstride(x, hsl)

color = get(gca,'colororder');

for k = 1:(length(hsl)-1)
    for j = 1:size(x,2)
        plot(x(hsl(k):hsl(k+1),j), 'color', color(j,:)); hold on
    end
end