function [Pper] = CalcPeripheralPower(segmentvelocity, rotenergy, segmenmass, vcom_mo, fsmo)

Etrans = nan(length(vcom_mo),6);
k = 0;
for s = 1:3:size(segmentvelocity,2)
    k = k+1;
    vrel = segmentvelocity(:,s:s+2) - vcom_mo;
    Etrans(:,k) = .5 * segmenmass(k) * dot(vrel', vrel')';
end

% sum over ankle, knee and hip per leg
Eperl = sum(Etrans(:,1:3),2) + sum(rotenergy(:,1:3),2);
Eperr = sum(Etrans(:,4:6),2) + sum(rotenergy(:,4:6),2);

% take derivative
Pper = nan(length(Eperl),2);
Pper(isfinite(Eperl),1) = grad5(Eperl(isfinite(Eperl)), 1/fsmo);
Pper(isfinite(Eperr),2) = grad5(Eperr(isfinite(Eperr)), 1/fsmo);

%% optional check: compare pelvis velocity with COM velocity, should be fairly similar
% figure;
% color = get(gca,'colororder');
% for i = 1:3
%     plot(segmentvelocity(:,end-3+i),'color',color(i,:)); hold on
%     plot(vcom_mo(:,i), '--','color',color(i,:))
% end
%     
end