function[transjointpower] = CalcTransJointPower_Locally(proxsegmentcom_loc_wrt_jointpos, fjoint_loc, fs)
% [transjointpower] = CalcTransJointPower_Locally(proxsegmentcom_loc_wrt_jointpos, fjoint_loc, fs)
% is the translational joint power expressed in proximal segment local
% coordinate system (Nx3) 
% jointpos_wrt_proxsegmentcom_loc is the segment center of gravity (CG) position, 
% computed with the V3D fucntion SEG_CGPOSITION
% (https://c-motion.com/v3dwiki/index.php?title=Model_Based_Item:_SEG_CGPOSITION)
% using the proximal segment as segment, the distal segment as reference
% and the proximal segment as resolution 
% fjoint_loc is the joint force, computed with the V3D function JOINT_FORCE
% (https://c-motion.com/v3dwiki/index.php?title=Model_Based_Item:_JOINT_FORCE)
% using the joint as joint/segment, and the proximal segment as resolution
% fs is the sample frequency (Hz) 
%
% Tim van der Zee, University of Calgary, 2020

% We want the opposite of proxsegmentcom_loc_wrt_jointpos, namely
% jointpos_wrt_proxsegmentcom_loc:
jointpos_wrt_proxsegmentcom_loc = -proxsegmentcom_loc_wrt_jointpos;

% Now take the time derivative of that:
jointvel_wrt_proxsegmentcom_loc = grad5(jointpos_wrt_proxsegmentcom_loc, 1/fs);

% And calculate power as the element-wise product with force
transjointpower = fjoint_loc .* jointvel_wrt_proxsegmentcom_loc;
end
