## 5 steps of inverse dynamics
This repository is meant to be the starting point for those who want to analyse the inverse dynamics grid data, but are bothered by "bad trials"

I have considered the following experimental conditions:

1. Constant step length (increasing speed),
2. Constant step frequency (increasing speed)
3. Constant speed (increasing step length)
4. Preferred walking (increasing speed)
5. Constant speed (increasing step width)

Ground reaction force data is sampled at 1200 Hz and motion capture data is sampled at 120 Hz. 
Experimental conditions add up to 33 trials in total. I have selected 5 steps (left heelstrike to right heelstrike) from each trial that I judged "good enough". 

## Data
Data is available here: https://drive.google.com/open?id=1jbQBL-D_V_q-KjOUVa_o2RSiBlSFKXyG

*Data last updated on 2021-02-06*

Data is divided into two folders:

* 5_steps_data: containing the 5 steps of data and interval indices
* processed data: including some processed terms from the 5 steps data (e.g. work, power as percentage gait cycle)

Please make sure that you frequently download the 5steps_data again, since it is still work-in-progress.

## Functions & scripts
![picture](drawing_organization.png)

## 33 trials

Trials correspond to the following conditions:

1.	0.7 m/s constant step length
2.	0.7 m/s preferred
3.	0.7 m/s constant step frequency
4.	0.9 m/s constant step length
5.	0.9 m/s preferred
6.	0.9 m/s constant step frequency
7.	1.1 m/s constant step length
8.	1.1 m/s preferred
9.	1.1 m/s constant step frequency
10.	1.6 m/s constant step frequency
11.	1.6 m/s preferred
12.	1.6 m/s constant step length
13.	1.8 m/s constant step frequency
14.	1.8 m/s preferred
15.	1.8 m/s constant step length
16.	2.0 m/s preferred
17.	1.25 m/s lowest step frequency
18.	1.25 m/s lower step frequency
19.	1.25 m/s low step frequency
20.	1.25 m/s preferred
21.	1.25 m/s preferred
22.	1.25 m/s preferred
23.	1.25 m/s high step frequency
24.	1.25 m/s higher step frequency
25.	1.25 m/s highest step frequency
26.	1.25 m/s zero step width
27.	1.25 m/s 10 cm step width
28.	1.25 m/s 20 cm step width
29.	1.25 m/s 30 cm step width
30.	1.25 m/s 40 cm step width
31.	1.40 m/s constant step frequency
32.	1.40 m/s preferred
33.	1.40 m/s constant step length

In the above:

 * preferred means using preferred step length & step frequency for the speed in question
 * constant step length means using the step length that was preferred when walking at 1.25 m/s
 * constant step frequency means using the step frequency that was preferred when walking at 1.25 m/s

## Missing trial

Subject 7 trial 24 is missing. Could be an issue with saving during data collection

## Excluded trials

1. Trial 26-30 (step width trials), because they require 1 force platform processing
2. All trials from subject 4, because the pelvis markers are too close together
3. These trials are bad and excluded:

 * Subject 3 trial 4: apparent sync error between COP and mocap
 * Subject 6 trial 21: super short (only 4 steps recorded)
 * Subject 6 trial 31: no mocap data
 * Subject 9 trial 14: apparent sync error between COP and mocap
 * Subject 10 trial 1: incorrect stepping on force platform
 
Some other trials are questionable, see bottom of this readme.

## Axes definition
 * Inverse dynamics is consistent with ground reaction force (https://c-motion.com/v3dwiki/index.php?title=Joint_Angle). 
 * Ground reaction force, left leg: x = lateral, y = anterior, z = up and right leg: x = medial, y = anterior, z = up
 * Rotations: x = flexion/extension, y = abduction/adduction, z = axial rotation

## 10 data fields
Data is in a (1x33) struct called "data", containing the following fields:

*grf* 

 * RCS: global
 * units: Newtons
 * size: Nx6, first 3 columns are left leg, last 3 are right leg
 * order: (1) med-lat, (2) for-aft, (3) vertical
 * method: de-offsetted, thresholded (i.e. zero'ed) and low-pass filtered, called FP1 and FP2 in V3D 

*grf2*

 * RCS: global
 * units: Newtons
 * size: Nx6, first 3 columns are left leg, last 3 are right leg
 * order: (1) med-lat, (2) for-aft, (3) vertical
 * method: low-pass filtered (in the ANALOG/PROCESSED folder in V3D). In V3D this variable is in Volts. I converted it to Newton using the provided calibration matrix. Be aware that some axes are flipped compared to grf

*jointpower*

 * RCS: local (proximal segment)
 * units: Watt/kg
 * size: Nx18, first 9 columns are left leg, last 9 are right leg
 * order: (1) ankle (x,y,z), (2) knee (x,y,z), (3) hip (x,y,z)
 * method: [JOINT-POWER] (https://c-motion.com/v3dwiki/index.php/Model_Based_Item:_JOINT_POWER), with negative axes and using local coordinate system (distal relative to proximal)

*jointpower2*

 * RCS: NA
 * units: Watt
 * size: Nx6, first 3 columns are left leg, last 3 are right leg
 * order: (1) ankle (summed), (2) knee (summed), (3) hip (summed)
 * method: [JOINT-POWER-SCALAR] (https://c-motion.com/v3dwiki/index.php/Model_Based_Item:_JOINT_POWER_SCALAR) using local coordinate system (distal relative to proximal)

*jointangle*

 * RCS: local (proximal segment)
 * units: degrees
 * size: Nx18, first 9 columns are left leg, last 9 are right leg
 * order: (1) ankle (x,y,z), (2) knee (x,y,z), (3) hip (x,y,z)
 * method: [JOINT-ANGLE] (https://c-motion.com/v3dwiki/index.php?title=Joint_Angle), with negative axes and using local coordinate system (distal relative to proximal)

*jointangle2*

 * RCS: local (proximal segment)
 * units: degrees
 * size: Nx18, first 9 columns are left leg, last 9 are right leg
 * order: (1) ankle (x,y,z), (2) knee (x,y,z), (3) hip (x,y,z)
 * calculation: [JOINT-ANGLE] (https://c-motion.com/v3dwiki/index.php?title=Joint_Angle), without negative axes and using local coordinate system (distal relative to proximal)

*jointmoment*

 * RCS: local (proximal segment)
 * units: N-m/kg
 * size: Nx18, first 9 columns are left leg, last 9 are right leg
 * order: (1) ankle (x,y,z), (2) knee (x,y,z), (3) hip (x,y,z)
 * method: [JOINT-MOMENT] (https://c-motion.com/v3dwiki/index.php?title=Model_Based_Item:_JOINT_MOMENT), with negative axes and using local coordinate system (distal relative to proximal), and Segment Coordinate System method

*jointmoment2*

 * RCS: local (proximal segment)
 * units: N-m
 * size: Nx18, first 9 columns are left leg, last 9 are right leg
 * order: (1) ankle (x,y,z), (2) knee (x,y,z), (3) hip (x,y,z)
 * method: [JOINT-MOMENT] (https://c-motion.com/v3dwiki/index.php?title=Model_Based_Item:_JOINT_MOMENT), without negative axes and using local coordinate system (distal relative to proximal), and Segment Coordinate System method

*jointforce*

 * RCS: the field has two subfields: loc and lab, referring to local and global reference coordinate systems respectively. local always means the proximal segment.
 * units: N/kg for loc and N for lab
 * size: Nx18, first 9 columns are left leg, last 9 are right leg
 * order: (1) ankle (x,y,z), (2) knee (x,y,z), (3) hip (x,y,z)
 * method: [JOINT-FORCE] (https://c-motion.com/v3dwiki/index.php?title=Model_Based_Item:_JOINT_FORCE)

*rotenergy*

 * RCS: NA
 * units: Joules
 * size: Nx6, first 3 columns are left leg, last 3 are right leg
 * order: (1) foot, (2) shank, (3) thigh
 * method: [ROTATIONAL-ENERGY-SCALAR] (https://c-motion.com/v3dwiki/index.php/ROTATIONAL_ENERGY_SCALAR)

*segmenvel*

 * RCS: global
 * units: m/s?
 * size: Nx21, first 9 columns are left leg, last 9 are right leg
 * order: (1) foot, (2) shank, (3) thigh and last 3 columns are pelvis
 * method: [SEG-VELOCITY] (https://c-motion.com/v3dwiki/index.php?title=Model_Based_Item:_SEG_VELOCITY)

*jointpos*

 * RCS: this field has two subfields: loc and lab, referring to local and global reference coordinate systems respectively. local always means the proximal segment.
 * units: m
 * size: Nx18, first 9 columns are left leg, last 9 are right leg
 * order: (1) foot, (2) shank, (3) thigh
 * method: [SEG_PROXIMAL_JOINT] (https://c-motion.com/v3dwiki/index.php/Model_Based_Item:_SEG_PROXIMAL_JOINT)

*transpower*

 * RCS: this field has two subfields: loc and lab, referring to local and global reference coordinate systems respectively. local always means the proximal segment.
 * units: W/kg
 * size: Nx18, first 9 columns are left leg, last 9 are right leg
 * order: (1) foot, (2) shank, (3) thigh
 * method: element-wise multiplication of jointforce with time derivative of jointtrans

## De-offsetting 
*grf2* is interesting because comparison with *grf* allows quantifying how well the de-offsetting was done. From *grf* alone you're not able to do that, because it is thresholded and therefore made equal to zero during swing (see folder de-offset). 

## Some thoughts and notes
**Joint power**: you might want to use *jointpower2* because it is expressed in Watts instead of Watts/kg. A good check might be multiplying *jointpower2* by the reported subject masses, to see whether that makes it equal to *jointpower* 

**Segment velocity**: currently assume to be in units m/s, but not entirely sure. Required to calculate peripheral work.

## Questionable trials
*Below is an overview of a document called "5 step weird trials" which can be found in a folder called Notes. The document is focused on Subjects 1-3 and 5-10 and is organized in two categories. A large set of notes for trials 1-25 and 31-33 is first and at the end there is a small section for trials 26-30-these are the step width trials. The document also contains figures for the following odd trials.*

*In the Notes folder there is also a table called "5 steps weird trials table" to easily see if trials have weird plots for GRF, Joint Moments, or both.*

*ground reaction force*

1. S2T25, S7T25, S9T25 ,S10T25- weird spiky graphs
2. S9T3- Trials 3 for all subjects has odd spiky ant./posterior forces but Subject 9 stands out

*Joint moment- Hip*

1. S3T3- smaller left hip moment compared to other subjects 

*Joint moment- Knee*

1. S1T1-odd step on left leg, plot looks different than others
2. S2- knee joint moment is lower than the others for most trials
3. S2T8-odd moment (see figure)
4. S8T3- left leg moment curve is flatter than the others
5. S8T5- weird moment curves, left and right leg over lap
6. S9T1- larger moment on the right than left (see figure)
7. S10T2- larger moment on the left than right, and curve is rather spiky
8. S10T6-left and right moment curve overlaps
9. S10T8- flatter moment curves compared to others

*Joint moment- Ankle*

1. S2- moment is lower than others for many trials
2. S9T4- weird joint moment for step 5, has wrong sign

*Subject 4 is also weird- See document for details.*

