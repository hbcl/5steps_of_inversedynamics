function[] = make_soft_figures_v2(datafolder,var)
% make_soft_figures_v2(datafolder)   plots inverse dynamics grid data
%   from a file named 'Wsoft.mat' to be found in datafolder.
%   Subplots of soft tissue power are displayed, and outlier or other
%   data points may be selected, followed by an input dialog for 
%   which panel (1-4) is sought. 
%
%   Program needs visualize_anklekin, etc. in path, as well as
%   additional data files 'Anklekin.mat' etc. 

st = [1:3:16, 17, 26, 31];
extracols = [2 2 2 2 2 0 8 4 2];
typsubj = 10;

% Use dialog to point to ID grid file 'Wsoft.mat'
if nargin == 0 || ~exist('datafolder')
  [filename, datafolder] = uigetfile('*.mat', ...
    'Select inverse dynamics grid file Wsoft.mat');
else
  filename = 'Wsoft.mat';
end

load(fullfile(datafolder, filename))

leglength = [0.8941 0.9398 1.04  0.8636 0.8636 0.9398 0.9906 0.99 0.9398 0.876];
prefreq = [106 124 106 116 116 102 102 102 110 110]/60; % BPM -> Hz
g = 9.81;
dimensionless = 1;

% exclude subject 4
Wsoft(:,4,:) = nan;

% exclude subject 9 trial 14 and subject 3 trial 4 (sync error)
Wsoft(4,3,:) = nan;
Wsoft(14,9,:) = nan;

% exclude subject 10 trial 1 (not walking straight)
Wsoft(1,10,:) = nan;

parms.g = 9.81;
Mg = mass .* parms.g;
MgL = Mg .* leglength;

%% Make dimensionless
Wsoftn = Wsoft ./ (repmat(MgL,33,1,2)); % net soft tissue work
Wcomn = Wcom ./ (repmat(MgL,33,1,2)); % net COM work
Wjointn = Wjoint ./ (repmat(MgL,33,1,2)); % net summed joint work
Wcolln = Wcoll ./ (repmat(MgL,33,1,2)); % net COM collision work
Wsoftcolln = Wsoftcoll ./ (repmat(MgL,33,1,2)); % net soft tissue collision work
Fpeakn = Fpeak ./ Mg;

%% Reorganize based on freq and SL and speed
N = 10; % number of subjects
M = 10; % numbers of trials

% define variables
vCSLs = nan(M,N);
vCFRs = nan(M,N);
vPRFs = nan(M,N);
relfreq = nan(M,N);

ycSL = nan(M,N,2);
ycFR = nan(M,N,2);
yPREF = nan(M,N,2);
ycSP = nan(M,N,2);

% trials
icSL = [1 4 7 12 15 20 33];
icFR = [3 6 9 10 13 20 31];
iPREF = [2 5 8 11 14 16 20 32];
icSP = [17:20, 23:25];

% corresponding speeds / step lengths
vCSLs(1:7,:) = repmat([.7 .9 1.1 1.6 1.8 1.25 1.4]',1,N);
vCFRs(1:7,:) = repmat([.7 .9 1.1 1.6 1.8 1.25 1.4]',1,N);
vPRFs(1:8,:) = repmat([.7 .9 1.1 1.6 1.8 2.0 1.25 1.4]',1,N);
relfreq(1:7,:) = repmat([.7 .8 .9 1 1.1 1.2 1.3]',1,N);

% convert step frequency into step length
absfreq = relfreq .* repmat(prefreq, M,1); % [Hz]
sCSPs = 1.25 * 1./absfreq; % [m]

if strcmp(var, 'soft') == 1
    y = Wsoftn;
else
    y = Wcolln;
end

% assign soft tissue power to conditions
ycSL(1:7,:,:) = y(icSL,:,:);
ycFR(1:7,:,:) = y(icFR,:,:);
yPREF(1:8,:,:) = y(iPREF,:,:);
ycSP(1:7,:,:) = y(icSP,:,:);
   

%% make dimensionless (optional)
if dimensionless == 1
    vCSLs = vCSLs ./ repmat(sqrt(g*leglength),M,1);
    vCFRs = vCFRs ./ repmat(sqrt(g*leglength),M,1);
    vPRFs = vPRFs ./ repmat(sqrt(g*leglength),M,1);
    sCSPs = sCSPs ./ repmat(leglength,M,1);
end

%% Plot soft tissue work
close all
fig = figure(11);
color = get(gca,'colororder');
syms = 'ox+*sdv^<>';


for i = 1:2
    for j = 1:10
        subplot(221)
        plot(vCSLs(:,j), ycSL(:,j,i), syms(j),'color',color(i,:)); hold on
        title('constant step length')

        subplot(222)
        plot(vCFRs(:,j), ycFR(:,j,i),syms(j),'color',color(i,:)); hold on
        title('constant frequency')

        subplot(223)
        plot(vPRFs(:,j), yPREF(:,j,i),syms(j),'color',color(i,:)); hold on
        title('preferred walking')

        subplot(224)
        plot(sCSPs(:,j), ycSP(:,j,i),syms(j),'color',color(i,:)); hold on
        title('constant speed')
    end
end

% make nicer
for i = 1:4
    subplot(2,2,i)
    xlim([0 .9])
    box off
    ylabel('soft tissue power')
    xlabel('walking speed')
    hold on; plot([0 10], [0 0], 'k-')
end

xlim([.5 1.3])
xlabel('step length')
subplot(222); legend('1','2','3','4','5','6,','7','8','9','10','location','best')

%% Create look-up table
% trials on rows, persons on columns
for i = 1:10
    lookup.p(1:10,i) = i;
    lookup.t(i,1:10) = i;
end

% copy in 3rd dimension
lookup.p(:,:,2) = lookup.p(:,:,1);
lookup.t(:,:,2) = lookup.t(:,:,1);

% ascribe leg number
lookup.l(1:10,1:10,1) = 1;
lookup.l(1:10,1:10,2) = 2;

cont =1;
%% Click on any data point and figure out where it came from
% while cont == 1
    dcm_obj = datacursormode(fig);
    set(dcm_obj,'DisplayStyle','datatip','SnapToDataVertex','off','Enable','on')

    pause
    c_info = getCursorInfo(dcm_obj);

    panel = input('whichpanel?');

    if panel == 1, idx = find(ycSL == c_info.Position(2));  
    elseif panel == 2, idx = find(ycFR == c_info.Position(2));
    elseif panel == 3, idx = find(yPREF == c_info.Position(2));
    elseif panel == 4, idx = find(ycSP == c_info.Position(2));
    end

    p = lookup.p(idx);
    t = lookup.t(idx);
    % l = lookup.l(idx);

    if panel == 1, tc = icSL(t);
    elseif panel == 2, tc = icFR(t);
    elseif panel == 3, tc = iPREF(t);
    elseif panel == 4, tc = icSP(t);
    end

    %% load that data
    % soft tissue power is COM+peri power minus joint power. Therefore,
    % positive soft tissue work means either negative joint work or positive
    % COM work. Test whether joint work is indeed negative!

    visualize_anklekin(datafolder,p,tc)
    visualize_kneekin(datafolder,p,tc)
    visualize_hipkin(datafolder,p,tc)
    
%     keyboard
    
%     load(['p', num2str(p),'_5steps_data.mat'],'data')
%     
%     figure
%     plot(data(tc).grf)

%     cont = input('want to continue?');
%     
% close(['Hipkin trial: ', num2str(tc)])
% close(['Kneekin trial: ', num2str(tc)])
% close(['Anklekin trial: ', num2str(tc)])
end

