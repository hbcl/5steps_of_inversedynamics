clear all; close all; clc

addpath(genpath(cd))
addpath(genpath('C:\Users\Tim\OneDrive - University of Calgary\shared 5steps data'))

trials = [17, 25];
subjs = [1:3, 5:10];

mass = [81.8 57.3 97.5 60.3 56.7 72.6 86.2 88.6 77 57]; % from excel sheets

Mknee = nan(101,10,2);
Mpeak = nan(10,2);
for subj = subjs
    load(['p',num2str(subj),'_5steps_data.mat'])
    
    for trial = 1:2
        
        [hsl,~,hsr] = invDynGrid_getHS_TO(data(trials(trial)).grf(:,1:3), data(trials(trial)).grf(:,4:6), 10);
        hsl_mo = ceil(hsl/10); hsr_mo = ceil(hsr/10);
        
        Mknee_left = mean(interpolate_to_percgaitcycle(data(trials(trial)).jointmoment(:,4),hsl_mo,101),2,'omitnan');
        Mknee_right = mean(interpolate_to_percgaitcycle(data(trials(trial)).jointmoment(:,13),hsr_mo,101),2,'omitnan');
        
        Mknee(:,subj, trial) = mean([Mknee_left Mknee_right],2);
        Mpeak(subj,trial) = max(Mknee(:,subj,trial));
        
        Fpeak(subj,trial) = max(data(trials(trial)).grf(:,3));
       

    end
end

%% Plotting
plot([0:100], mean(Mknee(:,:,1),2,'omitnan'),'linewidth',2); hold on
plot([0:100], mean(Mknee(:,:,2),2,'omitnan'),'linewidth',2);

color = get(gca,'colororder');

plot([0:100], mean(Mknee(:,:,1),2,'omitnan') + std(Mknee(:,:,1),1,2,'omitnan'),'linewidth',1, 'color', color(1,:))
plot([0:100], mean(Mknee(:,:,1),2,'omitnan') - std(Mknee(:,:,1),1,2,'omitnan'),'linewidth',1, 'color', color(1,:))

plot([0:100], mean(Mknee(:,:,2),2,'omitnan') + std(Mknee(:,:,2),1,2,'omitnan'),'linewidth',1, 'color', color(2,:))
plot([0:100], mean(Mknee(:,:,2),2,'omitnan') - std(Mknee(:,:,2),1,2,'omitnan'),'linewidth',1, 'color', color(2,:))

%% Fpeak


