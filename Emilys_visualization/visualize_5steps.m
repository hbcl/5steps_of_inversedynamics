function[] = visualize_5steps(datadir,subj,trial,type)

cd(datadir)
color = get(gca,'colororder');
load(['p',num2str(subj),'_5steps_data.mat'])

disp(['subj:', num2str(subj)])
disp(['trial:', num2str(trial)])

% find the columns
if strcmp(type,'grf'), var = data(trial).grf;
elseif strcmp(type,'jointmoment'), var = data(trial).jointmoment;
elseif strcmp(type,'jointpower'), var = data(trial).jointpower;
elseif strcmp(type,'jointangle'), var = data(trial).segmenangle;
end
            

if sum(isnan(data(trial).grf),'all')<1
    hsl = invDynGrid_getHS_TO(data(trial).grf(:,1:3), data(trial).grf(:,4:6), 10);

    if ~strcmp(type,'grf'), hsl = round(hsl/10) + 1;
    end
    
    for k = 1:(length(hsl)-1)

        for j = 1:3
            if strcmp(type,'grf')
                subplot(211), plot(var(hsl(k):hsl(k+1),j),'color',color(j,:)); hold on; title('left ground reaction force');
                subplot(212), plot(var(hsl(k):hsl(k+1),j+3),'color',color(j,:)); hold on; title('left ground reaction force')  
                
            else
                subplot(231), plot(var(hsl(k):hsl(k+1),j+0),'color',color(j,:)); hold on; title('left ankle'); 
                subplot(232), plot(var(hsl(k):hsl(k+1),j+3),'color',color(j,:)); hold on; title('left knee')                  
                subplot(233), plot(var(hsl(k):hsl(k+1),j+6),'color',color(j,:)); hold on; title('left hip')  
                subplot(234), plot(var(hsl(k):hsl(k+1),j+9),'color',color(j,:)); hold on; title('right ankle')      
                subplot(235), plot(var(hsl(k):hsl(k+1),j+12),'color',color(j,:)); hold on; title('right knee')  
                subplot(236), plot(var(hsl(k):hsl(k+1),j+15),'color',color(j,:)); hold on; title('right hip')                  
            end
            
        end
    end
    if strcmp(type,'grf')
         legend('med-lat','for-aft','vertical')
    else
        legend('flex/ext','abd/add','axial')
    end
end
end





