%Subject 4, hip measurements compared to height
%From V3D, ASIS is the distance betwen the left and right ASIS and AP is
%the distance from the midpoint of the two ASIS to the sacrum. indices 1-10
%contains the data for Subject 1-10
%all values are in metres
Height=[1.73, 1.64, 1.9, 1.6, 1.62, 1.83, 1.89, 1.85, 1.78, 1.72];
HH_ASIS=[0.270247, 0.218272, 0.268542, 0.165625, 0.204197, 0.292914, 0.271178, 0.291679, 0.254703, 0.260243];
HH_AP=[0.069113, 0.063433, 0.084544, 0.06063, 0.059886, 0.084079, 0.084187, 0.07337, 0.064783, 0.060373];

ratio_ASIS=HH_ASIS./Height
ratio_AP=HH_AP./Height

averageASIS=mean(ratio_ASIS);
averageAP=mean(ratio_AP);

sdASIS=std(ratio_ASIS);
sdAP=std(ratio_AP);

S4ASIS=ratio_ASIS(4);
S4AP=ratio_AP(4);

deltaASIS=averageASIS-S4ASIS;
deltaAP=averageAP-S4AP;

disp('The average ratio of ASIS distance to height is')
disp(num2str(averageASIS))
disp('and S4 has a ratio of')
disp(S4ASIS)
% disp('The average ratio of AP distance to height is') 
% disp(num2str(averageAP))

disp('S4 ratio is')
disp(num2str(deltaASIS))
disp('less than the average for ASIS ratio but the standard deviation is')
disp(num2str(sdASIS))

% disp('S4 is')
% disp(num2str(deltaAP))
% disp('less than the average for AP ratio but the standard deviation is')
% disp(num2str(sdAP))

