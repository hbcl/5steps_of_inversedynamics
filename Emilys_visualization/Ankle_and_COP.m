function Ankle_and_COP(datafolder, codefolder, subjects, trials)

%Ankle position and COP
%Plots the ankle position and the COP so they can be compared in order to
%spot potencial syncing errors as these values should be fairly similar

%Inputs: datafolder- a string that has the full path name to the folder 
% where the 5 step data is located
% code folder- a string specifying where this code is. The path can stop at
% 5stepinversedynamics repository no need to include the subfolders
%subjects is a number or array of numbers for any of the subjects from 1:10
%trials is a number or array of numbers containing any trials from 1:33

%Outputs: a graph with time on the x axis and position on the y axis so you
%can see the ankle and cop positions relative to one another

addpath(genpath(datafolder))
addpath(genpath(codefolder))

for i=subjects
    load (['p', num2str(i),'_5steps_data.mat'])
    figure('name',(['subj:', num2str(i), 'trial #: ', num2str(trials)]))
    for trial = trials
        left_swing = data(trial).grf(:,3) < 20;
        right_swing = data(trial).grf(:,6) < 20;    
        data(trial).cop(left_swing,1:3) = nan;
        data(trial).cop(right_swing,4:6) = nan;    
        COPi = interp1(1:length(data(trial).cop), data(trial).cop(:,2), linspace(0,length(data(trial).cop), length(data(trial).jointpos.lab)));    
        t = 0:(1/120):(1/120)*(length(COPi)-1);    

        plot(t,data(trial).jointpos.lab(:,2),'linewidth',2); hold on
        plot(t,COPi,'linewidth',2)    
        xlabel('time (s)')
        ylabel('for-aft position (m)')
        axis([0 5 0 1.4])
    end
    legend('ankle', 'COP')
end
