function visualize_GRF(datafolder, codefolder, subjects, trials)
%function will plot the subject of interest's GRF data on the left and right
%leg in the first 2 columns and then the mean of the other subjects in the 
%right 2 columns with the subject of interest in bold
% This function excluded subject 4 but it can easily be included by adding
% it on row 23

%input single subj/trial at a time for the input subjects and trials
%datafolder- a string that has the full path name to the folder with the 5
%step data
%codefolder- a sting that has the full path name to the code, the path can
%stop at 5stepinversedynamics repository instead of the subfolder
% code folder- a string specifying where this code is. The path can stop at
% 5stepinversedynamics repository no need to include the subfolders
addpath(genpath(codefolder))
addpath(genpath(datafolder))

mass = [81.8 57.3 97.5 60.3 56.7 72.6 86.2 88.6 77 57]; % from excel sheets

GRFx = nan(101,5,2);
GRFy = nan(101,5,2);
GRFz = nan(101,5,2);

GRFxmean= nan(101,10,5,2);
GRFymean= nan(101,10,5,2);
GRFzmean= nan(101,10,5,2);


%% Data for all subjects
for j=[1:3,5:10]
    load(['p',num2str(j),'_5steps_data'])
    for trial=trials
        if isnan(data(trial).grf)==1
           disp (['Subject ', num2str(j), ' is missing for this trial'])
            continue
        else
            disp(j)

            % Extract grfs
            GRFL=[data(trial).grf(:,1) data(trial).grf(:,2) data(trial).grf(:,3)];
            GRFR=[data(trial).grf(:,4) data(trial).grf(:,5) data(trial).grf(:,6)];

            % Make a copy
            GRFL_copy = GRFL;
            GRFR_copy = GRFR;

            % Get heelstrikes
            HSL = invDynGrid_getHS_TO(GRFL_copy,GRFR_copy, 40);
            HSL = unique(HSL);

            GRFxmean(:,j,1)= mean(interpolate_to_percgaitcycle(GRFL(:,1),HSL,101),2,'omitnan')/mass(j);
            GRFxmean(:,j,2)= mean(interpolate_to_percgaitcycle(GRFR(:,1),HSL,101),2,'omitnan')/mass(j);

            GRFymean(:,j,1)= mean(interpolate_to_percgaitcycle(GRFL(:,2),HSL,101),2,'omitnan')/mass(j);
            GRFymean(:,j,2)= mean(interpolate_to_percgaitcycle(GRFR(:,2),HSL,101),2,'omitnan')/mass(j);

            GRFzmean(:,j,1)= mean(interpolate_to_percgaitcycle(GRFL(:,3),HSL,101),2,'omitnan')/mass(j);
            GRFzmean(:,j,2)= mean(interpolate_to_percgaitcycle(GRFR(:,3),HSL,101),2,'omitnan')/mass(j);
        end
    end    
end

%% Data for subject of interest
for subj=subjects
    load(['p',num2str(subj),'_5steps_data'])
    for trial=trials
       if isnan(data(trial).grf)==0
            % Extract grfs
            grfl = data(trial).grf(:,1:3);
            grfr = data(trial).grf(:,4:6);

            % Make a copy
            grfl_copy = grfl;
            grfr_copy = grfr;

            % Threshold at 20 N
            grfl_copy(grfl_copy(:,3)<20,:) = 0;
            grfr_copy(grfr_copy(:,3)<20,:) = 0;     

            % Get heelstrikes
            hsl = invDynGrid_getHS_TO(grfl_copy,grfr_copy, 40);
            hsl = unique(hsl);
            %4th col- 1=left, 2=right leg

            GRFx(:,1:5,1) =interpolate_to_percgaitcycle(grfl(:,1),hsl,101)/mass(subj);
            GRFx(:,1:5,2) =interpolate_to_percgaitcycle(grfr(:,1),hsl,101)/mass(subj);

            GRFy(:,1:5,1) = interpolate_to_percgaitcycle(grfl(:,2),hsl,101)/mass(subj);
            GRFy(:,1:5,2) = interpolate_to_percgaitcycle(grfr(:,2),hsl,101)/mass(subj);

            GRFz(:,1:5,1) = interpolate_to_percgaitcycle(grfl(:,3),hsl,101)/mass(subj);
            GRFz(:,1:5,2) = interpolate_to_percgaitcycle(grfr(:,3),hsl,101)/mass(subj);
       end
    end


    %% Plot for subject of interet
    figure('name', (['subj: ', num2str(subj),', trial: ', num2str(trial)]))

    %i=1:5 plot all 5 steps on top of one another
    %subplots 1, 2, 5, 6, 9, and 10-subject of interest GRF
    %subplots 3,4,6, 7,and 11,12- mean of 5 steps for each of the 10 subjects
    %plotted one on top of the other

    for i=1:5
        subplot (3,4,1)
        title ('Left leg')
        plot(GRFx(:,i,1))
        ylabel ('med/lat. GRF (N/kg)')
        hold on

        subplot (3,4,2)
        title ('Right leg')
        plot(GRFx(:,i,2))
        ylabel ('med/lat. GRF (N/kg)')
        hold on

        subplot (3,4,5)
        plot(GRFy(:,i,1))
        ylabel ('ant/post. GRF (N/kg)');
        hold on

        subplot (3,4,6)
        plot(GRFy(:,i,2));
        ylabel ('ant/post. GRF (N/kg)');
        hold on

        subplot (3,4,9);
        plot(GRFz(:,i,1));
        ylabel ('vertical GRF (N/kg)');
        hold on;

        subplot (3,4,10);
        plot(GRFz(:,i,2));
        ylabel ('vertical GRF (N/kg)');
        hold on
    end
    
    %% Plot all subject means
    %plots all subjects means across steps

    %normalized GRF for subjects mass (GRF(N/kg))

    for j=[1:3,5:10]
        subplot (3,4,3)
        title ('Subject Means Left')
        plot(GRFxmean(:,:,1)); hold on
        plot(GRFxmean(:,subj,1),'k','linewidth',2);
        ylabel ('med/lat. GRF (N/kg)')
        hold on
        subplot (3,4,7)
        plot(GRFymean(:,:,1));hold on
        plot(GRFymean(:,subj,1),'k','linewidth',2); 
        ylabel ('ant/post. GRF (N/kg)');
        hold on;
        subplot (3,4,11)
        plot(GRFzmean(:,:,1));hold on
        plot(GRFzmean(:,subj,1),'k','linewidth',2);
        ylabel ('vertical GRF (N/kg)');
        hold on

        subplot (3,4,4)
        title ('Subject Means Right') %%add if statement for subject of interest to be in red
        plot(GRFxmean(:,:,2)); hold on
        plot(GRFxmean(:,subj,2),'k','linewidth',2);
        ylabel ('med/lat. GRF (N/kg)')
        hold on
        subplot (3,4,8)
        plot(GRFymean(:,:,2)); hold on
        plot(GRFymean(:,subj,2),'k','linewidth',2);
        ylabel ('ant/post. GRF (N/kg)');
        hold on;
        subplot (3,4,12)
        plot(GRFzmean(:,:,2)); hold on
        plot(GRFzmean(:,subj,2),'k','linewidth',2);
        ylabel ('vertical GRF (N/kg)');
        hold on

        for k=1:12
            subplot (3,4,k)
            axis tight
            xlabel ('%gait cycle')
        end

        subplot (3,4,3)
        title ('Subject Means Left')
        subplot (3,4,4)
        title ('Subject Means Right')
    end
end
end
