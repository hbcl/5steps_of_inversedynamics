function GRFplots(datafolder, codefolder, subjects, trials)
% GRFplots produces 2 graphs. The first plot-shows??? 
%The second has 3 subplots for GRF. It plots the left and right
% leg on on top of the other and the 3 subplots are the GRF in the x, y,
% and z directions
% Inputs: datafolder- a string that has the full path name to the folder 
% where the 5 step data is located
% code folder- a string specifying where this code is. The path can stop at
% 5stepinversedynamics repository no need to include the subfolders
% Subjects can be a single number from 1:10 or an array including any
% combinations of subjects from 1 to 10
% Trials can be a single trial number from 1:33 or an array of trials
% Ouputs: For each subject/trial combination, a single plot will appear,
% pause function is used so press a key to get the next plot to appear


%% 5 steps analysis
addpath(genpath(datafolder));
addpath(genpath(codefolder));
cd(datafolder);

%Translational data for the hip
fs = 120; % Hz
dt = 1/fs;

load('Hipkin')
load('Kneekin')
load('Anklekin')

<<<<<<< Updated upstream:Emilys_visualization/GRFplots.m
%% Figure 1: whole trial figure: ground reaction forces
date = '0710';
=======
%% whole trial figure: ground reaction forces
date = '1019';
>>>>>>> Stashed changes:6DOF analysis/emilys_script.m
midparts = {'25Force6Marker_walking_070to110_and_160to200_data', '25Force6Marker_walking_125_and_140_data'};
subjnames = {'eu' ,'kel', 'kez', 'leb', 'mmm', 'pbc', 'pga', 'smo', 'wj', 'yg'};
fs = 1200;
titles = {'f1x','f1y','f1z','f1z - raw','f2x','f2y','f2z','f2z - raw'};

for subj = subjects
        subjname = char(subjnames(subj));
%         load(['p',num2str(subj),'_5steps_data'])
    for trial = trials
        
        if trial<17, CMOfile = 1;
        else CMOfile = 2;
        end
%Mac
         cd([datafolder,'/rawdata/', subjname])
%Pc
%         cd([datafolder,'\rawdata\', subjname])
        
        if exist([subjname, midparts{CMOfile}, date,'_TG', num2str(trial),'.mat'],'file')
            load([subjname, midparts{CMOfile}, date,'_TG', num2str(trial),'.mat'])
            
            zeror = double(zerorange{1}) * 10;
        else, continue
        end
        

        figure('name', (['subj: ', num2str(subj),', trial: ', num2str(trial)]))
        
        subplot(3,1,1)
        if isempty(f1{1})==1
            subplot(3,1,1)
            plot (0,0)
            subplot(3,1,2)
            subplot (3,1,3)
        elseif isempty(f1{1})==0

            for i = 1:3
                subplot(2,4,i)
                plot(f1{1}(:,i)); hold on
                plot([zeror(1,1) zeror(1,1)], [min(f1{1}(:,i)) max(f1{1}(:,i))],'k')
                plot([zeror(2,1) zeror(2,1)], [min(f1{1}(:,i)) max(f1{1}(:,i))],'k')
                axis tight; xlim([zeror(1,1)-fs zeror(2,1)+fs])
                if i == 3, subplot(2,4,4); plot(f1zanalog{1});
                    axis tight; xlim([zeror(1,1)-fs zeror(2,1)+fs])
                end
                
                subplot(2,4,i+4)
                plot(f2{1}(:,i)); hold on
                plot([zeror(1,2) zeror(1,2)], [min(f2{1}(:,i)) max(f2{1}(:,i))],'k')
                plot([zeror(2,2) zeror(2,2)], [min(f2{1}(:,i)) max(f2{1}(:,i))],'k')
                axis tight; xlim([zeror(1,2)-fs zeror(2,2)+fs])
                
                if i == 3, subplot(2,4,8); plot(f2zanalog{1});
                    axis tight; xlim([zeror(1,2)-fs zeror(2,2)+fs])
                end
            end
            
            
            
            for i = 1:8
                subplot(2,4,i)
                title(titles{i})
            end
            
        end
        
        pause
        close
    end
end



%% Figure 2: 5 step figure: ground reaction forces

for subj =subjects
        load(['p',num2str(subj),'_5steps_data'])
    for trial =trials
         figure('name', (['subj: ', num2str(subj),', trial: ', num2str(trial)]))
        
        subplot(3,1,1)
        if isnan(data(trial).grf)==1
            subplot(3,1,1)
            plot (0,0)
            subplot(3,1,2)
            subplot (3,1,3)
        elseif isnan(data(trial).grf)==0
            subplot(3,1,1)
            plot(data(trial).grf(:,[1 4]))
            subplot(3,1,2)
            plot(data(trial).grf(:,[2 5]))
            subplot(3,1,3)
            plot(data(trial).grf(:,[3 6]))
        end
        for k=1:3
            subplot(3,1,k)
            ylabel ('Force (N)')
            xlabel('5step frames')
        end
        pause
        close
    end
end
        
end