function Joint_moment_plots(datafolder, codefolder, subjects, trials, joint)

%The function plots the joint moments for the left and right leg on
%top of one another and includes 3 subplots for the 3 axes of rotation
%Inputs: datafolder- a string that has the full path name to the folder 
% where the 5 step data is located
% code folder- a string specifying where this code is. The path can stop at
% 5stepinversedynamics repository no need to include the subfolders
%subjects and trials can be defined as a single or as an array of any numbers
%from 1:10 for subjects and 1:33 fr trials
%joint can be defined as 'ankle', 'knee', or 'hip'

addpath(genpath(datafolder))
addpath(genpath(codefolder))

for subj =subjects
        load(['p',num2str(subj),'_5steps_data'])
    for trial =trials
         figure('name', (['subj: ', num2str(subj),', trial: ', num2str(trial)]))
        
         %Figures from top to bottom are ankle/knee/hip joint moment in the x, y, and z
         %Plots all 5 steps on the right leg(blue) on top of the left
         %leg(red) plot
         
         if strcmp(joint, 'ankle')
             num=[1,2,3,10,11,12];
         elseif strcmp(joint, 'knee') 
             num=[4,5,6,13,14,15];
         elseif strcmp(joint, 'hip')
             num=[7,8,9,16,17,18];
         end
         
        subplot(3,1,1)
        if isnan(data(trial).jointmoment)==1
            subplot(3,1,1)
            plot (0,0)
            subplot(3,1,2)
            subplot (3,1,3)   
        elseif isnan(data(trial).jointmoment)==0
            subplot(3,1,1)
            plot(data(trial).jointmoment(:,[num(1) num(4)]))
            xlabel ('5step frames')
            ylabel ('Joint Moment (Nm/kg)')
            subplot(3,1,2)
            plot(data(trial).jointmoment(:,[num(2) num(5)]))
            xlabel ('5step frames')
            ylabel ('Joint Moment (Nm/kg)')
            subplot(3,1,3)
            plot(data(trial).jointmoment(:,[num(3) num(6)]))
            xlabel ('5step frames')
            ylabel ('Joint Moment (Nm/kg)')
    
        end
        pause
    end
end
end

