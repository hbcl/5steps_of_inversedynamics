clear all; close all; clc

load('p2_5steps_data.mat')

for t = 1

titles = {'left ankle', 'left knee', 'left hip','right ankle', 'right knee', 'right hip'}; 

if ~isnan(data(t).transpower.loc)

    for i = 1:6

        j = (i-1)*3 + 1;

        figure(t);
        subplot(2,3,i)
        plot(sum(data(t).transpower.loc(:,j:(j+2)),2)); hold on
        plot(sum(data(t).transpower.lab(:,j:(j+2)),2),'--');

        xlabel('Sample #')
        ylabel('Power (W/kg)')
        title(titles{i});
    end
end
legend('local method' ,'global method')
end