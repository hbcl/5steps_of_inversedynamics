function[]=visualize_5stepdata(datafolder,trials,legs, directions, datatype)
%Shows the mean of all subjects compared to each individual subject
%trials- input a number from 1-33 or a range of trials (ex.1:4)
%legs-input 'left' for left leg, 'right' for right leg or 'both' for both graphs
%directions-input x, y, or z,
%datatype-input 'rotation' for rotational data (Moment, Angle, Power) and
%'translation' for translational data (Joint Force, Translation, Translational Power)

cd(datafolder)
load('Hipkin.mat')
load('Kneekin.mat')
load('Anklekin.mat')

if strcmp(legs,'left')==1
    leg=1;
elseif strcmp(legs,'right')==1
    leg=2;
elseif strcmp(legs,'both')==1
    leg=1:2;
end

if strcmp(directions,'x')==1
   title_direction='Medial/Lateral';
   direction=1;
elseif strcmp(directions,'y')==1
    title_direction='Ant/Posterior';
    direction=2;
elseif strcmp(directions,'z')==1;
    title_direction= 'Vertical';
    direction=3;
end

if strcmp(datatype,'rotation')==1
    data_title='Rotational Data';
elseif strcmp(datatype,'translation')==1
    data_title='Translational Data';
end
            

for trial=trials
    
%% Left leg plots-Rotation
    for i=leg
        if i==1 && strcmp(datatype,'rotation')==1
           figure('name',[data_title 'Left Leg trial: ', num2str(trial), 'direction', ])

            %Moment
            subplot(331)
            plot(mean(Mhipl(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(Mhipl(:,:,trial,direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Left hip moment (Nm/kg)')
            ylabel('Moment(Nm/kg)')
            
            subplot(332)
            plot(mean(Mkneel(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(Mkneel(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Left knee moment (Nm/kg)')
            ylabel('Moment(Nm/kg)')
           
            subplot(333)
            plot(mean(Manklel(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(Manklel(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Left ankle moment (Nm/kg)')
            ylabel('Moment(Nm/kg)')

            %Angle
            subplot(334)
            plot(mean(Ahipl(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(Ahipl(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Left hip angle (deg)')
            ylabel('Angle(deg)')

            subplot(335)
            plot(mean(Akneel(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(Akneel(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Left knee angle (deg)')
            ylabel('Angle(deg)')
           

            subplot(336)
            plot(mean(Aanklel(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(Aanklel(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Left ankle angle (deg)')
            ylabel('Angle(deg)')

            %Power
            subplot(337)
            plot(mean(Phipl(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(Phipl(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Left hip power (W/kg)')
            ylabel('Joint Power(W/kg)')

            subplot(338)
            plot(mean(Pkneel(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(Pkneel(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Left knee joint power (W/kg)')
            ylabel('Joint Power(W/kg)')

            subplot(339)
            plot(mean(Panklel(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(Panklel(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Left ankle joint power (W/kg)')
            ylabel('Joint Power(W/kg)')
          
            for i = 1:9
                subplot(3,3,i)
                xlim([0 100])
                xlabel('Percentage gait cycle');
                axis tight;
            end
        end



%% Right Leg Plots-Rotation
         if i==2 && strcmp(datatype, 'rotation')==1
            figure('name',[data_title 'Right Leg trial: ', num2str(trial), 'direction', ])

            %Moment
            subplot(331)
            plot(mean(Mhipr(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(Mhipr(:,:,trial,direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Right hip moment (Nm/kg)')
            ylabel('Moment(Nm/kg)')

            subplot(332)
            plot(mean(Mkneer(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(Mkneer(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Right knee moment (Nm/kg)')
            ylabel('Moment(Nm/kg)')

            subplot(333)
            plot(mean(Mankler(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(Mankler(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Right ankle moment (Nm/kg)')
            ylabel('Moment(Nm/kg)')
            
            %Angle
            subplot(334)
            plot(mean(Ahipr(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(Ahipr(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Right hip angle (deg)')
            ylabel('Angle(deg)')
            
            subplot(335)
            plot(mean(Akneer(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(Akneer(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Right knee angle (deg)')
            ylabel('Angle(deg)')

            subplot(336)
            plot(mean(Aankler(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(Aankler(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Right ankle angle (deg)')
            ylabel('Angle(deg)')

            %Power
            subplot(337)
            plot(mean(Phipr(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(Phipr(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Right hip power (W/kg)')
            ylabel('Joint Power(W/kg)')

            subplot(338)
            plot(mean(Pkneer(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(Pkneer(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Right knee joint power (W/kg)')
            ylabel('Joint Power(W/kg)')

            subplot(339)
            plot(mean(Pankler(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(Pankler(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Right ankle joint power (W/kg)')
            ylabel('Joint Power(W/kg)')
            
            for i = 1:9
                subplot(3,3,i)
                xlim([0 100])
                xlabel('Percentage gait cycle');
                axis tight;
            end
         end
         
         
 %% Left Leg Plot-Translation
        if i==1 && strcmp(datatype, 'translation')==1
           figure('name',[data_title 'Left Leg trial: ', num2str(trial), 'direction', ])
     
            %Joint Force
            figure('name',[title_direction, 'Translation and Power Hipkin Trial-Left: ', num2str(trial)])
            subplot(331)
            plot(mean(JFhipl_pc(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(JFhipl_pc(:,:,trial,direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Left hip joint force (N)')
            ylabel('Force(N)')

            subplot(332)
            plot(mean(JFkneel_pc(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(JFkneel_pc(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Left knee joint force (N)')
            ylabel('Force(N)')

            subplot(333)
            plot(mean(JFanklel_pc(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(JFanklel_pc(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Left ankle joint force (N)')
            ylabel('Force(N)')

            %Translation
            subplot(334)
            plot(mean(Thipl(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(Thipl(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Left hip translation (m)')
            ylabel('Translation(m)')

            subplot(335)
            plot(mean(Tkneel(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(Tkneel(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Left knee translation (m)')
            ylabel('Translation(m)')

            subplot(336)
            plot(mean(Tanklel(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(Tanklel(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Left ankle translation (m)')
            ylabel('Translation(m)')

            %Translational Power
            subplot(337)
            plot(mean(PThipl_pc(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(PThipl_pc(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Left hip joint power (W/kg)')
            ylabel('Joint Power(W/kg)')

            subplot(338)
            plot(mean(PTkneel_pc(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(PTkneel_pc(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Left knee joint power (W/kg)')
            ylabel('Joint Power(W/kg)')

            subplot(339)
            plot(mean(PTanklel_pc(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(PTanklel_pc(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Left ankle joint power (W/kg)')
            ylabel('Joint Power(W/kg)')

            for i = 1:9
                subplot(3,3,i)
                xlim([0 100])
                xlabel('Percentage gait cycle');
                axis tight;
            end
        end

       
%% Right Leg Plot-Translation
        if i==2 && strcmp(datatype, 'translation')==1
            %Joint Force
            figure('name',[title_direction ' Translation and Power Hipkin Trial-Right: ', num2str(trial)])
            subplot(331)
            plot(mean(JFhipr_pc(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(JFhipr_pc(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Right hip joint force (N)')
            ylabel('Force(N)')

            subplot(332)
            plot(mean(JFkneer_pc(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(JFkneer_pc(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Right knee joint force (N)')
            ylabel('Force(N)')

            subplot(333)
            plot(mean(JFankler_pc(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(JFankler_pc(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Right ankle joint force (N)')
            ylabel('Force(N)')

            %Translation
            subplot(334)
            plot(mean(Thipr(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(Thipr(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Right hip translation (m)')
            ylabel('Translation(m)')

            subplot(335)
            plot(mean(Tkneer(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(Tkneer(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Right knee translation (m)')
            ylabel('Translation(m)')

            subplot(336)
            plot(mean(Tankler(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(Tankler(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Right ankle translation (m)')
            ylabel('Translation(m)')

            %Translational Power
            subplot(337)
            plot(mean(PThipr_pc(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(PThipr_pc(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Right hip joint power (W/kg)')
            ylabel('Joint Power(W/kg)')

            subplot(338)
            plot(mean(PTkneer_pc(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(PTkneer_pc(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Right knee joint power (W/kg)')
            ylabel('Joint Power(W/kg)')

            subplot(339)
            plot(mean(PTankler_pc(:,:,trial,direction),2,'omitnan'),'k','linewidth',3); hold on
            plot(PTankler_pc(:,:,trial, direction)); hold on
            plot([0 100], [0 0], 'k-')
            title('Right ankle joint power (W/kg)')
            ylabel('Joint Power(W/kg)')

            for i = 1:9
                subplot(3,3,i)
                xlim([0 100])
                xlabel('Percentage gait cycle');
                axis tight;
            end
        end
    end
end







