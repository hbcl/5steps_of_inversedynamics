function Pjo_trans = CalcTransJointPower(omega_s1,v_com_s1,r_com_s1,v_prox_s2,r_prox_s2,F_s1_ON_s2)
% Pjo_trans = CalcTransJointPower(omega_s1,v_com_s1,r_com_s1,v_prox_s2,r_prox_s2,F_s1_ON_s2)
% Calculates translational joint power in an articulation between two
% segments s1 and s2. The net joint force acts from s1 on s2 (F_s1_ON_s2)
% at the joint centre, the location of which is assumed to coincide with
% the proximal end of s2 (r_prox_s2). 
%
% Procedure:
% The translational joint power is the difference between the translational
% power acting from s1 on s2 and that acting from s2 on s1. This is
% calculated as the dot product of the joint contact force with the
% difference between the velocity of the joint centre when that is taken to
% be rigidly connected to s2 and when that is taken to be rigidly connected
% to s1. 
%
% !! All vectors MUST be expressed in the same (eg global) reference frame
%
% definition of inputs: 
% r_... [n x 3] position vector
% v_... [n x 3] velocity vector 
% omega_s1 [n x 3] angular velocity vector of s1
% F_s1_ON_s2 [n x 3] force from s1 on s2 acting at proximal end of s2

% Koen Lemaire 6/2020

% vector from s1 centre of mass (r_com_s1) to joint centre (r_prox_s2)
r=r_prox_s2-r_com_s1;

% velocity of joint centre on s1 
v_jc_s1=v_com_s1 + cross(omega_s1,r,2);

% velocity of joint centre on s2 
v_jc_s2=v_prox_s2;

% power of joint contact force:
Pjo_trans=(v_jc_s2-v_jc_s1).*F_s1_ON_s2;
