function variableplot(field,variable, trials)

%field= grf  variable= force1 for example
% trials for exp. 1 [1, 4, 7, 12, 15, 33]   
% exp. 2 constant step freq [3, 6, 9, 10, 13, 31]
% exp. 3 constant speed [17:25]
% exp. 4 perferred speed [2, 5, 8, 11, 14, 16, 20:22, 32]

%% Load data and define variable of interest
addpath(genpath('D:\ID_grid_data\Tim'))
cd 'D:\ID_grid_data\Tim'

for subject= [1:3, 5:10]
    load ((strcat('p', num2str(subject), '_5StepsData')))
    
    if subject==1
        varsize=size(data(trials(1)).(field).(variable))
        if varsize(2)>1
            disp('define variable "col" for the column of data you want to display')
            disp('then press any key to continue')
            pause
        else
            col=1
        end
    end
    
   
       %% Extract grfs 
    for trial=1:length(trials)
        
        GRFL=[data(trial).grf.force1(:,1) data(trial).grf.force1(:,2) data(trial).grf.force1(:,3)];
        GRFR=[data(trial).grf.force2(:,1) data(trial).grf.force2(:,2) data(trial).grf.force2(:,3)];

        % Get heelstrikes
        HSL = invDynGrid_getHS_TO(GRFL,GRFR,40);
        HSL = unique(HSL);

        trialdata=interpolate_to_percgaitcycle(data(trials(trial)).(field).(variable)(:,col), HSL,101)
        
        %var1{subject, trial}= 
    end
%use interpolate 
%GRFxmean(:,j,1)= mean(interpolate_to_percgaitcycle(GRFL(:,1),HSL,101),2,'omitnan')/mass(j);
%GRFxmean(:,j,2)= mean(interpolate_to_percgaitcycle(GRFR(:,1),HSL,101),2,'omitnan')/mass(j);
end

end
