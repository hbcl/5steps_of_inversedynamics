function [] = find_5steps(datafolder, subjects, trials)

addpath(genpath(datafolder))

load('5steps_indices.mat','hsl','start')
load('5steps_heelstrikes.mat','hsl_grf','hsr_grf')

subjnames = {'eu' ,'kel', 'kez', 'leb', 'mmm', 'pbc', 'pga', 'smo', 'wj', 'yg'};
date = '0710';
midparts = {'25Force6Marker_walking_070to110_and_160to200_data', '25Force6Marker_walking_125_and_140_data'};

% hsl_grf = nan(10,33);
% hsr_grf = nan(10,33);

for subj = subjects
    disp(subj)
    
    subjname = char(subjnames(subj));
    cd(datafolder)
    
    for trial = 1:length(trials)
        
%         if isfinite(hsl_grf(subj,trial)), continue
%         end
        
        disp(trial)
        
        if trial<17, CMOfile = 1;
        else, CMOfile = 2;
        end
       

        load([subjname, midparts{CMOfile}, date,'_TG', num2str(trial),'.mat'],'f1','f2')

        if ~isnan(hsl(subj,trial)) && ~isempty(f1)
                         
            %% Find new heelstrike
            grfl = f1{1}(start(subj,trial):end,1:3);
            grfr = f2{1}(start(subj,trial):end,1:3);
            
            % Threshold at 20 N
            grfl(grfl(:,3)<20,:) = 0;
            grfr(grfr(:,3)<20,:) = 0;  
            
            % find hsl with different method            
            [LHS, ~, RHS, ~] = invDynGrid_getHS_TO(grfl, grfr, 5);
            
            % make sure they are unique
            LHS = unique(LHS);
            
            % first new left heel strike: choose the closest to the first left heel strike
            [~,closest] = min(abs(LHS-hsl(subj,trial)));
            hsl_close = [LHS(closest) LHS(closest+5)];

            % only consider right heel strikes after the identified left
            % heel strike and pick the first one
            RHS_future = RHS(RHS>hsl_close(2));
            hsr_first = RHS_future(1);
            
            figure;
            plot(grfl(:,3)); hold on
            plot(hsl(subj,trial), 0, 'bo')
            plot(LHS, zeros(size(LHS)), 'bx')
            plot(hsl_close, [0 0], 'b+')
            
            plot(grfr(:,3)); hold on
%             plot(hsl(subj,trial), 0, 'o')
            plot(RHS, zeros(size(RHS)), 'rx')
            plot(hsr_first, [0 0], 'r+')

            % ground reaction force heelstrikes
            hsl_grf(subj,trial) = hsl_close(1) +  start(subj,trial) -1;
            hsr_grf(subj,trial) = hsr_first +  start(subj,trial) -1;
           
        end
    end
end

cd(datafolder)
% save('5steps_heelstrikes.mat','hsl_grf','hsr_grf')

end
