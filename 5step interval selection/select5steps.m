function[] = select5steps(datafolder,subjects,trials)
%Use selct5steps to select 5 step range
addpath(genpath(datafolder))
cd(datafolder)
subjnames = {'eu' ,'kel', 'kez', 'leb', 'mmm', 'pbc', 'pga', 'smo', 'wj', 'yg'};
date = '0710';
midparts = {'25Force6Marker_walking_070to110_and_160to200_data', '25Force6Marker_walking_125_and_140_data'};

% start with what we have already and add to that
load('5steps_indices_new.mat','hsl','start')

for subj = subjects
    disp(subj)
    
    subjname = char(subjnames(subj));
    
    for trial = trials

        netshift = 0;
        shift = 10;

        if trial<17, CMOfile = 1;
        else, CMOfile = 2;
        end

        if exist([subjname, midparts{CMOfile}, date,'_TG', num2str(trial),'.mat'],'file')
            load([subjname, midparts{CMOfile}, date,'_TG', num2str(trial),'.mat'],'f1','f2','plank','plkne','plhip','prank','prkne','prhip')
        else, continue
        end
    
        if isempty(f1)
            continue
        end
        
        %ask if hsl exists or not, if it exists use previous range, if not
        %pop up figure to select heekstrike range
        if isnan(start(subj, trials))
            figure; 
            plot(f1{1}); hold on; plot(f2{1})
            xlabel ('Force')
            ylabel ('Frames')
            %if isnan(start(subj, trial))
            [idx,~] = ginput(2);
            close
            %idx=both clicks
            idx = round(idx);
            
        else idx(1)=start(subj, trials);
             idx(2)=length(f1{1});
        end
        
        % extract ground reaction force
        grfl = f1{1}(idx(1):idx(2),:);
        grfr = f2{1}(idx(1):idx(2),:);

        % Threshold at 20 N
        grflt = grfl;
        grfrt = grfr;
        grflt(grflt(:,3)<20,:) = 0;
        grfrt(grfrt(:,3)<20,:) = 0;  

        idx_mo = round(idx/10);
%pjl and pjr are power
        pjl = [sum(plank{1}(idx_mo(1):idx_mo(2),:),2) sum(plkne{1}(idx_mo(1):idx_mo(2),:),2) sum(plhip{1}(idx_mo(1):idx_mo(2),:),2)];
        pjr = [sum(prank{1}(idx_mo(1):idx_mo(2),:),2) sum(prkne{1}(idx_mo(1):idx_mo(2),:),2) sum(prhip{1}(idx_mo(1):idx_mo(2),:),2)];
        
        % extract heelstrikes and toe-offs from grf
        hsl_new = unique(invDynGrid_getHS_TO(grflt, grfrt, 20));
        hsl_mo = unique(round(hsl_new/10) + 1); % heelstrikes for mocop (fs_mocop = .1*fs_grf)
        
        %n= #of heelstrikes, if the pre selected heelstrike exists use that
        %as the start of the 5 step range, if not use the midpoint as the
        %beginning of 5 steps
        n = length(hsl_new);
        if isnan(hsl(subj, trials))
            netshift=0
        else
            [~,i] = min(abs(hsl(subj, trial)-hsl_new));
                netshift = (i)-(ceil(n/2));
            end

    while shift ~= 0
        close all
       
%         keyboard
        %netshift=0 at the beginning so 5 step start at middle of hsl_new,
        %unless the previously selected range is used, in which case
        %netshift~= 0
        if (netshift) > abs(ceil(n/2)),
           hsl_mid = hsl_new(1:5);
        elseif (netshift) < (ceil(n/2)*(-1)),
            hsl_mid=hsl_new(n-5:n);
        else, hsl_mid = hsl_new((ceil(n/2)+netshift):(ceil(n/2)+5+netshift)); 
        end
        
        hsl_mo_mid = round(hsl_mid/10) + 1;
    
        h = figure;
        
        %% left
        %plot GRF of entire stance phase and GRF of 5 steps
        subplot(241);
        plot(pjl); hold on
        
        plot([hsl_mo_mid(1) hsl_mo_mid(1)], [min(min(pjl)) max(max(pjl))],'k','linewidth',2)
        plot([hsl_mo_mid(end) hsl_mo_mid(end)], [min(min(pjl)) max(max(pjl))],'k','linewidth',2)
        ylabel ('Power(W)')
        
        subplot(242);
        plotperstride(pjl, hsl_mo); hold on
        plotperstride(pjl, hsl_mo_mid)
        ylabel ('Power(W)')
        
        subplot(243);
        plotperstride(pjl, hsl_mo_mid)
        ylabel ('Power(W)')
        
        subplot(244);
        plotperstride(grfl, hsl_mid)
        ylabel ('Force(N)')
        
        %% right
        subplot(245);
        plot(pjr); hold on
        
        plot([hsl_mo_mid(1) hsl_mo_mid(1)], [min(min(pjr)) max(max(pjr))],'k','linewidth',2)
        plot([hsl_mo_mid(end) hsl_mo_mid(end)], [min(min(pjr)) max(max(pjr))],'k','linewidth',2)
        ylabel ('Power(W)')
        
        subplot(246);
        plotperstride(pjr, hsl_mo); hold on
        plotperstride(pjr, hsl_mo_mid)
        ylabel ('Power(W)')
        
        subplot(247);
        plotperstride(pjr, hsl_mo_mid)
        ylabel ('Power(W)')
        
        subplot(248);
        plotperstride(grfr, hsl_mid)
        ylabel ('Force(N)')
        
        for i=1:8
            subplot (2,4,i)
            xlabel ('frames')
            axis tight
            
        end
        
        set(h,'Units','normalized', 'position', [0 .5 1 .5])
        
        %Ask user if they want to shift, the number entered shifts them
        %that many heelstrikes forward or backward from the current 5 step
        %range (not the first 5 step range)
        shift = input('Want to shift? [0 = no shift, + = forward shift, - = backward shift]');
        close(h)
        
        netshift = netshift + shift;

    end

    hsl(subj,trial) = hsl_mid(1);
    start(subj,trial) = idx(1);
    end
end
    
% save('5steps_indices.mat','hsl','start')

%% No solution (2020-06-19)
% subj 6 trial 21 is super short (4 steps) and 31 is missing mocap data
% subj 3 trial 4 has weird powers
% subj 8 trial 15 has crazy powers
% subj 9 trial 14 has crazy powers (especially ankle)

%% Old comments (2019)
% subj 1: looks great !
% subj 2: trial 12 is absent, 
% subj 3: trial 4 crazy joint power, trial 12 crazy COM power (possibly
% due to difference left and right), trial 20 and 30-33 are absent,
% trial 25 crazy COM power (possibly due to difference left and right
% leg)
% subj 4: trial 7 and 9 crazy joint power (all joints) due to crazy
% moments, trial 15 and 16 and 21 are absent, trial 27 is very short,
% trials 28 and 30 and 33 crazy joint power (e.g. ankle crazy negative)
% subj 5: trial 15 crazy joint power (mostly knee: doing 100 Nm during push-off)
% subj 6: trial 12 is absent, trial 21 super short, trial 31 no joint
% powers and moments
% subj 7: trial 25 and 26 absent, joint power 32 looks slightly crazy
% (e.g. 3 W/kg positive knee power)
% subj 8: trial 11 and 16 are absent, trial 15 crazy COM power (possibly due to difference left and right, about a factor 2)
% subj 9: trial 4, 6 and 14 crazy joint power (mostly ankle and knee, crazy
% negative peaks), in trial 4 it is there all the time, in trial 6 it
% starts half way, in trial 16 it is mostly ankle (power peaks of 1600
% Wats and moment peaks of 200 Nm)
% subj 10: trial 1 crazy grf, it seems like participant only walks on 1
% of the force platforms, same for trial 23
end