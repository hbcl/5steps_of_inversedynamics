 function [] = process_5steps(datafolder, subjects, trials)
% Loads:
% 1. 5 steps indices
% 2. Raw data that is outputted from visual3D
% Saves: 5 steps data stored for each participant in a struct

addpath(genpath(datafolder))
load('5steps_heelstrikes.mat','hsl_grf','hsr_grf')
subjnames = {'eu' ,'kel', 'kez', 'leb', 'mmm', 'pbc', 'pga', 'smo', 'wj', 'yg'};
date = '1019';

% convert to mocap
hsl_mo = round(hsl_grf/10) + 1;
hsr_mo = round(hsr_grf/10) + 1;

for subj = subjects
    disp(subj)

    % start with what you have
    cd(datafolder)
    load(['p',num2str(subj),'_5steps_data'], 'data')
    
    % go to subject folder
    subjname = char(subjnames(subj));
    cd('C:\Users\Tim\OneDrive\shifted')
    cd([datafolder,'/rawdata/',subjname])
    
    for trial = trials
        disp(trial)
        
        files=dir(['*',date,'_TG', num2str(trial),'.mat']);

        if ~isempty(files)
            load(files.name)
        else, continue
        end
        
        if ~isnan(hsl_grf(subj,trial)) && ~isempty(f1)

            % if hip is empty
            if isempty(plhip{1})
                plhip{1} = nan(size(plkne{1}));
                prhip{1} = nan(size(prkne{1}));
                plhip2{1} = nan(size(plkne2{1}));
                prhip2{1} = nan(size(prkne2{1}));
            end

            %% Process GRF
            f1a = [f1xproc{1} f1yproc{1} f1zproc{1} m1xproc{1} m1yproc{1} m1zproc{1}]';
            f2a = [f2xproc{1} f2yproc{1} f2zproc{1} m2xproc{1} m2yproc{1} m2zproc{1}]';
            
            C = fp_calmatrix{1};
            C(4:6,:,:) = C(4:6,:,:) / 1000; % mm -> m
            
            f1av = (C(:,:,1)' * f1a)';
            f2av = (C(:,:,2)' * f2a)';

            %% Translational power method #1: global
            % flank_lab is the ProxEndForce from the KINETIC/KINEMATIC
            % folder. It's vertical component is negative, indicating that
            % it is the force from the proximal segment on the distal
            % segment
            % (https://c-motion.com/v3dwiki/index.php?title=KINETIC_KINEMATIC:_ProxEndForce)
            
            ptlank_lab = CalcTransJointPower(lsk_omega_lab{1},lsk_cgvel_lab{1},lsk_cgpos_lab{1},lank_vel_lab{1},lank_pos_lab{1},flank_lab{1}/subjectMass{1});
            ptrank_lab = CalcTransJointPower(rsk_omega_lab{1},rsk_cgvel_lab{1},rsk_cgpos_lab{1},rank_vel_lab{1},rank_pos_lab{1},frank_lab{1}/subjectMass{1});

            ptlkne_lab = CalcTransJointPower(lth_omega_lab{1},lth_cgvel_lab{1},lth_cgpos_lab{1},lkne_vel_lab{1},lkne_pos_lab{1},flkne_lab{1}/subjectMass{1});
            ptrkne_lab = CalcTransJointPower(rth_omega_lab{1},rth_cgvel_lab{1},rth_cgpos_lab{1},rkne_vel_lab{1},rkne_pos_lab{1},frkne_lab{1}/subjectMass{1});

            ptlhip_lab = CalcTransJointPower(rpv_omega_lab{1},rpv_cgvel_lab{1},rpv_cgpos_lab{1},lhip_vel_lab{1},lhip_pos_lab{1},flhip_lab{1}/subjectMass{1});
            ptrhip_lab = CalcTransJointPower(rpv_omega_lab{1},rpv_cgvel_lab{1},rpv_cgpos_lab{1},rhip_vel_lab{1},rhip_pos_lab{1},frhip_lab{1}/subjectMass{1}); 

            %% Translational power method #2: local
%             lank_vel_loc = CalcTransJointPower_Locally(lsk_wrt_lank_loc{1}, flank_loc, fs);
            
            lank_vel_loc = grad5(-lsk_wrt_lank_loc{1}, 1/120);
            rank_vel_loc = grad5(-rsk_wrt_rank_loc{1}, 1/120);
            lkne_vel_loc = grad5(-lth_wrt_lkne_loc{1}, 1/120);
            rkne_vel_loc = grad5(-rth_wrt_rkne_loc{1}, 1/120);
            lhip_vel_loc = grad5(-rpv_wrt_lhip_loc{1}, 1/120);
            rhip_vel_loc = grad5(-rpv_wrt_rhip_loc{1}, 1/120);

            ptlank_loc = flank_loc{1} .* lank_vel_loc;
            ptrank_loc = frank_loc{1} .* rank_vel_loc;
            ptlkne_loc = flkne_loc{1} .* lkne_vel_loc;
            ptrkne_loc = frkne_loc{1} .* rkne_vel_loc;
            ptlhip_loc = flhip_loc{1} .* lhip_vel_loc;
            ptrhip_loc = frhip_loc{1} .* rhip_vel_loc;

            %% Store file details
            data(trial).name = FILE_NAME{1};
            
            %% Store forces
            data(trial).grf = [f1{1}(hsl_grf(subj,trial):hsr_grf(subj,trial),:) f2{1}(hsl_grf(subj,trial):hsr_grf(subj,trial),:)];
            data(trial).grf2 = [f1av(hsl_grf(subj,trial):hsr_grf(subj,trial),1:3) f2av(hsl_grf(subj,trial):hsr_grf(subj,trial),1:3)];
%             data(trial).cop = [COP1{1}(hsl_grf(subj,trial):hsr_grf(subj,trial),:) COP2{1}(hsl_grf(subj,trial):hsr_grf(subj,trial),:)];
            
            data(trial).jointpower = [plank{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) plkne{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)...
                                  plhip{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) prank{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)...
                                  prkne{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) prhip{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)];
           data(trial).jointpower2 = [plank2{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) plkne2{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)...
                                  plhip2{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) prank2{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)...
                                  prkne2{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) prhip2{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)];
           data(trial).jointangle = [alank{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) alkne{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)...
                                  alhip{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) arank{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)...
                                  arkne{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) arhip{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)];
           data(trial).jointangle2 = [alank2{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) alkne2{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)...
                                  alhip2{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) arank2{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)...
                                  arkne2{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) arhip2{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)];

           %% Store translation data
           data(trial).jointpos.loc = [-lsk_wrt_lank_loc{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) -lth_wrt_lkne_loc{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) -rpv_wrt_lhip_loc{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) ...
                                       -rsk_wrt_rank_loc{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) -rth_wrt_rkne_loc{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) -rpv_wrt_rhip_loc{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)];

           data(trial).jointpos.lab = [lank_pos_lab{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) lkne_pos_lab{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) lhip_pos_lab{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) ...
                                       rank_pos_lab{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) rkne_pos_lab{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) rhip_pos_lab{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)];

           data(trial).transpower.loc = [ptlank_loc(hsl_mo(subj,trial):hsr_mo(subj,trial),:) ptlkne_loc(hsl_mo(subj,trial):hsr_mo(subj,trial),:) ptlhip_loc(hsl_mo(subj,trial):hsr_mo(subj,trial),:)...
                                         ptrank_loc(hsl_mo(subj,trial):hsr_mo(subj,trial),:) ptrkne_loc(hsl_mo(subj,trial):hsr_mo(subj,trial),:) ptrhip_loc(hsl_mo(subj,trial):hsr_mo(subj,trial),:)];

           data(trial).transpower.lab = [ptlank_lab(hsl_mo(subj,trial):hsr_mo(subj,trial),:) ptlkne_lab(hsl_mo(subj,trial):hsr_mo(subj,trial),:) ptlhip_lab(hsl_mo(subj,trial):hsr_mo(subj,trial),:)...
                                         ptrank_lab(hsl_mo(subj,trial):hsr_mo(subj,trial),:) ptrkne_lab(hsl_mo(subj,trial):hsr_mo(subj,trial),:) ptrhip_lab(hsl_mo(subj,trial):hsr_mo(subj,trial),:)];
           %% Store rotation data
           data(trial).jointmoment = [mlank{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) mlkne{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)...
                                  mlhip{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) mrank{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)...
                                  mrkne{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) mrhip{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)];
           data(trial).jointmoment2 = [mlank2{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) mlkne2{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)...
                                  mlhip2{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) mrank2{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)...
                                  mrkne2{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) mrhip2{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)];

           data(trial).jointforce.loc = [flank_loc{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) flkne_loc{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) flhip_loc{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)...
                                         frank_loc{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) frkne_loc{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) frhip_loc{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)]; 

           data(trial).jointforce.lab = [flank_lab{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) flkne_lab{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) flhip_lab{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)...
                                         frank_lab{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) frkne_lab{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) frhip_lab{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)]; 

           %% segment data
           data(trial).rotenergy =    [lft_rotenergy{1}(hsl_mo(subj,trial):hsr_mo(subj,trial)) lsk_rotenergy{1}(hsl_mo(subj,trial):hsr_mo(subj,trial))... 
                                   lth_rotenergy{1}(hsl_mo(subj,trial):hsr_mo(subj,trial)) rft_rotenergy{1}(hsl_mo(subj,trial):hsr_mo(subj,trial))...
                                   rsk_rotenergy{1}(hsl_mo(subj,trial):hsr_mo(subj,trial)) rth_rotenergy{1}(hsl_mo(subj,trial):hsr_mo(subj,trial))];

                              
           data(trial).segmenvel = [lft_cgvel_lab{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) lsk_cgvel_lab{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) lth_cgvel_lab{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) ...                   
                                    rft_cgvel_lab{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) rsk_cgvel_lab{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) rth_cgvel_lab{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:) rpv_cgvel_lab{1}(hsl_mo(subj,trial):hsr_mo(subj,trial),:)];
        else
            data(trial).name = nan;
            data(trial).grf = nan;
            data(trial).grf2 = nan;
            data(trial).cop = nan;
            
            data(trial).jointpower = nan;
            data(trial).jointpower2 = nan;
            data(trial).jointangle = nan;
            data(trial).jointangle2 = nan;
            data(trial).jointmoment = nan;
            data(trial).jointmoment2 = nan;

            data(trial).jointforce.lab = nan;
            data(trial).jointforce.loc = nan;
            data(trial).jointpos.lab = nan;
            data(trial).jointpos.loc = nan;
            data(trial).transpower.lab = nan;
            data(trial).transpower.loc = nan;

            data(trial).rotenergy = nan;
            data(trial).segmenvel = nan;
            
            

        end
    end

    cd(datafolder)
    save(['p',num2str(subj),'_5steps_data_shifted'], 'data')
end
end

